#!/bin/bash
set -e
mvn clean package dockerfile:build
mvn dockerfile:tag@tag-latest
mvn dockerfile:push@push-latest
mvn dockerfile:push@push-version

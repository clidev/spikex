/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.launcher;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import io.spikex.base.helper.VerticleHelper;
import io.spikex.utils.Environment;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import joptsimple.internal.Strings;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.util.Arrays.asList;

/**
 * Start Spike.x
 *
 * @author cli
 */
public final class Main {

    private static final String OPT_CONFIG_FILE = "config";
    private static final String OPT_INIT = "init";
    private static final String OPT_HELP = "help";

    private static final String KEY_MODULES = "modules";
    private static final String KEY_NAME = "name";
    private static final String KEY_CONFIGURATIONS = "configurations";

    private static final String URL_CONFIG_SPIKEX = "classpath:spikex.json";
    private static final String URL_CONFIG_LOGBACK_CONFIG = "classpath:logback.xml";
    private static final String URL_CONFIG_LOGBACK_CONSOLE_CONFIG = "classpath:logback-console.xml";

    private static final String DEF_CONFIG_FILENAME = "spikex.json";
    private static final String MAIN_VERTICLE_NAME = ".MainVerticle";

    public static void main(final String[] args) throws Exception {

        OptionParser parser = new OptionParser();

        // Main configuration file
        OptionSpec<String> optConfigFile = parser.accepts(OPT_CONFIG_FILE,
                "Name of Spike.x configuration file (optional)")
                .withOptionalArg()
                .ofType(String.class);

        // Init
        parser.accepts(OPT_INIT,
                "Initialize configuration (optional)")
                .withOptionalArg()
                .ofType(Boolean.class);

        // Help
        parser.acceptsAll(asList(OPT_HELP, "?"), "show help").forHelp();

        OptionSet options = parser.parse(args);

        Path configPath = Paths.get(Environment.configHome());

        if (options.has(OPT_HELP)) {
            parser.printHelpOn(System.out);
        } else if (options.has(OPT_INIT)) {
            //
            // Extract configuration files from launcher jar
            //
            Path spikexConfigDest = configPath.resolve(DEF_CONFIG_FILENAME);

            // Create config dir if it doesn't exist
            spikexConfigDest.getParent().toFile().mkdirs();

            io.spikex.utils.Files.copyFile(
                    URI.create(URL_CONFIG_SPIKEX),
                    spikexConfigDest,
                    true);

            // Logback config
            io.spikex.utils.Files.copyFile(
                    URI.create(URL_CONFIG_LOGBACK_CONFIG),
                    spikexConfigDest,
                    true);

            // Logback console config
            io.spikex.utils.Files.copyFile(
                    URI.create(URL_CONFIG_LOGBACK_CONSOLE_CONFIG),
                    spikexConfigDest,
                    true);

            // Filter configurations
            FiltersConfig.init(spikexConfigDest);

        } else {
            //
            // Start Spike.x
            //
            String configFile = DEF_CONFIG_FILENAME;
            if (options.has(OPT_CONFIG_FILE)) {
                configFile = options.valueOf(optConfigFile);
            }

            JsonObject root = Json.parse(
                    new String(Files.readAllBytes(configPath.resolve(configFile)))).asObject();
            JsonArray modules = root.get(KEY_MODULES).asArray();
            modules.forEach(module -> {

                String moduleName = module.asObject().getString(KEY_NAME, "");

                if (Strings.isNullOrEmpty(moduleName)) {
                    throw new IllegalArgumentException("module name is missing in: " + configPath);
                }

                //
                // Build config paths
                //
                JsonArray moduleConfigs = new JsonArray();
                JsonValue configs = module.asObject().get(KEY_CONFIGURATIONS);
                if (configs != null) {
                    JsonArray configArray = configs.asArray();
                    for (int i = 0; i < configArray.size(); i++) {
                        moduleConfigs.add(configPath
                                .resolve(moduleName)
                                .resolve(configArray.get(i).asString())
                                .toString());
                    }
                }

                //
                // Launch verticle
                //
                VerticleHelper.startVerticle(
                        new String[]{
                                Environment.asString(),
                                moduleConfigs.toString()
                        },
                        moduleName + MAIN_VERTICLE_NAME);
            });
        }
    }
}

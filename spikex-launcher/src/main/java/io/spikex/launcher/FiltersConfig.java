/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.launcher;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;

/**
 * @author cli
 */
public final class FiltersConfig {

    private static final String CONFIG_COLLECTD = "collectd-input.json";
    private static final String URL_CONFIG_FILTER_COLLECTD =
            "classpath:io.spikex.filters/" + CONFIG_COLLECTD;

    private static final String CONFIG_GRAYLOG2 = "graylog2-input.json";
    private static final String URL_CONFIG_FILTER_GRAYLOG2 =
            "classpath:io.spikex.filters/" + CONFIG_GRAYLOG2;

    private static final String CONFIG_HTTP = "http-input.json";
    private static final String URL_CONFIG_FILTER_HTTP =
            "classpath:io.spikex.filters/" + CONFIG_HTTP;

    private static final String FILTERS_MODULE_ID = "io.spikex.filters";
    private static final String FILTERS_MAPPING_DIR = "mapping";

    private static final String MAPPING_COMMON = "common.json";
    private static final String URL_MAPPING_COMMON =
            "classpath:mapping/" + MAPPING_COMMON;

    private static final String MAPPING_GRAYLOG2 = "graylog2.json";
    private static final String URL_MAPPING_GRAYLOG2 =
            "classpath:mapping/" + MAPPING_GRAYLOG2;

    public static void init(final Path spikexConfigDest) throws IOException {

        Path filtersConfigDest = spikexConfigDest.getParent().resolve(FILTERS_MODULE_ID);
        filtersConfigDest.toFile().mkdirs();

        // Collectd
        io.spikex.utils.Files.copyFile(
                URI.create(URL_CONFIG_FILTER_COLLECTD),
                filtersConfigDest.resolve(CONFIG_COLLECTD),
                true);

        // Graylog2
        io.spikex.utils.Files.copyFile(
                URI.create(URL_CONFIG_FILTER_GRAYLOG2),
                filtersConfigDest.resolve(CONFIG_GRAYLOG2),
                true);

        // Http (elasticsearch)
        io.spikex.utils.Files.copyFile(
                URI.create(URL_CONFIG_FILTER_HTTP),
                filtersConfigDest.resolve(CONFIG_HTTP),
                true);

        Path mappingConfigDest = spikexConfigDest.getParent().resolve(FILTERS_MAPPING_DIR);
        mappingConfigDest.toFile().mkdirs();

        // Common mapping example
        io.spikex.utils.Files.copyFile(
                URI.create(URL_MAPPING_COMMON),
                mappingConfigDest.resolve(MAPPING_COMMON),
                true);

        // Graylog2 mapping example
        io.spikex.utils.Files.copyFile(
                URI.create(URL_MAPPING_GRAYLOG2),
                mappingConfigDest.resolve(MAPPING_GRAYLOG2),
                true);
    }
}

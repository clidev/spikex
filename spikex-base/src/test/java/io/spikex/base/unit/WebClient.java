/**
 * Copyright (c) 2015 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.base.unit;

import io.spikex.base.connection.AbstractHttpClient;
import io.spikex.base.connection.ConnectionException;
import io.spikex.base.connection.HttpClientAdapter;
import io.spikex.base.connection.HttpClientResponseAdapter;
import io.spikex.base.connection.HttpClientWithOpts;
import io.spikex.base.connection.IConnection;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpClientResponse;

import static com.google.common.net.HttpHeaders.CONTENT_LENGTH;

/**
 *
 * @author cli
 */
public final class WebClient extends AbstractHttpClient {

    private int m_counter;

    @Override
    protected void startClient() {

        logger().info("Client started with: {}", config());
        vertx.setTimer(500L, id -> {
            logger().info("===> Sending stop message");
            vertx.eventBus().send(context.deploymentID(), "stop");
        });

        vertx.setPeriodic(100L, id -> {
            // Periodic request (if any server is available)
            try {
                if (connections().getAvailableCount() > 0) {
                    doPost(
                            connections().next(),
                            "/test",
                            ++m_counter);
                }
            } catch (ConnectionException e) {
                logger().error(e.getMessage());
            }
        });

        try {
            // Initial request
            doPost(
                    connections().next(),
                    "/initial",
                    ++m_counter);
        } catch (ConnectionException e) {
            logger().error("Failed to send data to http server", e);
        }
    }

    private void doPost(
            final IConnection<HttpClientWithOpts> connection,
            final String postUri,
            final int requestNum) {

        HttpClientAdapter adapter = new HttpClientAdapter(connection) {

            @Override
            protected void doRequest(final HttpClientWithOpts client) {

                HttpClientRequest request = client.getClient().post(postUri, new HttpClientResponseAdapter() {
                    @Override
                    protected void handleSuccess(final HttpClientResponse response) {
                        logger().info("handleSuccess");
                        if (requestNum >= 5) {
                            disconnect();
                        }
                    }

                    @Override
                    protected void handleFailure(final HttpClientResponse response) {
                        logger().info("handleFailure");
                    }
                });
                String message = "Hello from HTTP client - request: " + requestNum;
                byte[] body = message.getBytes();
                request.putHeader(CONTENT_LENGTH, String.valueOf(body.length));
                request.write(Buffer.buffer(body));
                request.end();
            }
        };

        connection.doRequest(adapter);
    }
}

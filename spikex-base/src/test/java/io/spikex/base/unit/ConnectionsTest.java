/**
 * Copyright (c) 2015 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.base.unit;

import io.spikex.base.connection.ConnectionConfig;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.net.NetServer;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.RunTestOnContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static io.spikex.base.connection.ConnectionConfig.LB_STRATEGY_ROUND_ROBIN;

/**
 * @author cli
 */
@RunWith(VertxUnitRunner.class)
public class ConnectionsTest {

    private static final List<NetServer> NET_SERVERS = new ArrayList();
    private static final List<HttpServer> HTTP_SERVERS = new ArrayList();

    private static final List<URI> NET_SERVER_ADDRESSES = new ArrayList();
    private static final List<URI> HTTP_SERVER_ADDRESSES = new ArrayList();

    private static final Logger LOG = LoggerFactory.getLogger(ConnectionsTest.class);

    static {

        HTTP_SERVER_ADDRESSES.add(URI.create("http://localhost:52410"));
        HTTP_SERVER_ADDRESSES.add(URI.create("http://localhost:52414"));
        HTTP_SERVER_ADDRESSES.add(URI.create("http://localhost:52420"));

        // Non-existent server
        HTTP_SERVER_ADDRESSES.add(URI.create("http://localhost:52429"));

        NET_SERVER_ADDRESSES.add(URI.create("telnet://localhost:52210"));
        NET_SERVER_ADDRESSES.add(URI.create("telnet://localhost:52214"));
        NET_SERVER_ADDRESSES.add(URI.create("telnet://localhost:52220"));

        // Non-existent server
        NET_SERVER_ADDRESSES.add(URI.create("telnet://localhost:52230"));
    }

    @ClassRule
    public static final RunTestOnContext RULE = new RunTestOnContext();

    private final Future<String> m_startedEcho = Future.future();
    private final Future<String> m_startedWeb = Future.future();

    @Before
    public void startTcpServers(final TestContext context) {

        // Start TCP servers
        for (URI address : NET_SERVER_ADDRESSES.subList(0, 3)) {
            NET_SERVERS.add(startLocalTcpServer(address));
        }
    }

    @Before
    public void startHttpServers(final TestContext context) {

        // Start HTTP servers
        for (URI address : HTTP_SERVER_ADDRESSES.subList(0, 3)) {
            HTTP_SERVERS.add(startLocalHttpServer(address));
        }
    }

    @Test(timeout = 3000)
    public void testTcpConnection(final TestContext context) {

        Async async = context.async();

        // Create connection configuration
        ConnectionConfig.Nodes nodes = ConnectionConfig.builder(NET_SERVER_ADDRESSES)
                .loadBalancing(LB_STRATEGY_ROUND_ROBIN, 150L)
                .build();

        // Start worker verticle that communicates with TCP echo servers
        startVerticle(
                context,
                EchoClient.class.getName(),
                nodes,
                m_startedEcho);

        m_startedEcho.setHandler(deployment -> {

            String deploymentId = deployment.result();
            RULE.vertx().eventBus().consumer(deploymentId, message -> {

                for (NetServer server : NET_SERVERS) {
                    server.close();
                }

                async.complete();
            });
        });
    }

    @Test(timeout = 3000)
    public void testHttpConnection(final TestContext context) {
        Async async = context.async();

        // Create connection configuration
        ConnectionConfig.Nodes nodes = ConnectionConfig.builder(HTTP_SERVER_ADDRESSES)
                .loadBalancing(LB_STRATEGY_ROUND_ROBIN, 150L)
                .logActivity(false)
                .build();

        // Start worker verticle that communicates with HTTP servers
        startVerticle(
                context,
                WebClient.class.getName(),
                nodes,
                m_startedWeb);

        m_startedWeb.setHandler(deployment -> {

            String deploymentId = deployment.result();
            RULE.vertx().eventBus().consumer(deploymentId, message -> {

                for (HttpServer server : HTTP_SERVERS) {
                    server.close();
                }

                async.complete();
            });
        });
    }

    private NetServer startLocalTcpServer(final URI address) {

        LOG.info("Listening on {}:{}", address.getHost(), address.getPort());

        Vertx vertx = RULE.vertx();
        NetServer server = vertx.createNetServer();
        server.connectHandler(socket -> {

            LOG.debug("{} received connection from {}",
                    socket.localAddress(),
                    socket.remoteAddress());

            socket.handler(buffer -> socket.write(buffer));
            socket.closeHandler(v -> {
                LOG.info("Socket closed");
            });

        }).listen(address.getPort(), address.getHost());

        return server;
    }

    private HttpServer startLocalHttpServer(final URI address) {

        String host = address.getHost();
        int port = address.getPort();
        LOG.info("Listening on {}:{}", host, port);

        Vertx vertx = RULE.vertx();
        HttpServerOptions options = new HttpServerOptions().setLogActivity(false);
        HttpServer server = vertx.createHttpServer(options);
        server.requestHandler(request -> {

            LOG.debug("{} received request from {}",
                    request.localAddress(),
                    request.remoteAddress());

            request.bodyHandler(body -> LOG.info("{} client data: {}",
                    request.localAddress(),
                    body.toString(StandardCharsets.UTF_8.name())));
            request.response().setStatusCode(200).end();

        }).listen(port, host);

        return server;
    }

    private void startVerticle(
            final TestContext context,
            final String verticle,
            final ConnectionConfig.Nodes nodes,
            final Future future) {

        DeploymentOptions opts = new DeploymentOptions();
        opts.setConfig(nodes.toJson());

        Vertx vertx = RULE.vertx();
        vertx.exceptionHandler(context.exceptionHandler());
        vertx.deployVerticle(verticle, opts,
                res -> {
                    if (res.failed()) {
                        context.fail(res.cause());
                    } else {
                        future.complete(res.result());
                    }
                });
    }
}

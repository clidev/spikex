/**
 * Copyright (c) 2015 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.base.unit;

import io.spikex.base.connection.AbstractConnectionHealthChecker;
import io.spikex.base.connection.AbstractTcpClient;
import io.spikex.base.connection.ConnectionConfig;
import io.spikex.base.connection.IConnection;
import io.spikex.base.connection.IConnectionHealthChecker;
import io.spikex.base.connection.NetClientAdapter;
import io.spikex.base.connection.NetClientWithOpts;
import io.spikex.base.connection.TcpConnection;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetSocket;

import java.nio.charset.StandardCharsets;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

/**
 *
 * @author cli
 */
public final class EchoClient extends AbstractTcpClient {

    private int m_counter;

    @Override
    protected void startClient() {
        logger().info("Client started with: {}", config());
        vertx.setTimer(1000L, id -> {
            logger().info("===> Sending stop message");
            vertx.eventBus().send(context.deploymentID(), "stop");
        });
    }

    @Override
    protected void stopClient() {
        logger().info("Client stopped");
    }

    @Override
    protected IConnectionHealthChecker<TcpConnection> healthChecker(final ConnectionConfig.LoadBalancing lb) {
        return new AbstractConnectionHealthChecker<TcpConnection>(nodes().getLoadBalancing()) {

            @Override
            protected void healthCheck(
                    final TcpConnection connection,
                    final Handler<Boolean> handler) {

                connection.setConnected(true); // Assume OK
                sendData("Health check", connection, handler);
            }
        };
    }

    private void sendData(
            final String message,
            final IConnection<NetClientWithOpts> connection,
            final Handler<Boolean> handler) {

        NetClientAdapter adapter = new NetClientAdapter(connection) {

            @Override
            protected void handleReceivedData(
                    final NetSocket socket,
                    final Buffer buffer) {

                connection.setConnected(true); // Update state
                if (handler != null) {
                    handler.handle(TRUE);
                }

                logger().info("{} answered: {}",
                        socket.remoteAddress(),
                        buffer.toString(StandardCharsets.UTF_8.name()));

                if (++m_counter > 5) {
                    // Stop test
                    disconnect();
                }
            }

            @Override
            protected void handleConnectFailure(final Throwable cause) {
                logger().error("Failed to connect to: {} (marking as disconnected)", connection);
                connection.setConnected(false); // Update state
                disconnect();
                if (handler != null) {
                    handler.handle(FALSE);
                }
            }

            @Override
            protected void sendData(final NetSocket socket) {
                socket.write(message + " from " + socket.localAddress());
            }
        };

        connection.doRequest(adapter);
    }
}

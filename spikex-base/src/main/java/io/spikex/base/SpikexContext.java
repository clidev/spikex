/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.base;

import com.google.common.base.Strings;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author cli
 */
public class SpikexContext {

    private final VertxContainer m_container;

    private static final String KEY_HTTP_HOST = "spikex.http.host";
    private static final String KEY_HTTP_PORT = "spikex.http.port";

    private static final String DEFAULT_HTTP_HOST = "0.0.0.0";
    private static final String DEFAULT_HTTP_PORT = "9028";

    private static final String SYS_PROP_WORKER_POOL_SIZE = "io.spikex.worker.pool.size";
    private static final int DEF_WORKER_POOL_SIZE = 40;

    private static final Logger LOG = LoggerFactory.getLogger(SpikexContext.class);

    public SpikexContext() {
        m_container = new VertxContainer();
    }

    public VertxContainer vertxContainer() {
        return m_container;
    }

    public static class VertxContainer {

        private final Vertx m_vertx;
        private final Router m_router;

        public VertxContainer() {

            // Customize Vert.x settings
            VertxOptions options = new VertxOptions();
            options.setWorkerPoolSize(Integer.parseInt(
                    System.getProperty(SYS_PROP_WORKER_POOL_SIZE,
                            String.valueOf(DEF_WORKER_POOL_SIZE))));

            m_vertx = Vertx.vertx(options);
            LOG.debug("Vert.x instance: {}", m_vertx);

            //
            // Starting web server
            //
            HttpServer server = m_vertx.createHttpServer();
            m_router = Router.router(m_vertx);
            LOG.debug("Router instance: {}", m_router);

            String host = System.getProperty(KEY_HTTP_HOST, DEFAULT_HTTP_HOST);
            String port = System.getProperty(KEY_HTTP_PORT, DEFAULT_HTTP_PORT);

            if (!Strings.isNullOrEmpty(host)) {
                server.requestHandler(m_router::accept).listen(Integer.parseInt(port), host);
                LOG.info("HTTP server listening on {}:{}", host, port);
            }
        }

        public Vertx vertx() {
            return m_vertx;
        }

        public Router router() {
            return m_router;
        }
    }
}

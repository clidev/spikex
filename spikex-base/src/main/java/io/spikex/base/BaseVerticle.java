/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.base;

import io.spikex.base.helper.Variables;
import io.spikex.base.helper.VerticleHelper;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

/**
 * Spike.x base verticle. Provides access to shared resources.
 *
 * @author cli
 */
public class BaseVerticle extends AbstractVerticle {

    private static final Variables VARIABLES = new Variables();
    public static final String KEY_UPDATE_INTERVAL = "update-interval";
    public static final String KEY_INTERVAL_SHIFT = "interval-shift";

    private final long m_minInterval;

    private boolean m_started;
    private long m_updateInterval;
    private long m_timerId = -1L;

    private final Logger m_logger = LoggerFactory.getLogger(getClass());

    public BaseVerticle() {
        m_minInterval = 0L;
    }

    public BaseVerticle(final long minInterval) {
        m_minInterval = minInterval;
    }

    public void start() {

        m_started = true;

        onStart();

        //
        // Setup periodic timer if defined (support suffixes: s, m, h
        //
        Object value = config().getValue(KEY_UPDATE_INTERVAL);
        m_updateInterval = resolveInterval(value);
        if (m_updateInterval > 0L) {

            if (m_updateInterval < m_minInterval) {
                m_updateInterval = m_minInterval; // Limit interval
            }

            // Add shift/jitter to interval
            Random rnd = new Random();
            value = config().getValue(KEY_INTERVAL_SHIFT);
            m_updateInterval = m_updateInterval + rnd.nextInt((int) resolveInterval(value) + 1);

            m_logger.debug("Update interval: {}", m_updateInterval);
            m_timerId = vertx.setPeriodic(m_updateInterval, id -> {
                onTimerEvent();
            });
        }
    }

    public void stop() {

        m_started = false;

        //
        // Stop periodic timer
        //
        if (m_timerId != -1L) {
            vertx.cancelTimer(m_timerId);
        }

        onStop();
    }

    protected boolean isStarted() {
        return m_started;
    }

    protected final long getUpdateInterval() {
        return m_updateInterval;
    }

    protected final Variables variables() {
        return VARIABLES;
    }

    protected final Logger logger() {
        return m_logger;
    }

    protected void onStart() {
        // Do nothing by default...
    }

    protected void onStop() {
        // Do nothing by default...
    }

    protected void onTimerEvent() {
        // Do nothing by default...
    }

    protected JsonObject loadConfig(final String path) throws IOException {
        return new JsonObject(new String(Files.readAllBytes(Paths.get(path))));
    }

    protected EventBus eventBus() {
        return vertx.eventBus();
    }

    protected static Router router() {
        return VerticleHelper.container().router();
    }

    private long resolveInterval(final Object value) {

        long interval = 0L;

        if (value != null) {
            if (value instanceof String) {

                String strValue = (String) value;
                int len = strValue.length();
                char unit = strValue.charAt(len - 1);

                switch (unit) {
                    // seconds
                    case 's':
                        interval = Long.parseLong(strValue.substring(0, len - 1)) * 1000L;
                        break;
                    // minutes
                    case 'm':
                        interval = Long.parseLong(strValue.substring(0, len - 1)) * 1000L * 60L;
                        break;
                    // hours
                    case 'h':
                        interval = Long.parseLong(strValue.substring(0, len - 1)) * 1000L * 60L * 60L;
                        break;
                    // millis
                    default:
                        interval = Long.parseLong(strValue);
                        break;
                }
            } else {
                // Assume integer or long
                interval = (int) value;
            }
        }

        return interval;
    }
}

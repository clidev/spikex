/**
 * Copyright (c) 2015 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.base.connection;

import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.RequestOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

/**
 * @author cli
 */
public final class DefaultHttpConnectionHealthChecker
        extends AbstractConnectionHealthChecker<HttpConnection> {

    private static final int HTTP_SUCCESS_CODE = 200;
    private static final int HTTP_REDIRECTION_CODE = 300;
    private static final int HTTP_ERR_CODE = 400;

    private final Logger m_logger = LoggerFactory.getLogger(DefaultHttpConnectionHealthChecker.class);

    public DefaultHttpConnectionHealthChecker(
            final String loadBalancingStrategyName,
            final String statusUri,
            final long checkInterval) {

        super(loadBalancingStrategyName,
                statusUri,
                checkInterval);
    }

    public DefaultHttpConnectionHealthChecker(final ConnectionConfig.LoadBalancing lb) {
        super(lb);
    }

    @Override
    protected void healthCheck(
            HttpConnection connection,
            Handler<Boolean> handler) {

        connection.setConnected(true); // Asumme OK
        doGet(connection, loadBalancing().getStatusUri(), handler);
    }

    private void doGet(
            final HttpConnection connection,
            final String getUri,
            final Handler<Boolean> handler) {

        HttpClientAdapter adapter = new HttpClientAdapter(connection) {

            @Override
            protected void doRequest(final HttpClientWithOpts client) {

                RequestOptions opts = new RequestOptions();
                opts.setURI(getUri);
                opts.setHost(connection.getAddress().getHost());
                opts.setPort(connection.getAddress().getPort());
                HttpClientRequest request = connection.getClient()
                        .getClient().get(
                                opts,
                                response -> httpHealthCheck(connection, response, handler));

                Handler<Throwable> exceptionHandler = exceptionHandler();
                if (exceptionHandler == null) {
                    exceptionHandler = new DefaultConnectionExceptionHandler(connection);
                }

                request.exceptionHandler(exceptionHandler);
                request.end();
            }
        };
        connection.doRequest(adapter);
    }

    private void httpHealthCheck(
            final HttpConnection connection,
            final HttpClientResponse response,
            final Handler<Boolean> handler) {

        m_logger.trace("Got response: {}/{}",
                response.statusCode(),
                response.statusMessage());

        if (response.statusCode() < HTTP_ERR_CODE) {

            final Buffer body = Buffer.buffer();
            response.handler(data -> {
                if (response.statusCode() >= HTTP_SUCCESS_CODE
                        && response.statusCode() < HTTP_REDIRECTION_CODE) {

                    body.appendBuffer(data);
                }
            });
            response.endHandler(event -> {
                if (response.statusCode() >= HTTP_SUCCESS_CODE
                        && response.statusCode() < HTTP_REDIRECTION_CODE) {

                    String status = new String(body.getBytes(), StandardCharsets.UTF_8);
                    connection.setConnected(true);
                    if (handler != null) {
                        handler.handle(TRUE);
                    }
                    m_logger.trace("Connected to healthy host: {} ({})",
                            connection.getAddress(), status);
                } else {
                    DefaultHttpConnectionHealthChecker.this.handleFailure(connection, response, handler);
                }
            });
        } else {
            handleFailure(connection, response, handler);
        }
    }

    private void handleFailure(
            final HttpConnection connection,
            final HttpClientResponse response,
            final Handler<Boolean> handler) {

        if (response.statusCode() < HTTP_SUCCESS_CODE
                || response.statusCode() >= HTTP_REDIRECTION_CODE) {

            connection.disconnect();
            if (handler != null) {
                handler.handle(FALSE);
            }
            m_logger.error("Ignoring host: {}",
                    connection.getAddress(),
                    new IllegalStateException("Connection failure: "
                            + response.statusCode()
                            + "/"
                            + response.statusMessage()));
        }
    }
}

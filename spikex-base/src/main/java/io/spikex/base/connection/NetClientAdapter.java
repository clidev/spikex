/**
 * Copyright (c) 2015 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.base.connection;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetSocket;

/**
 *
 * @author cli
 */
public class NetClientAdapter implements Handler<AsyncResult<NetSocket>> {

    private final IConnection<NetClientWithOpts> m_connection;

    public NetClientAdapter(final IConnection<NetClientWithOpts> connection) {
        m_connection = connection;
    }

    public final IConnection<NetClientWithOpts> getConnection() {
        return m_connection;
    }

    @Override
    public final void handle(AsyncResult<NetSocket> result) {
        if (result.succeeded()) {
            handleConnectSuccess(result.result());
        } else {
            handleConnectFailure(result.cause());
        }
    }

    protected void handleConnectSuccess(final NetSocket socket) {
        socket.handler(buffer -> handleReceivedData(socket, buffer));
        sendData(socket);
    }

    protected void handleConnectFailure(final Throwable cause) {
        // Do nothing by default
    }

    protected void handleReceivedData(
            final NetSocket socket,
            final Buffer buffer) {
        // Do nothing by default
    }

    protected void sendData(final NetSocket socket) {
        // Do nothing by default
    }

    protected void disconnect() {
        m_connection.disconnect();
    }
}

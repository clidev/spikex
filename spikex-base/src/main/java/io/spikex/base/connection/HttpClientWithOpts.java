/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.base.connection;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;

/**
 * @author cli
 */
public final class HttpClientWithOpts {

    private final HttpClient m_client;
    private final HttpClientOptions m_opts;

    public HttpClientWithOpts(
            final HttpClient client,
            final HttpClientOptions opts) {

        m_client = client;
        m_opts = opts;
    }

    public HttpClient getClient() {
        return m_client;
    }

    public HttpClientOptions getOptions() {
        return m_opts;
    }

    public void close() {
        m_client.close();
    }
}

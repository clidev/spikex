/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.base.connection;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.net.NetClient;
import io.vertx.core.net.NetClientOptions;
import io.vertx.core.net.NetSocket;
import io.vertx.core.net.SocketAddress;

/**
 * @author cli
 */
public final class NetClientWithOpts implements NetClient {

    private final NetClient m_client;
    private final NetClientOptions m_opts;

    public NetClientWithOpts(
            final NetClient client,
            final NetClientOptions opts) {

        m_client = client;
        m_opts = opts;
    }

    public NetClient getClient() {
        return m_client;
    }

    public NetClientOptions getOptions() {
        return m_opts;
    }

    @Override
    public NetClient connect(
            final int port,
            final String host,
            final Handler<AsyncResult<NetSocket>> connectHandler) {

        return m_client.connect(port, host, connectHandler);
    }

    @Override
    public NetClient connect(
            final int port,
            final String host,
            final String serverName,
            final Handler<AsyncResult<NetSocket>> connectHandler) {

        return m_client.connect(port, host, serverName, connectHandler);
    }

    @Override
    public NetClient connect(
            final SocketAddress remoteAddress,
            final Handler<AsyncResult<NetSocket>> connectHandler) {

        return m_client.connect(remoteAddress, connectHandler);
    }

    @Override
    public NetClient connect(
            final SocketAddress remoteAddress,
            final String serverName,
            final Handler<AsyncResult<NetSocket>> connectHandler) {

        return m_client.connect(remoteAddress, serverName, connectHandler);
    }

    @Override
    public void close() {
        m_client.close();
    }

    @Override
    public boolean isMetricsEnabled() {
        return m_client.isMetricsEnabled();
    }
}

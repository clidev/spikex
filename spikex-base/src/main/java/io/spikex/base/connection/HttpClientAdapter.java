/**
 * Copyright (c) 2015 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.base.connection;

import io.vertx.core.Handler;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpClientResponse;

/**
 *
 * @author cli
 */
public abstract class HttpClientAdapter implements Handler<HttpClientWithOpts> {

    private final IConnection<HttpClientWithOpts> m_connection;

    public HttpClientAdapter(final IConnection<HttpClientWithOpts> connection) {
        m_connection = connection;
    }

    public final IConnection<HttpClientWithOpts> getConnection() {
        return m_connection;
    }

    @Override
    public void handle(final HttpClientWithOpts client) {
        // Trigger request
        doRequest(client);
    }

    protected abstract void doRequest(final HttpClientWithOpts client);

    protected HttpClientRequest doGet(
            final String uri,
            final Handler<HttpClientResponse> handler) {

        HttpClient client = m_connection.getClient().getClient();
        return client.get(uri, handler);
    }

    protected HttpClientRequest doPut(
            final String uri,
            final Handler<HttpClientResponse> handler) {

        HttpClient client = m_connection.getClient().getClient();
        return client.put(uri, handler);
    }

    protected HttpClientRequest doPost(
            final String uri,
            final Handler<HttpClientResponse> handler) {

        HttpClient client = m_connection.getClient().getClient();
        return client.post(uri, handler);
    }

    protected HttpClientRequest doDelete(
            final String uri,
            final Handler<HttpClientResponse> handler) {

        HttpClient client = m_connection.getClient().getClient();
        return client.delete(uri, handler);
    }

    protected HttpClientRequest doHead(
            final String uri,
            final Handler<HttpClientResponse> handler) {

        HttpClient client = m_connection.getClient().getClient();
        return client.head(uri, handler);
    }

    protected HttpClientRequest doOptions(
            final String uri,
            final Handler<HttpClientResponse> handler) {

        HttpClient client = m_connection.getClient().getClient();
        return client.options(uri, handler);
    }

    protected void disconnect() {
        m_connection.disconnect();
    }
}

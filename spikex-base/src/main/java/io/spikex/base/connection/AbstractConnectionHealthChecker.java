/**
 * Copyright (c) 2015 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.base.connection;

import io.vertx.core.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @param <E>
 * @author cli
 */
public abstract class AbstractConnectionHealthChecker<E extends IConnection>
        implements IConnectionHealthChecker<E> {

    private Handler<Throwable> m_exceptionHandler;

    private final ConnectionConfig.LoadBalancing m_lb;

    private final Logger m_logger = LoggerFactory.getLogger(getClass());

    public AbstractConnectionHealthChecker(
            final String loadBalancingStrategyName,
            final String statusUri,
            final long checkInterval) {

        m_lb = ConnectionConfig.LoadBalancing.create(
                loadBalancingStrategyName,
                statusUri,
                checkInterval);
    }

    public AbstractConnectionHealthChecker(final ConnectionConfig.LoadBalancing lb) {
        m_lb = lb;
    }

    public void setExceptionHandler(final Handler<Throwable> exceptionHandler) {
        m_exceptionHandler = exceptionHandler;
    }

    @Override
    public void doHealthCheck(
            final E connection,
            final Handler<Boolean> handler) {

        long currentTime = System.currentTimeMillis();
        long lastActivityTime = connection.getLastActivity();
        long checkInterval = m_lb.getCheckInterval();
        boolean checkEnabled = m_lb.isCheckEnabled();

        if (checkEnabled
                && (lastActivityTime + checkInterval) < currentTime) {

            healthCheck(connection, handler);
        }
    }

    protected abstract void healthCheck(
            E connection,
            Handler<Boolean> handler);

    protected Handler<Throwable> exceptionHandler() {
        return m_exceptionHandler;
    }

    protected ConnectionConfig.LoadBalancing loadBalancing() {
        return m_lb;
    }

    protected Logger logger() {
        return m_logger;
    }
}

/**
 * Copyright (c) 2015 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.base.helper;

import com.eaio.uuid.UUID;
import com.google.common.base.Preconditions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * JsonObject event factory and validator. Use this class to create new Spike.x
 * compliant events. Short string fields should not be over 256 characters.
 *
 * @author cli
 */
public final class Events {

    /**
     * Event identifier (UUID)
     */
    public static final String EVENT_FIELD_ID = "@id";

    /**
     * The host identifier - free form string (short string)
     */
    public static final String EVENT_FIELD_HOST = "@host";

    /**
     * The source of the event - free form string (short string)
     */
    public static final String EVENT_FIELD_SOURCE = "@source";

    /**
     * Event timestamp - the number of milliseconds that have elapsed since
     * 00:00:00 Coordinated Universal Time (UTC), Thursday, 1 January 1970, not
     * counting leap seconds.
     */
    public static final String EVENT_FIELD_TIMESTAMP = "@timestamp";

    /**
     * Event timezone - by default UTC
     */
    public static final String EVENT_FIELD_TIMEZONE = "@timezone";

    /**
     * The type of the event - must be one of the predefined event types
     */
    public static final String EVENT_FIELD_TYPE = "@type";

    /**
     * The event tags - free form list of strings
     */
    public static final String EVENT_FIELD_TAGS = "@tags";

    /**
     * The event chain - chain name of event
     */
    public static final String EVENT_FIELD_CHAIN = "@chain";

    /**
     * The event priority or severity - must be on of the pre-defined priorities
     */
    public static final String EVENT_FIELD_PRIORITY = "@priority";

    /**
     * The notification event title or subject - free form string
     */
    public static final String EVENT_FIELD_TITLE = "@title";

    /**
     * The notification or log event message - free form string
     */
    public static final String EVENT_FIELD_MESSAGE = "@message";

    /**
     * The metric event value
     */
    public static final String EVENT_FIELD_VALUE = "@value";

    /**
     * The metric event datasource name
     */
    public static final String EVENT_FIELD_DSNAME = "@dsname";

    /**
     * The metric event datasource type - must be one of the pre-defined types
     */
    public static final String EVENT_FIELD_DSTYPE = "@dstype";

    /**
     * The metric event datasource timestamp precision - must be one of the
     * pre-defined types
     */
    public static final String EVENT_FIELD_PRECISION = "@precision";

    /**
     * The metric event sampling interval in milliseconds - use empty string if not
     * applicable
     */
    public static final String EVENT_FIELD_INTERVAL = "@interval";

    /**
     * The metric event data instance - use empty string if not applicable
     */
    public static final String EVENT_FIELD_INSTANCE = "@instance";

    /**
     * The metric event data subgroup - use empty string if not applicable
     */
    public static final String EVENT_FIELD_SUBGROUP = "@subgroup";

    /**
     * The metric event unit
     */
    public static final String EVENT_FIELD_UNIT = "@unit";

    /**
     * The log event level
     */
    public static final String EVENT_FIELD_LOG_LEVEL = "@log-level";

    /**
     * The log full message (Graylog)
     */
    public static final String EVENT_FIELD_LOG_MESSAGE = "@log-message";

    /**
     * The list of events in a batch
     */
    public static final String EVENT_FIELD_BATCH_EVENTS = "@batch-events";

    /**
     * Batch size for batch events
     */
    public static final String EVENT_FIELD_BATCH_SIZE = "@batch-size";

    //
    // Supported event types
    //
    /**
     * The type used for events that carry sensor or performance data.
     */
    public static final String EVENT_TYPE_METRIC = "METRIC";

    /**
     * The type used for events that carry log lines.
     */
    public static final String EVENT_TYPE_LOG = "LOG";

    /**
     * The type used for events that carry messages (alerts, warnings, etc..)
     */
    public static final String EVENT_TYPE_NOTIFICATION = "NOTIFICATION";

    /**
     * The type used for events that contain other events
     */
    public static final String EVENT_TYPE_BATCH = "batch";

    //
    // Supported datasource types
    //
    public static final String DSTYPE_GAUGE = "GAUGE";
    public static final String DSTYPE_COUNTER = "COUNTER";
    public static final String DSTYPE_DERIVE = "DERIVE";
    public static final String DSTYPE_STRING = "STRING";

    //
    // Supported datasource timestamp precisions (InfluxDB compatible)
    //
    public static final String DSTIME_PRECISION_NANOS = "n";
    public static final String DSTIME_PRECISION_MICROS = "u";
    public static final String DSTIME_PRECISION_MILLIS = "ms";
    public static final String DSTIME_PRECISION_SEC = "s"; // Default in Spike.x
    public static final String DSTIME_PRECISION_MIN = "m";
    public static final String DSTIME_PRECISION_HOUR = "h";

    //
    // Supported event priorities
    //
    public static final String EVENT_PRIORITY_LOW = "LOW";
    public static final String EVENT_PRIORITY_NORMAL = "NORMAL";
    public static final String EVENT_PRIORITY_HIGH = "HIGH";

    //
    // UTC timezone
    //
    public static final ZoneId TIMEZONE_UTC = ZoneId.of("UTC");

    /**
     * Creates a new event having the minimal required fields.
     * The timestamp is taken from System.currentTimeMillis().
     * The timezone is UTC and timestamp precision is in seconds.
     *
     * @param source the event source (collectd, kernel, docker, etc.)
     * @param host   the event host
     * @param type   the event type
     * @return the new event
     */
    public static JsonObject createEvent(
            final String source,
            final String host,
            final String type) {

        return createEvent(
                System.currentTimeMillis(),
                TIMEZONE_UTC,
                source,
                host,
                type);
    }

    /**
     * Creates a new event having the minimal required fields.
     *
     * @param timestamp the event timestamp
     * @param timezone  the timezone to use for the timestamp
     * @param source    the event source (collectd, kernel, docker, etc.)
     * @param host      the event host
     * @param type      the event type
     * @return the new event
     */
    public static JsonObject createEvent(
            final long timestamp,
            final ZoneId timezone,
            final String source,
            final String host,
            final String type) {

        Preconditions.checkNotNull(timezone);
        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(host);
        Preconditions.checkNotNull(type);

        JsonObject event = new JsonObject();
        event.put(EVENT_FIELD_ID, new UUID().toString());
        event.put(EVENT_FIELD_TIMESTAMP, timestamp);
        event.put(EVENT_FIELD_TIMEZONE, timezone.getId());
        event.put(EVENT_FIELD_TYPE, type);
        event.put(EVENT_FIELD_PRIORITY, EVENT_PRIORITY_NORMAL); // Default
        event.put(EVENT_FIELD_SOURCE, source);
        event.put(EVENT_FIELD_HOST, host);
        event.put(EVENT_FIELD_TAGS, new JsonArray());
        return event;
    }

    /**
     * Creates a new metric event.
     *
     * @param timestamp the event timestamp
     * @param timezone  the timezone to use for the timestamp
     * @param source    the event source (collectd, kernel, docker, etc.)
     * @param host      the event host
     * @param dsname    the datasource name (system.memory, process.name, etc.)
     * @param dstype    the datasource type (GAUGE, COUNTER, STRING)
     * @param precision the timestamp precision (ns, us, ms, s, m or h)
     * @param subgroup  the metric subgroup (used, free, etc.)
     * @param instance  the metric instance (cpu0, cpu1, /mnt, /dev/sda1, db1, etc.)
     * @param unit      the unit of the value (kB/s, jiffies, count, etc.)
     * @param interval  the sampling interval in milliseconds
     * @param value     the value of the metric
     * @return the new event
     */
    public static JsonObject createMetricEvent(
            final long timestamp,
            final ZoneId timezone,
            final String source,
            final String host,
            final String dsname,
            final String dstype,
            final String precision,
            final String subgroup,
            final String instance,
            final String unit,
            final long interval,
            final Object value) {

        Preconditions.checkNotNull(timezone);
        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(host);
        Preconditions.checkNotNull(dsname);
        Preconditions.checkNotNull(dstype);
        Preconditions.checkNotNull(precision);
        Preconditions.checkNotNull(unit);
        Preconditions.checkNotNull(value);

        JsonObject event = new JsonObject();
        event.put(EVENT_FIELD_ID, new UUID().toString());
        event.put(EVENT_FIELD_TIMESTAMP, timestamp);
        event.put(EVENT_FIELD_TIMEZONE, timezone.getId());
        event.put(EVENT_FIELD_TYPE, EVENT_TYPE_METRIC);
        event.put(EVENT_FIELD_PRIORITY, EVENT_PRIORITY_NORMAL); // Default
        event.put(EVENT_FIELD_SOURCE, source);
        event.put(EVENT_FIELD_HOST, host);
        event.put(EVENT_FIELD_DSNAME, dsname);
        event.put(EVENT_FIELD_DSTYPE, dstype);
        event.put(EVENT_FIELD_PRECISION, precision);
        event.put(EVENT_FIELD_SUBGROUP, subgroup);
        event.put(EVENT_FIELD_INSTANCE, instance);
        event.put(EVENT_FIELD_UNIT, unit);
        event.put(EVENT_FIELD_INTERVAL, interval);
        event.put(EVENT_FIELD_VALUE, value);
        event.put(EVENT_FIELD_TAGS, new JsonArray());
        return event;
    }

    /**
     * Creates a new notification.
     * The timestamp is taken from
     * System.currentTimeMillis(). The timezone is UTC and timestamp precision
     * is in milliseconds.
     *
     * @param host     the event host
     * @param priority the event priority
     * @param title    the title of the notification
     * @param message  the message of the notification
     * @return the new event
     */
    public static JsonObject createLogEvent(
            final String host,
            final String priority,
            final String title,
            final String message) {

        return createLogEvent(
                System.currentTimeMillis(),
                TIMEZONE_UTC,
                host,
                priority,
                title,
                message);
    }

    /**
     * Creates a new log event.
     *
     * @param timestamp the event timestamp
     * @param timezone  the timezone to use for the timestamp
     * @param host      the event host
     * @param priority  the event priority
     * @param title     the title
     * @param message   the message
     * @return the new event
     */
    public static JsonObject createLogEvent(
            final long timestamp,
            final ZoneId timezone,
            final String host,
            final String priority,
            final String title,
            final String message) {

        Preconditions.checkNotNull(timezone);
        Preconditions.checkNotNull(host);
        Preconditions.checkNotNull(priority);
        Preconditions.checkNotNull(title);
        Preconditions.checkNotNull(message);

        JsonObject event = new JsonObject();
        event.put(EVENT_FIELD_ID, new UUID().toString());
        event.put(EVENT_FIELD_TIMESTAMP, timestamp);
        event.put(EVENT_FIELD_TIMEZONE, timezone.getId());
        event.put(EVENT_FIELD_TYPE, EVENT_TYPE_NOTIFICATION);
        event.put(EVENT_FIELD_HOST, host);
        event.put(EVENT_FIELD_PRIORITY, priority);
        event.put(EVENT_FIELD_TITLE, title);
        event.put(EVENT_FIELD_MESSAGE, message);
        event.put(EVENT_FIELD_TAGS, new JsonArray());
        return event;
    }

    /**
     * Creates a new batch event.
     *
     * @param events the list of events to add to the batch
     * @return the new batch event
     */
    public static JsonObject createBatchEvent(final List<JsonObject> events) {

        Preconditions.checkNotNull(events);

        JsonObject batch = new JsonObject();
        batch.put(EVENT_FIELD_ID, new UUID().toString());
        batch.put(EVENT_FIELD_TIMESTAMP, System.currentTimeMillis());
        batch.put(EVENT_FIELD_TIMEZONE, ZoneId.of("UTC").getId());
        batch.put(EVENT_FIELD_TYPE, EVENT_TYPE_BATCH);
        batch.put(EVENT_FIELD_PRIORITY, EVENT_PRIORITY_NORMAL); // Default

        JsonArray jsonEvents = new JsonArray();
        for (JsonObject event : events) {
            jsonEvents.add(event);
        }

        batch.put(EVENT_FIELD_TYPE, EVENT_TYPE_BATCH);
        batch.put(EVENT_FIELD_BATCH_EVENTS, jsonEvents);
        batch.put(EVENT_FIELD_BATCH_SIZE, events.size());
        return batch;
    }

    /**
     * Returns the event time in ISO 8601 format with timezone adjustment.
     *
     * @param event the event from which to retrieve the timestamp and timezone
     * @return the ISO 8601 formatted UTC timestamp
     */
    public static String timestamp(final JsonObject event) {
        long millis = event.getLong(EVENT_FIELD_TIMESTAMP);
        String zoneId = event.getString(EVENT_FIELD_TIMEZONE);
        Instant epoch = Instant.ofEpochMilli(millis);
        return ZonedDateTime.ofInstant(epoch, ZoneId.of(zoneId)).toString();
    }

    /**
     * Returns the current UTC date and time in ISO 8601 format.
     *
     * @return the ISO 8601 formatted UTC timestamp
     */
    public static String timestamp() {
        return timestamp(ZoneId.of("UTC"));
    }

    /**
     * Returns the current date and time in ISO 8601 format.
     *
     * @param timezone the timezone to use for the timestamp
     * @return the ISO 8601 formatted timestamp
     */
    public static String timestamp(final ZoneId timezone) {
        ZonedDateTime now = ZonedDateTime.now(timezone);
        return now.toString();
    }
}

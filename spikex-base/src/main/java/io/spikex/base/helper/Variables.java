/**
 * Copyright (c) 2015 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.base.helper;

import com.google.common.base.Strings;
import io.spikex.utils.Environment;
import io.spikex.utils.HostOs;
import io.vertx.core.json.JsonObject;
import org.apache.commons.text.StrSubstitutor;
import org.apache.commons.text.WordUtils;

import java.net.URI;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Variable resolving utility class.
 * <p>
 * This class is not thread-safe.
 *
 * @author cli
 */
public final class Variables {

    private final Map<String, String> m_systemEnv; // System env variables
    private final Map<Object, Object> m_systemProps; // System properties
    private final StrSubstitutor m_substitutor;

    private final String m_hostName;
    private final String m_spikexHome;
    private final String m_spikexConfigHome;
    private final String m_spikexDataHome;
    private final String m_spikexTempHome;

    // Built-ins
    private static final String BUILTIN_PREFIX = "#";
    private static final String BUILTIN_HOST = "#host";
    private static final String BUILTIN_DATE = "#date";
    private static final String BUILTIN_TIMESTAMP = "#timestamp";
    private static final String BUILTIN_NOW = "#now";
    private static final String BUILTIN_NOW_EXTENDED = "#now(";
    private static final String BUILTIN_ENV = "#env.";
    private static final String BUILTIN_PROP = "#prop.";
    private static final String BUILTIN_SDF = "#+";
    private static final String BUILTIN_LOOKUP = "#lookup(";
    private static final String BUILTIN_CAPITALIZE = "#capitalize(";
    private static final String BUILTIN_SPIKEX_HOME = "#spikex.home";
    private static final String BUILTIN_SPIKEX_CONFIG = "#spikex.config";
    private static final String BUILTIN_SPIKEX_DATA = "#spikex.data";
    private static final String BUILTIN_SPIKEX_TEMP = "#spikex.temp";

    private static final Pattern REGEXP_NOW
            = Pattern.compile("#now\\("
            + "([A-Z][0-9\\w\\-\\+_/]+)?,?" // Timezone
            + "([\\+\\-]?[0-9]+h)?,?" // Hours
            + "([\\+\\-]?[0-9]+m)?,?" // Minutes
            + "([\\+\\-]?[0-9]+s)?" // Seconds
            + "\\)");

    private static final Pattern REGEXP_ONE_PARAM
            = Pattern.compile("#[\\w]+\\("
            + "([\\w\\-\\.\\+~@#$%&_=]+)"
            + "\\)");

    private static final Pattern REGEXP_TWO_PARAMS
            = Pattern.compile("#[\\w]+\\("
            + "([\\w\\-\\.\\+~@#$%&_=:]+)"
            + "\\s{0,5},\\s{0,5}"
            + "([\\w\\-\\.\\+~@#$%&_=]+)"
            + "\\)");

    private static final String VAR_PREFIX = "%{";
    private static final String VAR_SUFFIX = "}"; // Must be only one character long

    private static final String SCHEME_CLASSPATH = "classpath:";

    public Variables() {

        Map<String, Object> values = new HashMap();
        m_systemEnv = new ConcurrentHashMap();
        m_systemEnv.putAll(System.getenv());

        for (Map.Entry<String, String> entry : m_systemEnv.entrySet()) {
            values.put(
                    BUILTIN_ENV + entry.getKey(),
                    entry.getValue());
        }

        m_systemProps = new ConcurrentHashMap();
        Properties props = System.getProperties();
        for (Map.Entry<Object, Object> entry : props.entrySet()) {
            m_systemProps.put(entry.getKey(), entry.getValue());
            values.put(
                    BUILTIN_PROP + entry.getKey(),
                    entry.getValue());
        }

        m_spikexHome = Environment.spikexHome();
        m_spikexConfigHome = Environment.configHome();
        m_spikexDataHome = Environment.dataHome();
        m_spikexTempHome = Environment.tempHome();
        m_hostName = HostOs.hostName();

        values.put(BUILTIN_SPIKEX_HOME, m_spikexHome);
        values.put(BUILTIN_SPIKEX_CONFIG, m_spikexConfigHome);
        values.put(BUILTIN_SPIKEX_DATA, m_spikexDataHome);
        values.put(BUILTIN_SPIKEX_TEMP, m_spikexTempHome);
        values.put(BUILTIN_HOST, m_hostName);

        m_substitutor = new StrSubstitutor(values, VAR_PREFIX, VAR_SUFFIX);
    }

    public String substitute(String str) {

        if (!Strings.isNullOrEmpty(str)) {
            str = m_substitutor.replace(str);
        }

        return str;
    }

    public URI resolveUrl(
            final Class clazz,
            final String url) {

        String resolvedUrl = url;

        if (url.startsWith(SCHEME_CLASSPATH)) {
            String path = url.substring(SCHEME_CLASSPATH.length());
            resolvedUrl = clazz.getResource(path).toExternalForm();
        }

        return URI.create(substitute(resolvedUrl));
    }

    public String resolveVar(final String strValue) {
        int pfxLen = VAR_PREFIX.length();
        int pfxPos = strValue.indexOf(VAR_PREFIX);
        int sfxPos = strValue.indexOf(VAR_SUFFIX);
        return strValue.substring(pfxPos + pfxLen, sfxPos);
    }

    public <T extends Object> T lookupMetaData(
            final JsonObject event,
            final String lookupDef,
            final Map<String, MetaData> lookupValues) {

        T value = null;

        if (!Strings.isNullOrEmpty(lookupDef)) {

            Matcher m = REGEXP_TWO_PARAMS.matcher(lookupDef);
            if (m.matches()) {

                String eventField = m.group(1);
                String lookupField = m.group(2);
                Object eventValue = event.getValue(eventField);

                if (eventValue != null
                        && eventValue instanceof String) {

                    MetaData data = lookupValues.get(eventValue); // Lookup meta data based on event field value
                    if (data != null) {
                        value = data.getValue(lookupField);
                    }
                }
            }
        }

        return value;
    }

    public <T extends Object, V extends Object> T translate(final V value) {
        return translate(null, value);
    }

    public <T extends Object, V extends Object> T translate(
            final JsonObject event,
            final V value) {

        return translate(event, value, null);
    }

    public <T extends Object, V extends Object> T translate(
            final JsonObject event,
            final V value,
            final Map<String, Map<String, Object>> lookupValues) {

        Object result = value;

        if (value != null
                && value instanceof String) {

            String strValue = (String) value;
            StringBuilder sb = new StringBuilder();

            int strLen = strValue.length();
            int pfxLen = VAR_PREFIX.length();
            int pfxPos = strValue.indexOf(VAR_PREFIX);
            int sfxPos = strValue.indexOf(VAR_SUFFIX);
            int pos = 0;

            while (pfxPos >= 0
                    && sfxPos > pfxPos) {

                String var = strValue.substring(pfxPos + pfxLen, sfxPos);
                result = resolveValue(event, var, lookupValues);

                if (result != null) {
                    if (pfxPos > 0
                            || sfxPos < (strLen - 1)) {
                        sb.append(strValue.substring(pos, pfxPos));
                        sb.append(String.valueOf(result));
                    }
                } else {
                    sb.append(strValue.substring(pos, sfxPos + 1));
                }

                if (sfxPos < (strLen - 1)) {

                    pos = sfxPos + 1;
                    pfxPos = strValue.indexOf(VAR_PREFIX, pos);
                    sfxPos = strValue.indexOf(VAR_SUFFIX, pos);

                } else {
                    pos = strLen;
                    pfxPos = -1; // break
                }
            }

            if (sb.length() > 0) {
                if (pos < strLen) {
                    sb.append(strValue.substring(pos, strLen));
                }
                result = sb.toString();
            }
        }

        return (T) (result == null ? "" : result);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("host.name: ");
        sb.append(m_hostName);
        sb.append(",spikex.home: ");
        sb.append(m_spikexHome);
        sb.append(",spikex.conf: ");
        sb.append(m_spikexConfigHome);
        sb.append(",spikex.data: ");
        sb.append(m_spikexDataHome);
        sb.append(",spikex.tmp: ");
        sb.append(m_spikexTempHome);
        sb.append(",env-vars: ");
        sb.append(m_systemEnv);
        sb.append(",sys-props: ");
        sb.append(m_systemProps);
        return sb.toString();
    }

    private Object resolveValue(
            final JsonObject event,
            final String var,
            final Map<String, Map<String, Object>> lookupValues) {

        Object value = null;

        if (var.length() > 0) {

            if (var.startsWith(BUILTIN_PREFIX)) {

                ZonedDateTime dtNow;
                DateTimeFormatter fmt;

                if (var.startsWith(BUILTIN_SDF)) {
                    // Simple date format
                    String pattern = var.substring(BUILTIN_SDF.length());
                    dtNow = ZonedDateTime.now(ZoneOffset.UTC);
                    fmt = DateTimeFormatter.ofPattern(pattern);
                    value = fmt.format(dtNow);
                } else if (var.startsWith(BUILTIN_ENV)) {
                    // env
                    Object val = m_systemEnv.get(var.substring(BUILTIN_ENV.length()));
                    value = (val != null ? String.valueOf(val) : "");
                } else if (var.startsWith(BUILTIN_PROP)) {
                    // prop
                    Object val = m_systemProps.get(var.substring(BUILTIN_PROP.length()));
                    value = (val != null ? String.valueOf(val) : "");
                } else if (var.startsWith(BUILTIN_NOW_EXTENDED)) {
                    // now extended with timezone and time offset
                    ZonedDateTime dt = Variables.createDateTimeNow(var);
                    value = dt.toInstant().toEpochMilli();
                } else if (var.startsWith(BUILTIN_LOOKUP)) {
                    // lookup value from map of values
                    Object val = lookupValue(event, var, lookupValues);
                    value = (val != null ? String.valueOf(val) : "");
                } else if (var.startsWith(BUILTIN_CAPITALIZE)) {
                    // capitalize the first letter of each word
                    value = capitalize(event, var);
                } else {

                    switch (var) {
                        case BUILTIN_SPIKEX_HOME:
                            value = m_spikexHome;
                            break;
                        case BUILTIN_SPIKEX_CONFIG:
                            value = m_spikexConfigHome;
                            break;
                        case BUILTIN_SPIKEX_DATA:
                            value = m_spikexDataHome;
                            break;
                        case BUILTIN_SPIKEX_TEMP:
                            value = m_spikexTempHome;
                            break;
                        case BUILTIN_HOST:
                            value = m_hostName;
                            break;
                        case BUILTIN_DATE:
                            dtNow = ZonedDateTime.now(ZoneOffset.UTC);
                            value = dtNow.format(DateTimeFormatter.ISO_LOCAL_DATE);
                            break;
                        case BUILTIN_TIMESTAMP:
                            dtNow = ZonedDateTime.now(ZoneOffset.UTC);
                            value = dtNow.format(DateTimeFormatter.ISO_INSTANT);
                            break;
                        case BUILTIN_NOW:
                            value = System.currentTimeMillis();
                            break;
                        default:
                            value = var; // Just return the variable def
                            break;
                    }
                }
            } else {
                //
                // Retrieve value from existing field in the event
                //
                if (event != null) {
                    value = event.getValue(var);
                }
            }
        }
        return value;
    }

    private Object lookupValue(
            final JsonObject event,
            final String var,
            final Map<String, Map<String, Object>> lookupValues) {

        Object val = null;
        Object lookupValue = null;

        // #lookup(field, lookup-map)
        Matcher m = REGEXP_TWO_PARAMS.matcher(var);
        if (m.matches()) {

            String field = m.group(1);
            String lookupMap = m.group(2);

            // Consider only substring of field value
            int len = 0;
            int n = field.indexOf(':');
            if (n != -1) {
                len = Integer.parseInt(field.substring(n + 1));
                field = field.substring(0, n);
            }

            val = event.getValue(field);
            Map<String, Object> values = lookupValues.get(lookupMap);

            if (val != null
                    && values != null) {
                if (len == 0) {
                    lookupValue = values.get(val);
                } else {
                    // Support "starts-with" lookups
                    String strVal = String.valueOf(val);
                    lookupValue = values.get(strVal.substring(0, len));
                }
            }
        }

        return lookupValue != null ? lookupValue : val;
    }

    private String capitalize(
            final JsonObject event,
            final String var) {

        String val = "";

        // #capitalize(field)
        Matcher m = REGEXP_ONE_PARAM.matcher(var);

        boolean found = m.find();
        if (found) {

            String field = m.group(1);
            Object value = event.getValue(field);

            if (value != null
                    && value instanceof String) {
                val = WordUtils.capitalizeFully((String) value);
            }
        }

        return val;
    }

    public static ZonedDateTime createDateTimeNow(final String var) {

        ZonedDateTime dt;

        // #now
        // #now(UTC)
        // #now(UTC,0h,-10m,0s)
        // #now(0h,-10m,0s)
        // #now(30m)
        Matcher m = REGEXP_NOW.matcher(var);
        //
        // Timezone
        //
        String tz = null;
        boolean found = m.find();
        if (found) {
            tz = m.group(1);
        }
        //
        // Hours
        //
        int hours = 0;
        if (found) {
            String val = m.group(2);
            if (val != null) {
                hours = Integer.parseInt(val.substring(0, val.length() - 1));
            }
        }
        //
        // Minutes
        //
        int mins = 0;
        if (found) {
            String val = m.group(3);
            if (val != null) {
                mins = Integer.parseInt(val.substring(0, val.length() - 1));
            }
        }
        //
        // Seconds
        //
        int secs = 0;
        if (found) {
            String val = m.group(4);
            if (val != null) {
                secs = Integer.parseInt(val.substring(0, val.length() - 1));
            }
        }

        // System.out.println("var: " + var + " tz: " + tz + " hours: " + hours + " mins: " + mins + " secs: " + secs);

        if (tz != null) {
            dt = ZonedDateTime.now(ZoneId.of(tz));
        } else {
            dt = ZonedDateTime.now(ZoneOffset.UTC);
        }

        dt = dt.plusHours(hours);
        dt = dt.plusMinutes(mins);
        dt = dt.plusSeconds(secs);

        return dt;
    }
}

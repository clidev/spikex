/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.base.helper;

import io.spikex.base.SpikexContext;
import io.vertx.core.AsyncResult;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author cli
 */
public class VerticleHelper {

    public static final String KEY_CONFIGS = "configs";

    private static AtomicReference<SpikexContext> CONTEXT = new AtomicReference(new SpikexContext());

    private static final Logger LOG = LoggerFactory.getLogger(VerticleHelper.class);

    public static void startVerticle(
            final String[] args,
            final String verticleClass) {

        startVerticle(args, verticleClass, new DeploymentOptions());
    }

    public static void startVerticle(
            final String[] args,
            final String verticleClass,
            final DeploymentOptions opts) {

        startVerticle(
                args,
                verticleClass,
                opts,
                res -> {
                    if (res.succeeded()) {
                        LOG.trace("Verticle started ({})", res.result());
                    } else {
                        LOG.error("Failed to start {}", verticleClass, res.cause());
                    }
                });
    }

    public static void startVerticle(
            final String[] args,
            final String verticleClass,
            final DeploymentOptions opts,
            final Handler<AsyncResult<String>> handler) {

        LOG.debug("Starting verticle: {} args: {}", verticleClass, Arrays.asList(args));

        // Start verticle
        if (args != null && args.length > 0) {
            JsonObject config = new JsonObject(args[0]); // Environment paths
            if (args.length > 1) {
                config.put(KEY_CONFIGS, new JsonArray(args[1])); // Module configs
            }
            opts.setConfig(config);
        }
        container().vertx().deployVerticle(verticleClass, opts, handler);
    }

    public static SpikexContext.VertxContainer container() {
        return CONTEXT.get().vertxContainer();
    }
}

/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.base.helper;

import io.spikex.utils.IBuilder;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.Shareable;

import java.io.Serializable;

/**
 *
 */
public final class HostMetaData implements MetaData, Shareable, Serializable {

    private static final long serialVersionUID = 113L;

    public static final String EVENT_FIELD_HOST = "@host";
    public static final String EVENT_FIELD_HOST_ID = "@host.id";
    public static final String EVENT_FIELD_HOST_DESC = "@host.desc";
    public static final String EVENT_FIELD_HOST_ADDRESS = "@host.address";
    public static final String EVENT_FIELD_HOST_PUBLICIP = "@host.publicip";
    public static final String EVENT_FIELD_HOST_HW = "@host.hw";
    public static final String EVENT_FIELD_HOST_OS = "@host.os";
    public static final String EVENT_FIELD_HOST_ZONE = "@zone";
    public static final String EVENT_FIELD_HOST_REGION = "@region";
    public static final String EVENT_FIELD_HOST_ENV = "@env";

    public static final String DSNAME_HOST_META_DATA = "host_meta_data";
    public static final String SHARED_DATA_MAP = HostMetaData.class.getName();

    private final String m_id; // Unique identifier
    private final String m_hostName; // Hostname
    private final String m_desc; // Free-form description
    private final String m_address; // IP or DNS address
    private final String m_publicip; // Public IP
    private final String m_hw;
    private final String m_os;
    private final String m_zone; // Geo zone
    private final String m_region; // Geo region
    private final String m_env; // Test, Staging, Production, etc..

    private HostMetaData(final Builder builder) {
        m_id = builder.m_id;
        m_hostName = builder.m_hostName;
        m_desc = builder.m_desc;
        m_address = builder.m_address;
        m_publicip = builder.m_publicip;
        m_hw = builder.m_hw;
        m_os = builder.m_os;
        m_zone = builder.m_zone;
        m_region = builder.m_region;
        m_env = builder.m_env;
    }

    @Override
    public Object getValue(final String field) {

        Object value;

        switch (field) {
            case EVENT_FIELD_HOST:
                value = getHostName();
                break;
            case EVENT_FIELD_HOST_ID:
                value = getId();
                break;
            case EVENT_FIELD_HOST_DESC:
                value = getDescription();
                break;
            case EVENT_FIELD_HOST_ADDRESS:
                value = getAddress();
                break;
            case EVENT_FIELD_HOST_PUBLICIP:
                value = getPublicIp();
                break;
            case EVENT_FIELD_HOST_HW:
                value = getHw();
                break;
            case EVENT_FIELD_HOST_OS:
                value = getOs();
                break;
            case EVENT_FIELD_HOST_ZONE:
                value = getZone();
                break;
            case EVENT_FIELD_HOST_REGION:
                value = getRegion();
                break;
            case EVENT_FIELD_HOST_ENV:
                value = getEnv();
                break;
            default:
                value = "";
        }

        return value;
    }

    public String getId() {
        return m_id;
    }

    public String getHostName() {
        return m_hostName;
    }

    public String getDescription() {
        return m_desc;
    }

    public String getAddress() {
        return m_address;
    }

    public String getPublicIp() {
        return m_publicip;
    }

    public String getHw() {
        return m_hw;
    }

    public String getOs() {
        return m_os;
    }

    public String getZone() {
        return m_zone;
    }

    public String getRegion() {
        return m_region;
    }

    public String getEnv() {
        return m_env;
    }

    @Override
    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        json.put(EVENT_FIELD_HOST, getHostName());
        json.put(EVENT_FIELD_HOST_ID, getId());
        json.put(EVENT_FIELD_HOST_DESC, getDescription());
        json.put(EVENT_FIELD_HOST_ADDRESS, getAddress());
        json.put(EVENT_FIELD_HOST_PUBLICIP, getPublicIp());
        json.put(EVENT_FIELD_HOST_HW, getHw());
        json.put(EVENT_FIELD_HOST_OS, getOs());
        json.put(EVENT_FIELD_HOST_ZONE, getZone());
        json.put(EVENT_FIELD_HOST_REGION, getRegion());
        json.put(EVENT_FIELD_HOST_ENV, getEnv());
        return json;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder implements IBuilder<HostMetaData> {

        private String m_id;
        private String m_hostName;
        private String m_desc;
        private String m_address;
        private String m_publicip;
        private String m_hw;
        private String m_os;
        private String m_zone;
        private String m_region;
        private String m_env;

        private Builder() {
            m_id = "";
            m_hostName = "";
            m_desc = "";
            m_address = "";
            m_publicip = "";
            m_hw = "";
            m_os = "";
            m_zone = "";
            m_region = "";
            m_env = "";
        }

        public Builder withId(final String id) {
            m_id = id;
            return this;
        }

        public Builder withDescription(final String desc) {
            m_desc = desc;
            return this;
        }

        public Builder withHostName(final String hostName) {
            m_hostName = hostName;
            return this;
        }

        public Builder withAddress(final String address) {
            m_address = address;
            return this;
        }

        public Builder withPublicIp(final String publicip) {
            m_publicip = publicip;
            return this;
        }

        public Builder withHw(final String hw) {
            m_hw = hw;
            return this;
        }

        public Builder withOs(final String os) {
            m_os = os;
            return this;
        }

        public Builder withZone(final String zone) {
            m_zone = zone;
            return this;
        }

        public Builder withRegion(final String region) {
            m_region = region;
            return this;
        }

        public Builder withEnv(final String env) {
            m_env = env;
            return this;
        }

        @Override
        public HostMetaData build() {
            return new HostMetaData(this);
        }
    }
}
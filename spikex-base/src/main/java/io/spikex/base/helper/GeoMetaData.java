/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.base.helper;

import io.spikex.utils.IBuilder;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.Shareable;

import java.io.Serializable;

public class GeoMetaData implements MetaData, Shareable, Serializable {

    private static final long serialVersionUID = 2039L;

    public static final String EVENT_FIELD_GEO_HASH = "@geo.hash";
    public static final String EVENT_FIELD_GEO_TYPE = "@geo.type";
    public static final String EVENT_FIELD_GEO_ADDRESS = "@geo.address";
    public static final String EVENT_FIELD_GEO_COUNTRY = "@geo.country";
    public static final String EVENT_FIELD_GEO_CITY = "@geo.city";
    public static final String EVENT_FIELD_GEO_AREA = "@geo.area";
    public static final String EVENT_FIELD_GEO_LATITUDE = "@geo.latitude";
    public static final String EVENT_FIELD_GEO_LONGITUDE = "@geo.longitude";

    public static final String SHARED_DATA_MAP = GeoMetaData.class.getName();

    private final String m_hash; // Geohash (https://en.wikipedia.org/wiki/Geohash)
    private final String m_type; // GeoIP2, ...
    private final String m_address; // public IP, street address
    private final String m_country; // ISO 3166-1 alpha-2
    private final String m_city; // City name
    private final String m_area; // Geographical area (state, municipality, town, etc..)
    private final Double m_latitude;
    private final Double m_longitude;
    private final long m_creationTimestamp;

    private GeoMetaData(final GeoMetaData.Builder builder) {
        m_hash = builder.m_hash;
        m_type = builder.m_type;
        m_address = builder.m_address;
        m_country = builder.m_country;
        m_city = builder.m_city;
        m_area = builder.m_area;
        m_latitude = builder.m_latitude;
        m_longitude = builder.m_longitude;
        m_creationTimestamp = System.currentTimeMillis();
    }

    @Override
    public Object getValue(final String field) {

        Object value;

        switch (field) {
            case EVENT_FIELD_GEO_HASH:
                value = getHash();
                break;
            case EVENT_FIELD_GEO_TYPE:
                value = getType();
                break;
            case EVENT_FIELD_GEO_ADDRESS:
                value = getAddress();
                break;
            case EVENT_FIELD_GEO_COUNTRY:
                value = getCountry();
                break;
            case EVENT_FIELD_GEO_CITY:
                value = getCity();
                break;
            case EVENT_FIELD_GEO_AREA:
                value = getArea();
                break;
            case EVENT_FIELD_GEO_LATITUDE:
                value = getLatitude();
                break;
            case EVENT_FIELD_GEO_LONGITUDE:
                value = getLongitude();
                break;
            default:
                value = "";
        }

        return value;
    }

    public String getHash() {
        return m_hash;
    }

    public String getType() {
        return m_type;
    }

    public String getAddress() {
        return m_address;
    }

    public String getCountry() {
        return m_country;
    }

    public String getCity() {
        return m_city;
    }

    public String getArea() {
        return m_area;
    }

    public Double getLatitude() {
        return m_latitude;
    }

    public Double getLongitude() {
        return m_longitude;
    }

    public long getCreationTimestamp() {
        return m_creationTimestamp;
    }

    @Override
    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        json.put(EVENT_FIELD_GEO_HASH, getHash());
        json.put(EVENT_FIELD_GEO_TYPE, getType());
        json.put(EVENT_FIELD_GEO_ADDRESS, getAddress());
        json.put(EVENT_FIELD_GEO_COUNTRY, getCountry());
        json.put(EVENT_FIELD_GEO_CITY, getCity());
        json.put(EVENT_FIELD_GEO_AREA, getArea());
        json.put(EVENT_FIELD_GEO_LATITUDE, getLatitude());
        json.put(EVENT_FIELD_GEO_LONGITUDE, getLongitude());
        return json;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder implements IBuilder<GeoMetaData> {

        private String m_hash;
        private String m_type;
        private String m_address;
        private String m_country;
        private String m_city;
        private String m_area;
        private Double m_latitude;
        private Double m_longitude;

        private Builder() {
            m_hash = "";
            m_type = "";
            m_address = "";
            m_country = "";
            m_city = "";
            m_area = "";
            m_latitude = 0.0d;
            m_longitude = 0.0d;
        }

        public Builder withHash(final String hash) {
            m_hash = hash;
            return this;
        }

        public Builder withType(final String type) {
            m_type = type;
            return this;
        }

        public Builder withAddress(final String address) {
            m_address = address;
            return this;
        }

        public Builder withCountry(final String country) {
            m_country = country;
            return this;
        }

        public Builder withCity(final String city) {
            m_city = city;
            return this;
        }

        public Builder withArea(final String area) {
            m_area = area;
            return this;
        }

        public Builder withLatitude(final Double latitude) {
            m_latitude = latitude;
            return this;
        }

        public Builder withLongitude(final Double longitude) {
            m_longitude = longitude;
            return this;
        }

        @Override
        public GeoMetaData build() {
            return new GeoMetaData(this);
        }
    }
}

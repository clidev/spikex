/**
 * Copyright (c) 2016 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.utils;

import com.google.common.base.Strings;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * Helper class for common environment variables.
 *
 * @author cli
 */
public final class Environment {

    public static final String SCHEME_HTTP = "http";
    public static final String SCHEME_HTTPS = "https";

    private static final String SPIKEX_HOME = "spikex.home";
    private static final String SPIKEX_CONFIG_FILENAME = "spikex.config.filename";
    private static final String SPIKEX_CONFIG_HOME = "spikex.config.home";
    private static final String SPIKEX_MODULES_HOME = "spikex.modules.home";
    private static final String SPIKEX_DEPLOY_HOME = "spikex.deploy.home";
    private static final String SPIKEX_DATA_HOME = "spikex.data.home";
    private static final String SPIKEX_TEMP_HOME = "spikex.temp.home";
    private static final String SPIKEX_JVM_RESTART_EXIT_CODE = "spikex.jvm.restart.exit.code";

    // Defaults
    private static final String DEFAULT_CONFIG_FILENAME = "spikex.json";
    private static final String DEFAULT_CONFIG_DIR = "config";
    private static final String DEFAULT_MODULES_DIR = "modules";
    private static final String DEFAULT_DEPLOY_DIR = "deploy";
    private static final String DEFAULT_DATA_DIR = "data";
    private static final String DEFAULT_TEMP_DIR = "temp";
    private static final String DEFAULT_JVM_RESTART_EXIT_CODE = "123";

    // These are set on startup / first class load
    private static final Map<String, Object> VALUES;

    private static final Logger LOG = LoggerFactory.getLogger(Environment.class);

    static {

        // Fail-fast - spikex.home must be set
        if (!isSetSpikexHome()) {
            System.err.println("spikex.home system property must be set");
            System.exit(1);
        }

        String home = System.getProperty(SPIKEX_HOME);
        Path spikexHome = Paths.get(home).normalize().toAbsolutePath();

        VALUES = new HashMap<>();
        VALUES.put(SPIKEX_HOME, spikexHome.toString());
        VALUES.put(SPIKEX_CONFIG_FILENAME, System.getProperty(SPIKEX_CONFIG_FILENAME,
                DEFAULT_CONFIG_FILENAME));

        VALUES.put(SPIKEX_CONFIG_HOME, System.getProperty(SPIKEX_CONFIG_HOME,
                spikexHome.resolve(DEFAULT_CONFIG_DIR).normalize().toString()));

        VALUES.put(SPIKEX_MODULES_HOME, System.getProperty(SPIKEX_MODULES_HOME,
                spikexHome.resolve(DEFAULT_MODULES_DIR).normalize().toString()));

        VALUES.put(SPIKEX_DEPLOY_HOME, System.getProperty(SPIKEX_DEPLOY_HOME,
                spikexHome.resolve(DEFAULT_DEPLOY_DIR).normalize().toString()));

        VALUES.put(SPIKEX_DATA_HOME, System.getProperty(SPIKEX_DATA_HOME,
                spikexHome.resolve(DEFAULT_DATA_DIR).normalize().toString()));

        VALUES.put(SPIKEX_TEMP_HOME, System.getProperty(SPIKEX_TEMP_HOME,
                spikexHome.resolve(DEFAULT_TEMP_DIR).normalize().toString()));

        VALUES.put(SPIKEX_JVM_RESTART_EXIT_CODE,
                Integer.parseInt(System.getProperty(SPIKEX_JVM_RESTART_EXIT_CODE,
                        DEFAULT_JVM_RESTART_EXIT_CODE)));

        LOG.info("Environment: {}", VALUES);
    }

    public static boolean isSetSpikexHome() {
        return !Strings.isNullOrEmpty(System.getProperty(SPIKEX_HOME));
    }

    public static String spikexHome() {
        return (String) VALUES.get(SPIKEX_HOME);
    }

    public static String configFilename() {
        return (String) VALUES.get(SPIKEX_CONFIG_FILENAME);
    }

    public static String configHome() {
        return (String) VALUES.get(SPIKEX_CONFIG_HOME);
    }

    public static String modulesHome() {
        return (String) VALUES.get(SPIKEX_MODULES_HOME);
    }

    public static String deployHome() {
        return (String) VALUES.get(SPIKEX_DEPLOY_HOME);
    }

    public static String dataHome() {
        return (String) VALUES.get(SPIKEX_DATA_HOME);
    }

    public static String tempHome() {
        return (String) VALUES.get(SPIKEX_TEMP_HOME);
    }

    public static int jvmRestartExitCode() {
        return (Integer) VALUES.get(SPIKEX_JVM_RESTART_EXIT_CODE);
    }

    public static JsonObject asJson() {
        return new JsonObject()
                .put(SPIKEX_HOME, spikexHome())
                .put(SPIKEX_CONFIG_FILENAME, configFilename())
                .put(SPIKEX_CONFIG_HOME, configHome())
                .put(SPIKEX_MODULES_HOME, modulesHome())
                .put(SPIKEX_DEPLOY_HOME, deployHome())
                .put(SPIKEX_DATA_HOME, dataHome())
                .put(SPIKEX_TEMP_HOME, tempHome());
    }

    public static String asString() {
        return asJson().encode();
    }
}

/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.utils;

import com.google.common.base.Strings;
import com.google.common.io.CharStreams;
import com.google.common.io.Resources;
import io.vertx.core.json.JsonArray;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URI;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static io.spikex.utils.Environment.SCHEME_HTTP;
import static io.spikex.utils.Environment.SCHEME_HTTPS;

/**
 * @author cli
 */
public final class Files {

    private static final int DOWNLOAD_CHUNK_SIZE = 2048;
    private static final long DOWNLOAD_MAX_FILE_SIZE = 0x6400000; // 100 MB
    private static final int UNZIP_BUFFER_SIZE = 8192;
    private static final long UNZIP_MAX_EXTRACT_SIZE = 0x6400000; // 100 MB
    private static final int UNZIP_MAX_ENTRY_COUNT = 1024;

    public static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");

    public static final String HEADER_USER_AGENT = "User-Agent";
    public static final String HEADER_ACCEPT = "Accept";

    public static final String USER_AGENT_ID = "Spike.x";

    private static final String SCHEME_CLASSPATH = "classpath";
    private static final String SCHEME_FILE = "file";

//    private static final long XX_HASH_SEED = 29572L;

    private static final Logger LOG = LoggerFactory.getLogger(Files.class);

    /**
     * Read and parse JSON file.
     *
     * @param file
     * @return the parsed JSON file as JsonArray or null
     */
    public static JsonArray readJsonArray(final Path file) {

        JsonArray json = null;
        LOG.debug("Reading JSON file: {}", file);

        try (Reader reader = java.nio.file.Files.newBufferedReader(file)) {
            json = new JsonArray(CharStreams.toString(reader));
        } catch (IOException e) {
            LOG.error("Failed to read JSON file: {}", file, e);
        }

        return json;
    }

    public static String calculateHash(final Path file) throws IOException {

        MessageDigest md;

        try {
            md = MessageDigest.getInstance("MD5");
            md.update(java.nio.file.Files.readAllBytes(file));
        } catch (NoSuchAlgorithmException e) {
            throw new IOException("Failed to calculate hash for file: " + file, e);
        }

        return DatatypeConverter.printHexBinary(md.digest());
    }

    /**
     * Copy source file to destination file.
     *
     * @param srcFile   the local file
     * @param destFile  the destination file
     * @param overwrite overwrite existing destination file
     */
    public static void copyFile(
            final URI srcFile,
            final Path destFile,
            final boolean overwrite) throws IOException {

        if (!java.nio.file.Files.exists(destFile)) {

            LOG.info("Copying {} to {}", srcFile, destFile);
            java.nio.file.Files.createDirectories(destFile.getParent());
            replaceFile(srcFile, destFile);

        } else {
            if (overwrite) {
                LOG.info("Replacing {} with {}", destFile, srcFile);
                replaceFile(srcFile, destFile);
            }
        }
    }

    /**
     * Download remote or copy file to local directory.
     *
     * @param srcFile   the remote file
     * @param destFile  the destination file
     * @param overwrite overwrite existing destination file
     */
    public static void downloadOrCopy(
            final URI srcFile,
            final Path destFile,
            final boolean overwrite) throws IOException {

        downloadOrCopy(srcFile, destFile, overwrite, null);
    }

    /**
     * Download remote or copy file to local directory.
     *
     * @param srcFile   the remote file
     * @param destFile  the destination file
     * @param overwrite overwrite existing destination file
     * @param headers   HTTP headers
     */
    public static void downloadOrCopy(
            final URI srcFile,
            final Path destFile,
            final boolean overwrite,
            final Headers headers) throws IOException {

        if (SCHEME_HTTP.equalsIgnoreCase(srcFile.getScheme())
                || SCHEME_HTTPS.equalsIgnoreCase(srcFile.getScheme())) {
            download(srcFile, destFile, overwrite, headers);
        } else {
            copyFile(srcFile, destFile, overwrite);
        }
    }

    /**
     * Download remote file to local directory.
     *
     * @param srcFile   the remote file
     * @param destFile  the destination file
     * @param overwrite overwrite existing destination file
     */
    public static void download(
            final URI srcFile,
            final Path destFile,
            final boolean overwrite) throws IOException {

        download(srcFile, destFile, overwrite, null);
    }

    /**
     * Download remote file to local directory.
     *
     * @param srcFile   the remote file
     * @param destFile  the destination file
     * @param overwrite overwrite existing destination file
     * @param headers   HTTP headers
     */
    public static void download(
            final URI srcFile,
            final Path destFile,
            final boolean overwrite,
            Headers headers) throws IOException {

        LOG.info("Downloading {} to {}", srcFile, destFile);

        OkHttpClient client = new OkHttpClient();

        if (headers == null) {
            headers = new Headers.Builder()
                    .set(HEADER_USER_AGENT, USER_AGENT_ID)
                    .build();
        }

        Request request = new Request.Builder()
                .url(srcFile.toURL())
                .headers(headers)
                .build();

        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {

            if (!java.nio.file.Files.exists(destFile)
                    || overwrite) {

                java.nio.file.Files.createDirectories(destFile.getParent());
                BufferedSource source = response.body().source();
                BufferedSink sink = Okio.buffer(Okio.sink(destFile));

                long total = 0L;
                long count;
                while (total + DOWNLOAD_CHUNK_SIZE <= DOWNLOAD_MAX_FILE_SIZE
                        && (count = source.read(sink.buffer(), DOWNLOAD_CHUNK_SIZE)) != -1) {

                    total += count;
                    sink.emit();
                }
                sink.close();

                //
                // Sanity check
                //
                if (total > DOWNLOAD_MAX_FILE_SIZE) {
                    throw new IllegalStateException("Aborted download of "
                            + srcFile + ". Total file size is larger than 100 MB.");
                }
            } else {
                LOG.trace("Not replacing existing file: {}", destFile);
            }
        } else {
            throw new IOException("Download failure: "
                    + response.code()
                    + ":"
                    + response.message());
        }
    }

    /**
     * Unzip file to given destination directory.
     *
     * @param file    the Zip file
     * @param destDir the destination directory
     */
    public static void unzip(
            final Path file,
            final Path destDir) {

        long total = 0L;
        byte[] buffer = new byte[UNZIP_BUFFER_SIZE];

        LOG.debug("Unpacking {} to {}", file, destDir);

        try (ZipFile zip = new ZipFile(file.toFile().getAbsolutePath())) {
            Enumeration<? extends ZipEntry> entries = zip.entries();
            for (int i = 0; entries.hasMoreElements(); i++) {
                //
                // Sanity checks
                //
                ZipEntry entry = entries.nextElement();
                if (i >= UNZIP_MAX_ENTRY_COUNT) {
                    throw new IllegalStateException("Aborting unpacking of "
                            + file + ". It has over 1024 entries.");
                }
                String name = entry.getName();
                LOG.trace("Inspecting entry: {}", name);
                if (!validName(name)) {
                    throw new IllegalStateException("Aborting unpacking of "
                            + file + ". Entry name is not valid or outside of target directory: "
                            + name);
                }

                if (entry.isDirectory()) {
                    java.nio.file.Files.createDirectories(destDir.resolve(name));
                } else {
                    total = extractFile(
                            destDir,
                            zip,
                            entry,
                            buffer,
                            total);
                    //
                    // Sanity check
                    //
                    if (total > UNZIP_MAX_EXTRACT_SIZE) {
                        throw new IllegalStateException("Aborting unpacking of "
                                + file + ". Total extracted size is larger than 100 MB.");
                    }
                }
            }
        } catch (IOException e) {
            LOG.error("Failed to process zip: {}", file, e);
        }
    }

    private static void replaceFile(
            final URI srcFile,
            final Path destFile) throws IOException {

        String scheme = srcFile.getScheme();
        if (!Strings.isNullOrEmpty(scheme)
                && !SCHEME_FILE.equals(scheme)) {

            URL srcUrl;

            // Handle unofficial classpath scheme
            if (SCHEME_CLASSPATH.equals(scheme)) {
                srcUrl = Resources.getResource(srcFile.toString().substring(
                        SCHEME_CLASSPATH.length() + 1));
            } else {
                srcUrl = srcFile.toURL();
            }

            Resources.asByteSource(srcUrl).copyTo(
                    com.google.common.io.Files.asByteSink(destFile.toFile()));

        } else {
            java.nio.file.Files.copy(
                    Paths.get(srcFile),
                    destFile);
        }
    }

    private static long extractFile(
            final Path destDir,
            final ZipFile zip,
            final ZipEntry entry,
            final byte[] buffer,
            long total) {

        String name = entry.getName();
        File destFile = destDir.resolve(name).toFile();

        try (FileOutputStream fileOut = new FileOutputStream(destFile);
             BufferedOutputStream bufOut = new BufferedOutputStream(fileOut);
             InputStream zipIn = zip.getInputStream(entry)) {

            int count;
            while (total + UNZIP_BUFFER_SIZE <= UNZIP_MAX_EXTRACT_SIZE
                    && (count = zipIn.read(buffer, 0, UNZIP_BUFFER_SIZE)) != -1) {
                bufOut.write(buffer, 0, count);
                total += count;
            }
        } catch (IOException e) {
            LOG.error("Failed to process entry: {}", name, e);
        }

        return total;
    }

    private static boolean validName(final String name) {

        boolean valid = false;
        try {
            String entryUniquePath = Paths.get(name).toFile().getCanonicalPath();
            String refUniquePath = Paths.get(".").toFile().getCanonicalPath();

            if (entryUniquePath.startsWith(refUniquePath)) {
                valid = true;
            }
        } catch (IOException e) {
            LOG.error("Failed to validateModule name: {}", name, e);
        }

        return valid;
    }
}

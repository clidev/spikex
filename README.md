# Spike.x #

Reactive event monitoring and data analysis built on top of Vert.x.

Spike.x provides components for resource monitoring, for data filtering and streaming, for sending of notifications and for storing of metrics and events in various backends.

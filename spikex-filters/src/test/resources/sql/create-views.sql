CREATE OR REPLACE VIEW test_view1 AS
SELECT
  'zeus.space.tld' AS "host",
  'schema_setting_' || LCASE(name) AS "dsname",
  'STRING' AS "dstype",
  value
FROM INFORMATION_SCHEMA.SETTINGS;

CREATE OR REPLACE VIEW test_view2 AS
SELECT
  'jupiter.space.tld' AS "host",
  'schema_help_' || TRANSLATE(LCASE(topic), ' ', '_') AS "dsname",
  'STRING' AS "dstype",
  text AS "value"
FROM INFORMATION_SCHEMA.HELP;

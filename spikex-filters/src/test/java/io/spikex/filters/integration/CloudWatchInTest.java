/**
 * Copyright (c) 2017 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.filters.integration;

import java.util.concurrent.CountDownLatch;

/**
 *
 */
public final class CloudWatchInTest {

    private static final String MODULE_ID = "io.spikex.filters";
    private static final String CONFIG_FILE = "cloudwatch-test.json";

    private static final CountDownLatch LATCH = new CountDownLatch(1);
/*
    @BeforeClass
    public static void startup() {
        TestUtils.startFilterChain(
                MODULE_ID,
                CONFIG_FILE,
                LATCH,
                HostOs.isMac() || HostOs.isLinux());
    }

    @Test(timeout = 120000)
    public void testFilter() throws Exception {
        LATCH.await();
        Thread.sleep(120000L);
    }
    */
}

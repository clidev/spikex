/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.integration;

import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * @author cli
 */
public final class StatsdInTest {

    private static final String MODULE_ID = "io.spikex.filters";
    private static final String CONFIG_FILE = "statsd-test.json";

    private static final CountDownLatch LATCH = new CountDownLatch(1);

    @BeforeClass
    public static void startup() {
        TestUtils.startFilterChain(
                MODULE_ID,
                CONFIG_FILE,
                LATCH,
                true);
    }

    @Test(timeout = 1500)
    public void testUdpBasic() throws Exception {

        LATCH.await();
        int port = 8125;
        StatsDClient client = new NonBlockingStatsDClient(
                "test",
                "127.0.0.1",
                port,
                new String[]{"env:Testing"});

        for (int i = 0; i < 20; i++) {
            client.incrementCounter("counter1", "@subgroup:tx");
            client.incrementCounter("counter2");
            client.count("count1", 128923L, "@instance:eth0", "@subgroup:packets");
            client.increment("count1");
            client.decrement("count1");
            client.decrementCounter("counter2");
            client.gauge("gauge1", 12723.233d);
            client.gauge("gauge2", 0L);
            client.gauge("gauge3", 4327);
            client.recordGaugeValue(
                    "gaugeval1", // aspect
                    3.023d, // value
                    0.1d, // sample rate
                    "env:Production", // tag 1
                    "instance:cpu1", // tag 2
                    "subgroup:load1m", // tag 3
                    "unit:percent"); // tag 4
            client.recordExecutionTime("query1", 25, "database:stats");
        }

        Thread.sleep(1000L);
    }

    @Test(timeout = 1500)
    public void testUdpNsq() throws Exception {

        LATCH.await();
        int port = 8125;
        StatsDClient client = new NonBlockingStatsDClient(
                "nsq",
                "127.0.0.1",
                port,
                new String[]{"env:Staging"});

        for (int i = 0; i < 20; i++) {
            // NSQ
            client.gauge("zeus_4150.topic.mytopic-1287231.channel.mychannel_red-128823.depth", 10.0d);
            client.gauge("zeus_4150.topic.mytopic-1287231.channel.mychannel_red-128823.depth", 10.0d);
            client.gauge("zeus_4150.topic.mytopic-1287231.channel.mychannel_red-128823.depth", 10.0d);

            client.count("zeus_4150.topic.mytopic-1287231.message_count", 132);
            client.count("zeus_4150.topic.mytopic-1287231.message_count", 156);
            client.count("zeus_4150.topic.mytopic-1287231.message_count", 89); // 377

            client.gauge("zeus_4150.mem.heap_objects", 278234112);
        }

        Thread.sleep(1000L);
    }
}

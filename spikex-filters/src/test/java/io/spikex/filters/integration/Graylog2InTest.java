/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.integration;

import io.spikex.base.helper.VerticleHelper;
import io.spikex.filters.input.Graylog2Filter;
import io.vertx.core.DeploymentOptions;
import org.graylog2.gelfclient.*;
import org.graylog2.gelfclient.transport.GelfTransport;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.concurrent.CountDownLatch;

/**
 * @author cli
 */
public final class Graylog2InTest {

    private static final String MODULE_ID = "io.spikex.filters";
    private static final String CONFIG_FILE = "graylog2-test.json";

    private static final CountDownLatch LATCH = new CountDownLatch(1);

    private static final Logger LOG = LoggerFactory.getLogger(Graylog2InTest.class);

    @BeforeClass
    public static void startup() {

        // Deploy filter
        DeploymentOptions opts = new DeploymentOptions();
        opts.setWorker(true);
        VerticleHelper.startVerticle(new String[0], Graylog2Filter.class.getName(), opts);

        // vertx.createDatagramSocket() in Graylog2Filter is slow
        // ... that's why we use the long timeout in testUdp
        TestUtils.startFilterChain(
                MODULE_ID,
                CONFIG_FILE,
                LATCH,
                true);
    }

    @Test(timeout = 5500)
    public void testUdp() throws Exception {

        LATCH.await();
        int port = 35823;
        InetAddress address = InetAddress.getLoopbackAddress();

        final GelfConfiguration config = new GelfConfiguration(new InetSocketAddress(address, port))
                .transport(GelfTransports.UDP)
                .queueSize(1)
                .maxInflightSends(1)
                .sendBufferSize(-1)
                .connectTimeout(1500)
                .reconnectDelay(200)
                .tcpNoDelay(true);

        final GelfTransport transport = GelfTransports.create(config);
        final GelfMessageBuilder builder = new GelfMessageBuilder("", "ip-172-21-32-103")
                .level(GelfMessageLevel.INFO)
                .additionalField("_service", "ComplexMathEngine")
                .additionalField("_priority", "1")
                .additionalField("_env", "staging");

        for (int i = 0; i < 100; i++) {
            final GelfMessage message = builder
                    .message("Testing graylog2 filter. This is message #" + i)
                    .fullMessage("The full message is usually longer than this.")
                    .additionalField("_count", i)
                    .build();

            // Blocks until there is capacity in the queue
            transport.send(message);
        }
    }
}

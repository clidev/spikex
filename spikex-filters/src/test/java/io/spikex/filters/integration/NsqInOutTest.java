/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.integration;

import io.spikex.base.helper.Events;
import io.spikex.base.helper.VerticleHelper;
import io.spikex.utils.HostOs;
import io.spikex.utils.process.ChildProcess;
import io.spikex.utils.process.DefaultProcessHandler;
import io.spikex.utils.process.ProcessExecutor;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static io.spikex.base.helper.Events.EVENT_FIELD_TAGS;
import static io.spikex.filters.integration.TestUtils.deleteFolder;

/**
 * @author cli
 */
public final class NsqInOutTest {

    private static List<ChildProcess> m_processes = new ArrayList();

    private static final Path TMP_PATH = Paths.get("/tmp/nsq");
    private static final String OUTPUT_ADDRESS = "nsq-in-out-test-Mutate";

    private static final String FIELD_NSQ_TOPIC = "@nsq.topic";
    private static final String NAME_NSQ_TOPIC = "tampere812.metrics.host.urho";

    private static final String MODULE_ID = "io.spikex.filters";
    private static final String CONFIG_FILE = "nsq-in-out-test.json";

    private static final CountDownLatch LATCH = new CountDownLatch(1);

    private static final Logger LOG = LoggerFactory.getLogger(NsqInOutTest.class);

    @BeforeClass
    public static void startup() {
        TestUtils.startFilterChain(
                MODULE_ID,
                CONFIG_FILE,
                LATCH,
                HostOs.isMac() || HostOs.isLinux());
    }

    @AfterClass
    public static void teardown() {
        if (HostOs.isMac() || HostOs.isLinux()) {
            for (ChildProcess process : m_processes) {
                if (process != null) {
                    process.destroy(true);
                }
            }
        }
    }

    @Test(timeout = 6000)
    public void testFilter() throws Exception {

        // We test collectd support on OS X and Linux
        if (HostOs.isMac() || HostOs.isLinux()) {
            LATCH.await();

            // Start nslookupd
            new ProcessThread(
                    "nsqlookupd",
                    new DefaultProcessHandler(),
                    m_processes)
                    .start();

            // Start nsqd
            new ProcessThread(
                    "nsqd",
                    new NsqProcessHandler(),
                    m_processes)
                    .start();

            // Wait for 1.5 seconds
            Thread.sleep(1500L);

            // Generate events (directly to outputs - bypassing chain)
            for (int i = 0; i < 50; i++) {

                JsonObject event = Events.createEvent(
                        getClass().getSimpleName(),
                        HostOs.hostName(),
                        Events.EVENT_TYPE_LOG);

                event.put(FIELD_NSQ_TOPIC, NAME_NSQ_TOPIC);

                JsonArray tags = new JsonArray();
                tags.add("nsq");
                event.put(EVENT_FIELD_TAGS, tags);

                VerticleHelper.container().vertx().eventBus().publish(
                        OUTPUT_ADDRESS,
                        Events.createBatchEvent(Arrays.asList(event)));

                Thread.sleep(50L);
            }
        }
    }

    public static class ProcessThread<V extends DefaultProcessHandler> extends Thread {

        private final String m_cmd;
        private final V m_handler;

        private List<ChildProcess> m_processes;

        public ProcessThread(
                final String cmd,
                final V handler,
                final List<ChildProcess> processes) {

            super(cmd);
            m_cmd = cmd;
            m_handler = handler;
            m_processes = processes;
        }

        @Override
        public void run() {
            final Path basePath = Paths.get("build/resources/test").toAbsolutePath();
            try {
                Path nsqPath = Paths.get(basePath.toString(), "nsq", "nsq-1.0.0-compat.linux-amd64.go1.8", "bin");
                if (HostOs.isMac()) {
                    nsqPath = Paths.get(basePath.toString(), "nsq", "nsq-1.0.0-compat.darwin-amd64.go1.8", "bin");
                }

                ChildProcess process = new ProcessExecutor()
                        .command(nsqPath.resolve(m_cmd).toString(), "-config=" + basePath.resolve(m_cmd + ".conf").toString())
                        .timeout(3000L, TimeUnit.MILLISECONDS)
                        .handler(m_handler)
                        .start();
                process.waitForExit();
                m_processes.add(process);

            } catch (InterruptedException e) {
                LOG.error("Failed to start nsqd service", e);
                Assert.fail();
            }
        }
    }

    private static class NsqProcessHandler extends DefaultProcessHandler {

        @Override
        public void onPreStart(final ChildProcess process) {
            // Cleanup and create empty dir
            cleanupFiles(TMP_PATH);

            // Always call super method
            super.onPreStart(process);
        }

        @Override
        public void onExit(final int statusCode) {
            // Cleanup
            deleteFolder(TMP_PATH.toFile());
        }

        private void cleanupFiles(final Path tmpPath) {
            try {
                deleteFolder(tmpPath.toFile());
                java.nio.file.Files.createDirectories(tmpPath);
            } catch (IOException e) {
                Assert.fail("Failed to create path:" + e.getMessage());
            }
        }
    }
}

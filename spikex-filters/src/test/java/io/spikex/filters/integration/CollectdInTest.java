/**
 * Copyright (c) 2017 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.filters.integration;

import com.google.common.io.Files;
import io.spikex.utils.HostOs;
import io.spikex.utils.process.ChildProcess;
import io.spikex.utils.process.DefaultProcessHandler;
import io.spikex.utils.process.ProcessExecutor;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static io.spikex.filters.integration.TestUtils.deleteFolder;

/**
 * @author cli
 */
public final class CollectdInTest {

    private static ChildProcess m_cmd;

    private static final Path TMP_PATH = Paths.get("/tmp/collectd");

    private static final String MODULE_ID = "io.spikex.filters";
    private static final String CONFIG_FILE = "collectd-test.json";

    private static final CountDownLatch LATCH = new CountDownLatch(1);

    private static final Logger LOG = LoggerFactory.getLogger(CollectdInTest.class);

    @BeforeClass
    public static void startup() {
        TestUtils.startFilterChain(
                MODULE_ID,
                CONFIG_FILE,
                LATCH,
                HostOs.isMac() || HostOs.isLinux());
    }

    @AfterClass
    public static void teardown() {
        if (HostOs.isMac() || HostOs.isLinux()) {
            m_cmd.destroy(true);
        }
    }

    @Test(timeout = 3500)
    public void testFilter() throws Exception {

        // We test collectd support on OS X and Linux
        if (HostOs.isMac() || HostOs.isLinux()) {
            LATCH.await();

            // Start collectd
            new CollectdThread("collectd.conf").start();

            // Wait for 2 seconds
            Thread.sleep(2000L);
        }
    }

    public static class CollectdThread extends Thread {

        private final String m_configName;

        public CollectdThread(final String configName) {

            super(CollectdThread.class.getSimpleName());
            m_configName = configName;
        }

        @Override
        public void run() {
            final Path basePath = Paths.get("build/resources/test").toAbsolutePath();
            final Path baseCollectdPath = basePath.resolve("collectd");
            try {
                Path collectdPath = baseCollectdPath.resolve("collectd-5.5.0-linux");
                if (HostOs.isMac()) {
                    collectdPath = baseCollectdPath.resolve("collectd-5.5.0-osx");
                }

                m_cmd = new ProcessExecutor()
                        .command(collectdPath.toString(), "-f", "-C", basePath.resolve(m_configName).toString())
                        .timeout(3000L, TimeUnit.MILLISECONDS)
                        .handler(new DefaultProcessHandler() {
                            @Override
                            public void onPreStart(final ChildProcess process) {
                                // Cleanup and create empty dir
                                cleanupFiles(TMP_PATH);

                                // Copy file to tmp dir
                                copyFiles(
                                        baseCollectdPath,
                                        basePath,
                                        TMP_PATH);

                                // Always call super method
                                super.onPreStart(process);
                            }

                            @Override
                            public void onExit(final int statusCode) {
                                // Cleanup
                                deleteFolder(TMP_PATH.toFile());
                            }

                            @Override
                            protected void handleStderr(final String txt) {
                                handleStdout(txt); // Reroute to stdout handler
                            }
                        })
                        .start();
                m_cmd.waitForExit();
            } catch (InterruptedException e) {
                LOG.error("Failed to start collectd service", e);
                Assert.fail();
            }
        }

        private void cleanupFiles(final Path tmpPath) {
            try {
                deleteFolder(tmpPath.toFile());
                java.nio.file.Files.createDirectories(tmpPath);
            } catch (IOException e) {
                org.junit.Assert.fail("Failed to create path:" + e.getMessage());
            }
        }

        private void copyFiles(
                final Path baseCollectdPath,
                final Path basePath,
                final Path tmpPath) {

            try {
                Path libPath = tmpPath.resolve("lib");
                java.nio.file.Files.createDirectories(libPath);
                // Copy types file to temp dir
                Files.copy(basePath.resolve("types.db").toFile(),
                        tmpPath.resolve("types.db").toFile());
                // Copy library files to temp dir
                Path baseLibPath = Paths.get(baseCollectdPath.toString(), "lib", "linux");
                if (HostOs.isMac()) {
                    baseLibPath = Paths.get(baseCollectdPath.toString(), "lib", "osx");
                }
                copyLibs(baseLibPath, libPath);
            } catch (IOException e) {
                Assert.fail("Failed to copy files:" + e.getMessage());
            }
        }

        private void copyLibs(
                final Path basePath,
                final Path destPath) throws IOException {

            // LogFile
            Files.copy(basePath.resolve("logfile.so").toFile(),
                    destPath.resolve("logfile.so").toFile());
            // Write HTTP
            Files.copy(basePath.resolve("write_http.so").toFile(),
                    destPath.resolve("write_http.so").toFile());
            // CPU - This doesn't seem to work on OSX (https://github.com/collectd/collectd/issues/22)
            Files.copy(basePath.resolve("cpu.so").toFile(),
                    destPath.resolve("cpu.so").toFile());
            // Interface
            Files.copy(basePath.resolve("interface.so").toFile(),
                    destPath.resolve("interface.so").toFile());
            // Load
            Files.copy(basePath.resolve("load.so").toFile(),
                    destPath.resolve("load.so").toFile());
            // Memory
            Files.copy(basePath.resolve("memory.so").toFile(),
                    destPath.resolve("memory.so").toFile());
            // Network
            Files.copy(basePath.resolve("network.so").toFile(),
                    destPath.resolve("network.so").toFile());
        }
    }
}

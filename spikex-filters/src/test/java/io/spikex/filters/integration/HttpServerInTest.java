/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.integration;

import io.vertx.core.http.HttpHeaders;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.concurrent.CountDownLatch;

/**
 * @author cli
 */
public final class HttpServerInTest {

    private static final String MODULE_ID = "io.spikex.filters";
    private static final String CONFIG_FILE = "http-server-test.json";

    private static final CountDownLatch LATCH = new CountDownLatch(1);

    private static final Logger LOG = LoggerFactory.getLogger(HttpServerInTest.class);

    @BeforeClass
    public static void startup() {
        TestUtils.startFilterChain(
                MODULE_ID,
                CONFIG_FILE,
                LATCH,
                true);
    }

    @Test(timeout = 3500)
    public void testEsClient() throws Exception {

        LATCH.await();
        RestClient restClient = RestClient.builder(new HttpHost("localhost", 9200))
                .setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback() {
                    @Override
                    public RequestConfig.Builder customizeRequestConfig(RequestConfig.Builder requestConfigBuilder) {
                        return requestConfigBuilder.setConnectTimeout(3500)
                                .setSocketTimeout(3500);
                    }
                })
                .setMaxRetryTimeoutMillis(3500)
                .build();

        HttpEntity entity = new NStringEntity("{\n"
                + "    \"user\" : \"kimchy\",\n"
                + "    \"ip_address\" : \"134.119.24.29\",\n"
                + "    \"post_date\" : \"2009-11-15T14:12:12\",\n"
                + "    \"message\" : \"trying out Elasticsearch\"\n"
                + "}", ContentType.APPLICATION_JSON);

        for(int i = 0; i < 10; i++) {

            Response response = restClient.performRequest(
                    "PUT",
                    "/twitter/tweet/1",
                    Collections.emptyMap(),
                    entity);

            LOG.info("Response: {}", EntityUtils.toString(response.getEntity()));
            LOG.info("Status: {}", response.getStatusLine());
            LOG.info("Content-Type: {}",
                    response.getHeader(HttpHeaders.CONTENT_TYPE.toString()));
            LOG.info("Content-Length: {}",
                    response.getHeader(HttpHeaders.CONTENT_LENGTH.toString()));

            Thread.sleep(150L);
        }
    }
}

/**
 * Copyright (c) 2015 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.filters.integration;

import io.spikex.base.helper.VerticleHelper;
import io.spikex.filters.MainVerticle;
import io.spikex.utils.Environment;
import io.spikex.utils.HostOs;
import io.vertx.core.DeploymentOptions;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.CountDownLatch;

/**
 *
 * @author cli
 */
public class TestUtils {

    private static final Logger m_logger = LoggerFactory.getLogger(TestUtils.class);

    // http://stackoverflow.com/questions/7768071/how-to-delete-directory-content-in-java
    public static void deleteFolder(File folder) {
        m_logger.info("Cleaning up folder: {}", folder);
        File[] files = folder.listFiles();
        if (files != null) { //some JVMs return null for empty dirs
            for (File f : files) {
                if (f.isDirectory()) {
                    m_logger.info("Removing dir: {}", f);
                    deleteFolder(f);
                } else {
                    m_logger.info("Removing file: {}", f);
                    f.delete();
                }
            }
        }
        folder.delete();
    }

    public static void startFilterChain(
            final String moduleId,
            final String configFile,
            final CountDownLatch latch,
            final boolean supportedPlatform) {

        Path configPath = Paths.get(
                Environment.configHome(),
                moduleId,
                configFile);

        if (supportedPlatform) {

            VerticleHelper.startVerticle(
                    new String[]{
                            Environment.asString(),
                            "[\"" + configPath.toString() + "\"]"
                    },
                    MainVerticle.class.getName(),
                    new DeploymentOptions(),
                    res -> {
                        if (res.failed()) {
                            Assert.fail(res.cause().toString());
                        }
                        latch.countDown();
                    });
        } else {
            m_logger.info("Skipping test - not a supported platform: {}",
                    HostOs.operatingSystem());
        }
    }
}

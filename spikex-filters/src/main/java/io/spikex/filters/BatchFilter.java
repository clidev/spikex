/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters;

import io.spikex.filters.internal.FilterEvents;
import io.vertx.core.json.JsonObject;
import org.eclipse.collections.impl.list.mutable.FastList;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static io.spikex.base.helper.Events.EVENT_FIELD_ID;

/**
 * @author cli
 */
public final class BatchFilter extends AbstractFilter {

    private final BlockingQueue m_queue;
    private final List<JsonObject> m_events;
    private int m_count;
    private int m_maxBatchSize;

    private static final String CONF_KEY_MAX_BATCH_SIZE = "max-batch-size";

    //
    // Default configuration values
    //
    private static final int DEF_MAX_BATCH_SIZE = 1000;

    public BatchFilter() {
        m_queue = new LinkedBlockingQueue();
        m_events = FastList.newList();
    }

    @Override
    protected void onStart() {
        m_maxBatchSize = config().getInteger(CONF_KEY_MAX_BATCH_SIZE,
                DEF_MAX_BATCH_SIZE);
    }

    @Override
    protected void onEvent(final JsonObject event) {

        if(logger().isTraceEnabled()) {
            logger().trace("Adding event to batch: {}", event.encode());
        }

        m_queue.add(event);
        m_count++;

        if (m_count >= m_maxBatchSize) {
            drainEvents();
        }
    }

    @Override
    protected void onTimerEvent() {
        drainEvents();
    }

    private void drainEvents() {
        List<JsonObject> events = m_events;
        events.clear();
        m_queue.drainTo(events, m_maxBatchSize);
        m_count = 0;

        if (!events.isEmpty()) {

            JsonObject batchEvent = FilterEvents.createBatchEvent(this, events);
            logger().trace("Created batch event: {} with {} events",
                    batchEvent.getString(EVENT_FIELD_ID),
                    events.size());

            //
            // Send event(s) to next in chain
            //
            publishEvent(batchEvent);
        }
    }
}

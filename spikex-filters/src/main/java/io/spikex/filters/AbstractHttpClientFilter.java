/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters;

import io.spikex.base.connection.AbstractHttpClient;
import io.vertx.core.json.JsonObject;

import static io.spikex.filters.AbstractFilter.*;

/**
 * @author cli
 */
public abstract class AbstractHttpClientFilter extends AbstractHttpClient {

    public String getChainName() {
        return config().getString(KEY_CHAIN_NAME, "");
    }

    public String getInputAddress() {
        return config().getString(KEY_ADDRESS_INPUT, "");
    }

    public String getOutputAddress() {
        return config().getString(KEY_ADDRESS_OUTPUT, "");
    }

    public void start() {

        super.start();

        //
        // Register event consumer
        //
        eventBus().consumer(getInputAddress(), message -> {
            onEvent((JsonObject) message.body());
        });
    }

    protected void onEvent(final JsonObject event) {
        // Do nothing by default...
    }

    protected void publishEvent(final JsonObject event) {
        vertx.eventBus().publish(getOutputAddress(), event);
    }

    protected void publishEvent(
            final String outputAddress,
            final JsonObject event) {

        vertx.eventBus().publish(outputAddress, event);
    }

    protected void sendEvent(final JsonObject event) {
        vertx.eventBus().send(getOutputAddress(), event);
    }
}

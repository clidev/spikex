/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.output;

import io.spikex.filters.AbstractFilter;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.graylog2.gelfclient.*;
import org.graylog2.gelfclient.transport.GelfTransport;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

import static io.spikex.base.helper.Events.*;

/**
 *
 */
public final class Graylog2Filter extends AbstractFilter {

    private GelfTransport m_gelfTransport;
    private String m_additionalFieldPrefix;

    private static final String CONF_KEY_ADDRESS = "address";
    private static final String CONF_KEY_PORT = "port";
    private static final String CONF_KEY_QUEUE_SIZE = "queue-size";
    private static final String CONF_KEY_SEND_BUFFER_SIZE = "send-buffer-size";
    private static final String CONF_KEY_MAX_IN_FLIGHT_SENDS = "max-in-flight-sends";
    private static final String CONF_KEY_TRANSPORT = "transport";
    private static final String CONF_KEY_RECONNECT_DELAY = "reconnect-delay";
    private static final String CONF_KEY_CONNECT_TIMEOUT = "connect-timeout";
    private static final String CONF_KEY_TCP_NO_DELAY = "tcp-no-delay";
    private static final String CONF_KEY_TCP_KEEP_ALIVE = "tcp-keep-alive";
    private static final String CONF_KEY_ADDITIONAL_FIELD_PREFIX = "additional-field-prefix";

    // Configuration defaults
    private static final int DEF_PORT = 12201;
    private static final int DEF_QUEUE_SIZE = 512;
    private static final int DEF_MAX_IN_FLIGHT_SENDS = 512;
    private static final int DEF_RECONNECT_DELAY = 500;
    private static final int DEF_CONNECT_TIMEOUT = 1000;
    private static final int DEF_SEND_BUFFER_SIZE = -1;
    private static final String DEF_ADDRESS = "127.0.0.1";
    private static final String DEF_TRANSPORT = "TCP";
    private static final String DEF_ADDITIONAL_FIELD_PREFIX = "_";
    private static final boolean DEF_TCP_NO_DELAY = false;
    private static final boolean DEF_TCP_KEEP_ALIVE = false;

    @Override
    protected void onStart() {

        final int port = config().getInteger(CONF_KEY_PORT, DEF_PORT);
        final int queueSize = config().getInteger(CONF_KEY_QUEUE_SIZE, DEF_QUEUE_SIZE);
        final int sendBufferSize = config().getInteger(CONF_KEY_SEND_BUFFER_SIZE, DEF_SEND_BUFFER_SIZE);
        final int maxInFlightSends = config().getInteger(CONF_KEY_MAX_IN_FLIGHT_SENDS, DEF_MAX_IN_FLIGHT_SENDS);
        final int reconnectDelay = config().getInteger(CONF_KEY_RECONNECT_DELAY, DEF_RECONNECT_DELAY);
        final int connectTimeout = config().getInteger(CONF_KEY_CONNECT_TIMEOUT, DEF_CONNECT_TIMEOUT);

        final String address = config().getString(CONF_KEY_ADDRESS, DEF_ADDRESS);
        final String transport = config().getString(CONF_KEY_TRANSPORT, DEF_TRANSPORT);

        final boolean tcpNoDelay = config().getBoolean(CONF_KEY_TCP_NO_DELAY, DEF_TCP_NO_DELAY);
        final boolean tcpKeepAlive = config().getBoolean(CONF_KEY_TCP_KEEP_ALIVE, DEF_TCP_KEEP_ALIVE);

        m_additionalFieldPrefix = config().getString(CONF_KEY_ADDITIONAL_FIELD_PREFIX, DEF_ADDITIONAL_FIELD_PREFIX);

        final GelfConfiguration gelfConfig = new GelfConfiguration(new InetSocketAddress(address, port))
                .transport(GelfTransports.valueOf(transport))
                .queueSize(queueSize)
                .maxInflightSends(maxInFlightSends)
                .sendBufferSize(sendBufferSize)
                .connectTimeout(connectTimeout)
                .reconnectDelay(reconnectDelay)
                .tcpNoDelay(tcpNoDelay)
                .tcpKeepAlive(tcpKeepAlive);

        m_gelfTransport = GelfTransports.create(gelfConfig);
    }

    @Override
    protected void onEvent(final JsonObject batchEvent) {

        try {
            logger().trace("Received event: {} batch-size: {}",
                    batchEvent.getString(EVENT_FIELD_ID),
                    batchEvent.getInteger(EVENT_FIELD_BATCH_SIZE, 0));

            //
            // Operate on arrays only (batches)
            //
            JsonArray batch = batchEvent.getJsonArray(EVENT_FIELD_BATCH_EVENTS, new JsonArray());
            if (!batchEvent.containsKey(EVENT_FIELD_BATCH_EVENTS)) {
                batch.add(batchEvent);
            }
            for (int i = 0; i < batch.size(); i++) {

                JsonObject event = batch.getJsonObject(i);

                String message = event.getString(EVENT_FIELD_MESSAGE, "");
                String fullMessage = event.getString(EVENT_FIELD_LOG_MESSAGE, "");
                String host = event.getString(EVENT_FIELD_HOST, "");
                String level = event.getString(EVENT_FIELD_LOG_LEVEL, GelfMessageLevel.INFO.name());
                long timestamp = event.getLong(EVENT_FIELD_TIMESTAMP);

                GelfMessageBuilder builder = new GelfMessageBuilder(message, host)
                        .level(GelfMessageLevel.valueOf(level))
                        .timestamp(timestamp)
                        .fullMessage(fullMessage)
                        .additionalFields(buildAdditionalFields(event));

                m_gelfTransport.send(builder.build());
            }
        } catch (Exception e) {
            logger().error("Failed to push GELF event: {}",
                    batchEvent.getString(EVENT_FIELD_ID), e);
        }
    }

    private Map<String, Object> buildAdditionalFields(final JsonObject event) {

        Map<String, Object> fields = new HashMap();

        // Add all fields except host, message, fullMessage, timestamp and level
        for (Map.Entry<String, Object> entry : event.getMap().entrySet()) {

            String key = entry.getKey();
            if (!EVENT_FIELD_HOST.equals(key)
                    && !EVENT_FIELD_MESSAGE.equals(key)
                    && !EVENT_FIELD_TIMESTAMP.equals(key)
                    && !EVENT_FIELD_LOG_LEVEL.equals(key)
                    && !EVENT_FIELD_LOG_MESSAGE.equals(key)) {

                fields.put(m_additionalFieldPrefix + entry.getKey(), entry.getValue());
            }
        }

        return fields;
    }
}

/**
 * Copyright (c) 2015 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.filters.output;

import com.google.common.base.Strings;
import io.spikex.filters.AbstractFilter;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import static io.spikex.base.helper.Events.EVENT_FIELD_BATCH_EVENTS;
import static io.spikex.base.helper.Events.EVENT_FIELD_ID;

/**
 * @author cli
 */
public final class LogFilter extends AbstractFilter {

    private String m_mdcValue;

    private static final String FIELD_MDC_KEY = "mdc-key";
    private static final String FIELD_MDC_VALUE = "mdc-value";

    //
    // Configuration defaults
    //
    private static final String DEF_MDC_KEY = "event";
    private static final String DEF_MDC_VALUE = "";

    private final Logger m_evnLogger = LoggerFactory.getLogger("io.spikex.events");

    @Override
    protected void onStart() {
        m_mdcValue = config().getString(FIELD_MDC_VALUE, DEF_MDC_VALUE);
    }

    @Override
    protected void onEvent(final JsonObject batchEvent) {

        try {
            //
            // Operate on arrays only (batches)
            //
            JsonArray batch = batchEvent.getJsonArray(EVENT_FIELD_BATCH_EVENTS, new JsonArray());
            if (!batchEvent.containsKey(EVENT_FIELD_BATCH_EVENTS)) {
                batch.add(batchEvent);
            }
            for (int i = 0; i < batch.size(); i++) {

                JsonObject event = batch.getJsonObject(i);

                // Simply log the event using the specified MDC
                String mdcKey = event.getString(FIELD_MDC_KEY, DEF_MDC_KEY);
                String mdcValue = event.getString(FIELD_MDC_VALUE, m_mdcValue);
                if (!Strings.isNullOrEmpty(mdcValue)) {
                    MDC.put(mdcKey, String.valueOf((Object) variables().translate(event, mdcValue)));
                }
                m_evnLogger.info(event.toString());
            }
        } catch (Exception e) {
            logger().error("Failed to log event: {}",
                    batchEvent.getString(EVENT_FIELD_ID), e);
        }
    }
}

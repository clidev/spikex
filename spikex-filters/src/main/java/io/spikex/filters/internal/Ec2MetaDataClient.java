/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.internal;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.*;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import io.spikex.base.helper.HostMetaData;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.List;

import static io.spikex.filters.internal.FilterEvents.EVENT_EMPTY_VALUE;

/**
 *
 */
public final class Ec2MetaDataClient {

    private static final String CONF_KEY_NAME = "name";
    private static final String CONF_KEY_VALUES = "values";

    private static final String KEY_NAME_TAG = "Name";
    private static final String KEY_ENVIRONMENT_TAG = "Environment";
    private static final String KEY_PLATFORM_TAG = "Platform";

    private AmazonEC2 m_client;
    private final List<Filter> m_filters;

    public Ec2MetaDataClient() {
        m_filters = new ArrayList();
    }

    public void connect(
            final String region,
            final JsonArray filters) {

        // Sanity checks
        Preconditions.checkArgument(
                !Strings.isNullOrEmpty(region), "region must be defined");

        m_client = AmazonEC2ClientBuilder
                .standard()
                .withRegion(region)
                .build();
//                .withClientConfiguration()

        m_filters.clear();
        for (int i = 0; i < filters.size(); i++) {

            JsonObject filter = filters.getJsonObject(i);
            String name = filter.getString(CONF_KEY_NAME);
            JsonArray values = filter.getJsonArray(CONF_KEY_VALUES, new JsonArray());

            // Sanity checks
            Preconditions.checkArgument(!Strings.isNullOrEmpty(name), "name is not defined or empty");
            Preconditions.checkArgument(values.size() > 0, "values is not defined or empty");

            m_filters.add(new Filter()
                    .withName(name)
                    .withValues(values.getList()));
        }
    }

    public void close() {
        m_client.shutdown();
    }

    public List<Instance> fetchInstances() {

        List<Instance> instances = new ArrayList();
        DescribeInstancesRequest request = null;

        if (m_filters.size() > 0) {
            request = new DescribeInstancesRequest();
            request.setFilters(m_filters);
        }

        DescribeInstancesResult result;

        String token = "";
        while (token != null) {

            if (request != null) {

                if (!token.isEmpty()) {
                    request.setNextToken(token);
                }

                result = m_client.describeInstances(request);
                token = result.getNextToken();

            } else {
                result = m_client.describeInstances();
                token = null;
            }

            List<Reservation> reservations = result.getReservations();
            for (Reservation reservation : reservations) {
                instances.addAll(reservation.getInstances());
            }
        }

        return instances;
    }

    public static HostMetaData createHostMetaData(
            final Instance instance,
            final String region,
            String envKey,
            String platformKey) {

        int foundCount = 0;
        String envValue = EVENT_EMPTY_VALUE;
        String platformValue = EVENT_EMPTY_VALUE;
        String nameValue = EVENT_EMPTY_VALUE;

        envKey = Strings.isNullOrEmpty(envKey) ? KEY_ENVIRONMENT_TAG : envKey;
        platformKey = Strings.isNullOrEmpty(platformKey) ? KEY_PLATFORM_TAG : platformKey;

        for (Tag tag : instance.getTags()) {

            if (tag.getKey().equals(envKey)) {
                envValue = tag.getValue();
                foundCount++;
            } else if (tag.getKey().equals(platformKey)) {
                platformValue = tag.getValue();
                foundCount++;
            } else if (tag.getKey().equals(KEY_NAME_TAG)) {
                nameValue = tag.getValue();
                foundCount++;
            }

            if (foundCount > 2) {
                break;
            }
        }

        String hostId = instance.getInstanceId();

        String hostName = instance.getPrivateDnsName();

        if (!Strings.isNullOrEmpty(hostName)) {
            hostName = Splitter.on('.').split(hostName).iterator().next();
        } else {
            hostName = EVENT_EMPTY_VALUE;
        }

        String privateAddress = instance.getPrivateIpAddress();
        String publicAddress = instance.getPublicIpAddress();
        String instanceType = instance.getInstanceType();
        String zone = instance.getPlacement().getAvailabilityZone();

        return HostMetaData.builder()
                .withId(hostId)
                .withHostName(hostName)
                .withAddress(Strings.isNullOrEmpty(privateAddress) ? EVENT_EMPTY_VALUE : privateAddress)
                .withPublicIp(Strings.isNullOrEmpty(publicAddress) ? EVENT_EMPTY_VALUE : publicAddress)
                .withDescription(nameValue)
                .withHw(Strings.isNullOrEmpty(instanceType) ? EVENT_EMPTY_VALUE : instanceType)
                .withOs(platformValue)
                .withEnv(envValue)
                .withZone(Strings.isNullOrEmpty(zone) ? EVENT_EMPTY_VALUE : zone)
                .withRegion(region)
                .build();
    }
}

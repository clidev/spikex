/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.internal;

import io.spikex.base.helper.GeoMetaData;
import io.spikex.base.helper.HostMetaData;
import io.spikex.base.helper.MetaData;
import io.spikex.base.helper.Variables;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.LocalMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * @author cli
 */
public final class Mapping {

    private final Map<String, Object> m_mapping;
    private final Map<String, Map<String, Object>> m_lookupValues;
    private final boolean m_overwriteValues;
    private final boolean m_dropNonStandardFields;
    private final LocalMap<String, MetaData> m_lookupHostData;
    private final LocalMap<String, MetaData> m_lookupGeoData;

    private static final String CONF_KEY_OVERRIDE_VALUES = "overwrite-values";
    private static final String CONF_KEY_DROP_NON_STANDARD_FIELDS = "drop-non-standard-fields";
    private static final String CONF_KEY_LOOKUP_VALUES = "lookup-values";
    private static final String CONF_KEY_MAPPING = "mapping";

    private static final boolean DEF_OVERRIDE_VALUES = false;
    private static final boolean DEF_DROP_NON_STANDARD_FIELDS = false;

    private static final String FIELD_STD_PREFIX = "@";

    private static final String LOOKUP_HOST = "%{#lookupHost(";
    private static final String LOOKUP_GEO = "%{#lookupGeo(";

    private static final Logger m_logger = LoggerFactory.getLogger(Mapping.class);

    private Mapping(
            final Vertx vertx,
            final Map<String, Object> mapping,
            final Map<String, Map<String, Object>> lookupValues,
            final boolean overwriteValues,
            final boolean dropNonStandardFields) {

        m_mapping = mapping;
        m_lookupValues = lookupValues;
        m_overwriteValues = overwriteValues;
        m_dropNonStandardFields = dropNonStandardFields;
        m_lookupHostData = vertx.sharedData().getLocalMap(HostMetaData.SHARED_DATA_MAP);
        m_lookupGeoData = vertx.sharedData().getLocalMap(GeoMetaData.SHARED_DATA_MAP);
    }

    public boolean isOverwriteValues() {
        return m_overwriteValues;
    }

    public boolean isDropNonStandardFields() {
        return m_dropNonStandardFields;
    }

    public void apply(
            final Variables variables,
            final JsonObject event) {

        Map<String, Object> mapping = m_mapping;
        Map<String, Map<String, Object>> lookupValues = m_lookupValues;

        // Perform mappings
        Set<String> fields = mapping.keySet();
        for (String field : fields) {
            if (!event.containsKey(field)
                    || isOverwriteValues()) {

                String value = String.valueOf(mapping.get(field));

                // Handle special lookups
                if (value.startsWith(LOOKUP_HOST)) {
                    value = variables.resolveVar(value);
                    m_logger.trace("Looking up host: {}", value);
                    event.put(field, String.valueOf((Object) variables.lookupMetaData(event, value, m_lookupHostData)));
                } else if (value.startsWith(LOOKUP_GEO)) {
                    value = variables.resolveVar(value);
                    m_logger.trace("Looking up location: {}", value);
                    event.put(field, String.valueOf((Object) variables.lookupMetaData(event, value, m_lookupGeoData)));
                } else {
                    event.put(field, String.valueOf((Object) variables.translate(event, value, lookupValues)));
                }
            }
        }

        // Remove non-standard fields
        if (isDropNonStandardFields()) {
            List<String> dropFields = new ArrayList();
            fields = event.fieldNames();
            for (String field : fields) {
                if (!field.startsWith(FIELD_STD_PREFIX)) {
                    dropFields.add(field);
                }
            }
            for (String field : dropFields) {
                event.remove(field);
            }
        }
    }

    @Override
    public String toString() {
        String sname = getClass().getSimpleName();
        StringBuilder sb = new StringBuilder(sname);
        sb.append("[");
        sb.append(hashCode());
        sb.append("] mapping: ");
        sb.append(m_mapping);
        sb.append(" lookup-values: ");
        sb.append(m_lookupValues);
        return sb.toString();
    }

    public static Mapping create(
            final Vertx vertx,
            final JsonObject config) {

        boolean overwriteValues = config.getBoolean(CONF_KEY_OVERRIDE_VALUES,
                DEF_OVERRIDE_VALUES);

        boolean dropNonStandardFields = config.getBoolean(CONF_KEY_DROP_NON_STANDARD_FIELDS,
                DEF_DROP_NON_STANDARD_FIELDS);

        JsonObject mapping = config.getJsonObject(CONF_KEY_MAPPING, new JsonObject());
        JsonObject lookupValues = config.getJsonObject(CONF_KEY_LOOKUP_VALUES, new JsonObject());

        // Build lookup map
        Map<String, Map<String, Object>> lookupMap = new HashMap();
        Set<String> fields = lookupValues.fieldNames();
        for (String field : fields) {
            Map<String, Object> values = lookupValues.getJsonObject(field).getMap();
            lookupMap.put(field, values);
        }

        return new Mapping(
                vertx,
                mapping.getMap(),
                lookupMap,
                overwriteValues,
                dropNonStandardFields);
    }
}

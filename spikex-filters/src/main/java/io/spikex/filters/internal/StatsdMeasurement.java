/**
 * Copyright (c) 2017 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.filters.internal;

import com.google.common.base.Strings;
import io.spikex.utils.Numbers;
import org.eclipse.collections.impl.map.mutable.UnifiedMap;

import java.util.Map;

import static io.spikex.base.helper.Events.DSTYPE_COUNTER;
import static io.spikex.base.helper.Events.DSTYPE_GAUGE;

/**
 * @author cli
 */
public final class StatsdMeasurement {

    public static final String TYPE_GAUGE = "g";
    public static final String TYPE_COUNTER = "c";
    public static final String TYPE_TIMER = "ms";

    private final String m_hostname;
    private final String m_bucket;
    private final String m_dstype;
    private final String m_subgroup;
    private final Map<String, String> m_tags;

    private String m_sender;
    private String m_dsname;
    private String m_instance;

    private boolean m_isDouble;
    private long m_longValue;
    private double m_doubleValue;

    public StatsdMeasurement(
            final String bucket,
            final String type,
            final String hostname) {

        m_bucket = bucket;
        m_dstype = resolveDstype(type);
        m_hostname = hostname;
        m_isDouble = false;
        m_tags = new UnifiedMap();
        m_sender = "";
        m_dsname = "";
        m_instance = "";

        // Subgroup (always last item of bucket)
        m_subgroup = bucket.substring(bucket.lastIndexOf('.') + 1);
    }

    public String getBucket() {
        return m_bucket;
    }

    public String getDsname() {
        String dsname = m_dsname;
        return (Strings.isNullOrEmpty(dsname) ? m_bucket : dsname);
    }

    public String getDstype() {
        return m_dstype;
    }

    public Object getValue() {

        Number value;

        if (m_isDouble) {
            value = Double.valueOf(m_doubleValue);
        } else {
            value = Long.valueOf(m_longValue);
        }

        return value;
    }

    public String getHostname() {
        String sender = m_sender;
        return (Strings.isNullOrEmpty(sender) ? m_hostname : sender);
    }

    public String getInstance() {
        return m_instance;
    }

    public String getSubgroup() {
        return m_subgroup;
    }

    public Map<String, String> getTags() {
        return m_tags;
    }

    public void addTag(
            final String name,
            final String value) {

        m_tags.put(name, value);
    }

    public void handleValue(
            final String type,
            final String value) {

        char sign = value.charAt(0);

        switch (type) {
            case TYPE_GAUGE:

                if (sign == '+' || sign == '-') {
                    updateValue(value);
                } else {
                    // Set initial value (>=0)
                    setValue(value);
                }
                break;

            case TYPE_COUNTER:

                if (sign == '+' || sign == '-') {
                    updateCounter(value);
                } else {
                    // Set initial value (>=0)
                    setValue(value);
                }
                break;

            case TYPE_TIMER:
                setValue(value);
                break;
        }
    }

    public void handleRate(final String sampleRate) {

        // Rates apply only to counters
        if (DSTYPE_COUNTER.equals(getDstype())) {

            double rate = Double.valueOf(sampleRate);

            if (rate > 0.0d) {
                m_longValue = (long) (m_longValue / rate);
            }
        }
    }

    public void setSender(final String sender) {
        m_sender = sender;
    }

    public void setDsname(final String dsname) {
        m_dsname = dsname;
    }

    public void setInstance(final String instance) {
        m_instance = instance;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(getClass().getSimpleName());
        sb.append("{host: ");
        sb.append(getHostname());
        sb.append(" dsname: ");
        sb.append(getDsname());
        sb.append(" dstype: ");
        sb.append(getDstype());
        sb.append(" instance: ");
        sb.append(getInstance());
        sb.append(" subgroup: ");
        sb.append(getSubgroup());
        sb.append("}");
        return sb.toString();
    }

    private void updateValue(final String value) {
        if (Numbers.isDecimal(value)) {
            m_doubleValue = m_doubleValue + Double.valueOf(value);
            m_isDouble = true;
        } else {
            m_longValue = m_longValue + Long.valueOf(value);
        }
    }

    private void updateCounter(final String value) {
        m_longValue = m_longValue + Long.valueOf(value);
    }

    private void setValue(final String value) {
        if (Numbers.isDecimal(value)) {
            m_doubleValue = Double.valueOf(value);
            m_isDouble = true;
        } else {
            m_longValue = Long.valueOf(value);
        }
    }

    private String resolveDstype(final String type) {

        String dstype = "";

        switch (type) {
            case TYPE_GAUGE:
                dstype = DSTYPE_GAUGE;
                break;
            case TYPE_COUNTER:
                dstype = DSTYPE_COUNTER;
                break;
            case TYPE_TIMER:
                dstype = DSTYPE_GAUGE;
                break;
        }

        return dstype;
    }
}

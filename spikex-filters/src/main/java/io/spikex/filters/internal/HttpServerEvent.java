/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.internal;

import io.vertx.core.http.HttpServerRequest;

/**
 * @author cli
 */
public final class HttpServerEvent {

    private final String m_data;
    private final HttpServerRequest m_request;

    private HttpServerEvent(
            final String data,
            final HttpServerRequest request) {

        m_data = data;
        m_request = request;
    }

    public String getData() {
        return m_data;
    }

    public HttpServerRequest getRequest() {
        return m_request;
    }

    public static HttpServerEvent create(
            final String data,
            final HttpServerRequest request) {

        return new HttpServerEvent(data, request);
    }
}

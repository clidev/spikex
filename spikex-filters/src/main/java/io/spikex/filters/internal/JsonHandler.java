/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.internal;

import io.spikex.base.helper.Events;
import io.spikex.filters.AbstractFilter;
import io.spikex.utils.HostOs;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import static io.spikex.base.helper.Events.EVENT_FIELD_TAGS;
import static io.spikex.base.helper.Events.EVENT_TYPE_LOG;

/**
 * @author cli
 */
public class JsonHandler implements Handler<HttpServerEvent> {

    private final AbstractFilter m_filter;
    private final EventBus m_eventBus;
    private final JsonArray m_tags;

    public JsonHandler(
            final AbstractFilter filter,
            final EventBus eventBus,
            final JsonArray tags) {

        m_filter = filter;
        m_eventBus = eventBus;
        m_tags = tags;
    }

    @Override
    public void handle(final HttpServerEvent evn) {

        // Try to parse json and emit new event
        JsonObject data = new JsonObject(evn.getData());
        JsonObject event = Events.createEvent(
                getClass().getSimpleName(),
                HostOs.hostName(),
                EVENT_TYPE_LOG);

        event.mergeIn(data);

        // Add tags
        event.put(EVENT_FIELD_TAGS, m_tags);

        String outAddr = m_filter.getOutputAddress();
        if (outAddr != null && outAddr.length() > 0) {
            m_eventBus.publish(outAddr, event);
        }
    }
}

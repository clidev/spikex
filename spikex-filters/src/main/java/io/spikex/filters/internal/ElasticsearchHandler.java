/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.internal;

import com.google.common.net.MediaType;
import io.spikex.filters.AbstractFilter;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author cli
 */
public final class ElasticsearchHandler extends JsonHandler {

    private final Pattern PATH_COMPS = Pattern.compile("/([^/]+)/([^/]+)/([0-9]+)");
    private final Logger m_logger = LoggerFactory.getLogger(ElasticsearchHandler.class);

    public ElasticsearchHandler(
            final AbstractFilter filter,
            final EventBus eventBus,
            final JsonArray tags) {

        super(filter, eventBus, tags);
    }

    @Override
    public void handle(final HttpServerEvent evn) {
        try {
            super.handle(evn); // emit event

            String index = "";
            String type = "";
            int version = 0;

            Matcher m = PATH_COMPS.matcher(evn.getRequest().path());
            if (m.matches()) {
                index = m.group(1);
                type = m.group(2);
                version = Integer.parseInt(m.group(3));
            }

            // Simulate Elasticsearch response
            JsonObject jsonRoot = new JsonObject();
            jsonRoot.put("_index", index);
            jsonRoot.put("_type", type);
            jsonRoot.put("_id", "1");
            jsonRoot.put("_version", version);

            JsonObject jsonShards = new JsonObject();
            jsonShards.put("total", 1);
            jsonShards.put("successful", 1);
            jsonShards.put("failed", 0);

            jsonRoot.put("_shards", jsonShards);
            jsonRoot.put("created", true);

            HttpServerResponse response = evn.getRequest().response();
            Buffer content = Buffer.buffer(jsonRoot.encode());

            response.putHeader(HttpHeaders.CONTENT_TYPE.toString(),
                    MediaType.JSON_UTF_8.toString());

            response.putHeader(HttpHeaders.CONTENT_LENGTH.toString(),
                    String.valueOf(content.length()));

            response.end(content);

        } catch (Exception e) {
            throw e;
        }
    }
}

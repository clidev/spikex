/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.internal;

import io.spikex.base.helper.Events;
import io.spikex.filters.AbstractFilter;
import io.vertx.core.json.JsonObject;

import java.time.ZoneId;
import java.util.List;

import static io.spikex.base.helper.Events.*;

/**
 * @author cli
 */
public class FilterEvents {

    /**
     * Represents an empty string value
     */
    public static final String EVENT_EMPTY_VALUE = "";


    public static JsonObject createMetricEvent(
            final AbstractFilter filter,
            final String host,
            final String dsname,
            final String dstype,
            final String subgroup,
            final String instance,
            final String unit,
            final long interval,
            final Object value) {

        return createMetricEvent(
                filter,
                System.currentTimeMillis(),
                TIMEZONE_UTC,
                host,
                dsname,
                dstype,
                DSTIME_PRECISION_SEC,
                subgroup,
                instance,
                unit,
                interval,
                value);
    }

    public static JsonObject createMetricEvent(
            final AbstractFilter filter,
            final long timestamp,
            final ZoneId timezone,
            final String host,
            final String dsname,
            final String dstype,
            final String precision,
            final String subgroup,
            final String instance,
            final String unit,
            final long interval,
            final Object value) {

        JsonObject event = Events.createMetricEvent(
                timestamp,
                timezone,
                filter.getClass().getSimpleName(),
                host,
                dsname,
                dstype,
                precision,
                subgroup,
                instance,
                unit,
                interval,
                value);

        event.put(EVENT_FIELD_CHAIN, filter.getChainName());
        return event;
    }

    public static JsonObject createBatchEvent(
            final AbstractFilter filter,
            final List<JsonObject> events) {

        JsonObject batchEvent = Events.createBatchEvent(events);
        batchEvent.put(EVENT_FIELD_SOURCE, filter.getClass().getSimpleName());
        batchEvent.put(EVENT_FIELD_CHAIN, filter.getChainName());
        return batchEvent;
    }
}

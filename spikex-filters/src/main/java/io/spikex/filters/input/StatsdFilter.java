/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.input;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.primitives.Ints;
import io.spikex.filters.AbstractFilter;
import io.spikex.filters.internal.FilterEvents;
import io.spikex.filters.internal.StatsdMeasurement;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.datagram.DatagramPacket;
import io.vertx.core.datagram.DatagramSocket;
import io.vertx.core.datagram.DatagramSocketOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.eclipse.collections.impl.map.mutable.UnifiedMap;

import java.util.*;

import static io.spikex.base.helper.Events.EVENT_FIELD_PRECISION;
import static io.spikex.base.helper.Events.EVENT_FIELD_TAGS;
import static io.spikex.base.helper.Events.DSTIME_PRECISION_MILLIS;
import static io.spikex.filters.internal.FilterEvents.EVENT_EMPTY_VALUE;

/**
 * namespace.instrumented section.target (noun).action (past tense verb)
 * accounts.authentication.password.attempted
 * accounts.authentication.password.succeeded
 * accounts.authentication.password.failed
 * <p>
 * http://matt.aimonetti.net/posts/2013/06/26/practical-guide-to-graphite-monitoring
 * http://obfuscurity.com/2012/05/Organizing-Your-Graphite-Metrics
 * http://metrics20.org
 *
 * @author cli
 */
public final class StatsdFilter extends AbstractFilter {

    private JsonArray m_tags;
    private Map<String, Mapping> m_mappings;
    private UnifiedMap<String, StatsdMeasurement> m_measurements;

    private static final String CONF_KEY_ADDRESS = "address";
    private static final String CONF_KEY_PORT = "port";
    private static final String CONF_KEY_MAPPINGS = "mappings";
    private static final String CONF_KEY_ADD_TAGS = "add-tags";

    // Configuration defaults
    private static final int DEF_PORT = 8125;
    private static final String DEF_HOST = "localhost";

    public StatsdFilter() {
        m_mappings = new HashMap();
        m_measurements = new UnifiedMap();
    }

    @Override
    protected void onStart() {

        final int port = config().getInteger(CONF_KEY_PORT, DEF_PORT);
        final String host = config().getString(CONF_KEY_ADDRESS, DEF_HOST);

        // Mappings
        JsonObject mappings = config().getJsonObject(CONF_KEY_MAPPINGS, new JsonObject());
        for (String prefix : mappings.fieldNames()) {
            JsonObject mapping = mappings.getJsonObject(prefix);
            m_mappings.put(prefix, Mapping.create(prefix, mapping));
        }

        // Tags to add
        m_tags = config().getJsonArray(CONF_KEY_ADD_TAGS, new JsonArray());

        //
        // Start listening to packets
        //
        DatagramSocketOptions opts = new DatagramSocketOptions();
        final DatagramSocket socket = vertx.createDatagramSocket(opts);
        socket.listen(port, host, ar -> {
            if (ar.succeeded()) {

                logger().info("Listening on {}:{}", host, port);
                socket.handler((packet) -> handlePacket(packet));

            } else {
                logger().error("Failed to bind to {}:{}", host, port,
                        ar.cause());
            }
        });
    }

    private void handlePacket(final DatagramPacket packet) {

        try {

            final Buffer buffer = packet.data();
            logger().trace("Packet size: {}", buffer.length());

            // Split into array of strings
            Iterable<String> lines = Splitter.on('\n')
                    .trimResults()
                    .omitEmptyStrings()
                    .split(buffer.toString()); // UTF-8

            for (String line : lines) {

                // Split line into StatsD datagram parts (+ datadog tag extension)
                // <bucket>:<value>|<type>|@<sample rate>|<#tag:value,tag:value>
                Iterator<String> parts = Splitter.on('|')
                        .split(line)
                        .iterator();

                // Bucket and value
                String bucketValue = parts.next();
                String bucket = bucketValue.substring(0, bucketValue.lastIndexOf(':'));
                String value = bucketValue.substring(bucketValue.lastIndexOf(':') + 1);

                String outAddr = getOutputAddress();
                if (value.length() > 0
                        && !Strings.isNullOrEmpty(outAddr)) {

                    // Measurement type (counter, gauge, timer) - sets are not supported
                    String type = parts.next();

                    // Existing or new measurement
                    StatsdMeasurement measurement = m_measurements.getIfAbsentPut(
                            bucket,
                            new StatsdMeasurement(
                                    bucket,
                                    type,
                                    packet.sender().host()));

                    measurement.handleValue(type, value);

                    while (parts.hasNext()) {

                        String part = parts.next();

                        if (part.startsWith("@")) {

                            // rate
                            measurement.handleRate(part.substring(1)); // Jump over '@'

                        } else if (part.startsWith("#")) {

                            // tags
                            String tags = part.substring(1); // Jump over '#'
                            Iterable<String> pairs = Splitter.on(',')
                                    .trimResults()
                                    .omitEmptyStrings()
                                    .split(tags);

                            // Add tags as event fields
                            for (String pair : pairs) {
                                String tagName = pair.substring(0, pair.indexOf(':'));
                                String tagValue = pair.substring(pair.indexOf(':') + 1);
                                measurement.addTag(tagName, tagValue);
                            }
                        } else {
                            logger().warn("Ignoring: {}", part);
                        }
                    }

                    // Handle possible mappings
                    if (!m_mappings.isEmpty()) {

                        String prefix = bucket.substring(0, bucket.indexOf('.'));
                        Mapping mapping = m_mappings.get(prefix);
                        if (mapping != null) {
                            logger().trace("Applying mapping: {}", mapping);
                            mapping.apply(measurement);
                        }
                    }

                    //
                    // Create new event per value
                    //
                    JsonObject event = FilterEvents.createMetricEvent(
                            this,
                            measurement.getHostname(),
                            measurement.getDsname(),
                            measurement.getDstype(),
                            measurement.getSubgroup(),
                            measurement.getInstance(),
                            EVENT_EMPTY_VALUE, // unit
                            0L, // interval
                            measurement.getValue());

                    // Millisecond precision by default
                    event.put(EVENT_FIELD_PRECISION, DSTIME_PRECISION_MILLIS);

                    // Add tags
                    event.put(EVENT_FIELD_TAGS, m_tags);

                    // Add extra fields
                    Map<String, String> tags = measurement.getTags();
                    for (Map.Entry<String, String> entry : tags.entrySet()) {
                        event.put(entry.getKey(), entry.getValue());
                    }

                    publishEvent(outAddr, event);
                }
            }

        } catch (Exception e) {
            logger().error("Failed to handle packet", e);
        }
    }

    private static class Mapping {

        private static final String CONF_KEY_SENDER = "sender";
        private static final String CONF_KEY_DSNAME = "dsname";
        private static final String CONF_KEY_MATCH = "match";
        private static final String CONF_KEY_INSTANCE = "instance";

        private final String m_prefix;
        private final int[] m_senderIndexes;
        private final int[] m_dsnameIndexes;
        private final List<MatchPair> m_matches;

        private Mapping(
                final String prefix,
                final int[] senderIndexes,
                final int[] dsnameIndexes) {

            m_prefix = prefix;
            m_senderIndexes = senderIndexes;
            m_dsnameIndexes = dsnameIndexes;
            m_matches = new ArrayList();
        }

        private void addMatchPair(final MatchPair match) {
            m_matches.add(match);
        }

        private void apply(final StatsdMeasurement measurement) {

            String bucket = measurement.getBucket();
            String[] items = Iterables.toArray(Splitter.on('.').split(bucket), String.class);

            // Resolve sender
            measurement.setSender(resolveName(m_senderIndexes, items));

            // Resolve dsname
            String dsname = resolveName(m_dsnameIndexes, items);
            if (dsname.length() > 0) {

                measurement.setDsname(dsname);

                // Continue with match pairs
                for (MatchPair match : m_matches) {
                    match.apply(
                            dsname,
                            measurement,
                            items);
                }
            }
        }

        private static String resolveName(
                final int[] indexes,
                final String[] items) {

            StringBuilder name = new StringBuilder();
            for (int i = 0; i < indexes.length; i++) {
                int idx = indexes[i];
                if (idx < items.length) {
                    name.append(items[idx]);
                    name.append("_");
                }
            }

            int len = name.length();
            if (len > 0) {
                return name.substring(0, len - 1);
            } else {
                return name.toString();
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("{prefix: ");
            sb.append(m_prefix);
            sb.append(" senderIndexes: ");
            sb.append(Arrays.toString(m_senderIndexes));
            sb.append(" dsnameIndexes: ");
            sb.append(Arrays.toString(m_dsnameIndexes));
            sb.append("}");
            return sb.toString();
        }

        private static Mapping create(
                final String prefix,
                final JsonObject json) {

            int[] senderIndexes = Ints.toArray(json.getJsonArray(CONF_KEY_SENDER, new JsonArray()).getList());
            int[] dsnameIndexes = Ints.toArray(json.getJsonArray(CONF_KEY_DSNAME, new JsonArray()).getList());

            Mapping mapping = new Mapping(
                    prefix,
                    senderIndexes,
                    dsnameIndexes);

            JsonArray matches = json.getJsonArray(CONF_KEY_MATCH, new JsonArray());
            for (int i = 0; i < matches.size(); i++) {

                JsonObject pair = matches.getJsonObject(i);
                String dsname = pair.getString(CONF_KEY_DSNAME);
                int[] instanceIndexes = Ints.toArray(pair.getJsonArray(CONF_KEY_INSTANCE, new JsonArray()).getList());

                mapping.addMatchPair(new MatchPair(dsname, instanceIndexes));
            }

            return mapping;
        }

    }

    private static class MatchPair {

        private final String m_dsname;
        private final int[] m_instanceIndexes;

        private MatchPair(
                final String dsname,
                final int[] instanceIndexes) {

            // Sanity checks
            Preconditions.checkArgument(!Strings.isNullOrEmpty(dsname), "dsname is not defined");
            Preconditions.checkArgument(instanceIndexes != null && instanceIndexes.length > 0, "instance indexes are not defined");

            m_dsname = dsname;
            m_instanceIndexes = instanceIndexes;
        }

        private void apply(
                final String dsname,
                final StatsdMeasurement measurement,
                final String[] items) {

            if (dsname.equals(m_dsname)) {
                measurement.setInstance(Mapping.resolveName(m_instanceIndexes, items));
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("{ dsname: ");
            sb.append(m_dsname);
            sb.append(" instanceIndexes: ");
            sb.append(Arrays.toString(m_instanceIndexes));
            sb.append("}");
            return sb.toString();
        }
    }
}

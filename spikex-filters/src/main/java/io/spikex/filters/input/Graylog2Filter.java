/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.input;

import io.spikex.base.helper.Events;
import io.spikex.filters.AbstractFilter;
import io.spikex.utils.HostOs;
import io.vertx.core.datagram.DatagramPacket;
import io.vertx.core.datagram.DatagramSocket;
import io.vertx.core.datagram.DatagramSocketOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import net.jodah.expiringmap.ExpiringMap;
import okio.GzipSource;
import okio.InflaterSource;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.zip.Inflater;

import static io.spikex.base.helper.Events.EVENT_FIELD_TAGS;
import static io.spikex.base.helper.Events.EVENT_TYPE_LOG;

/**
 * GELF message fields:
 * <pre>
 *  version - 1.1
 *  short_message - the log message
 *  timestamp - current timestamp
 *  level - log level
 *  source - the FQDN of the host that sent the message
 *  full_message - contains the stack trace when logging an exception
 * </pre>
 * <p>
 * Debug fields:
 * <pre>
 *  _file
 *  _line
 *  _module
 *  _func
 *  _logger_name
 * </pre>
 *
 * @author cli
 */
public final class Graylog2Filter extends AbstractFilter {

    private static final String CONF_KEY_ADDRESS = "address";
    private static final String CONF_KEY_PORT = "port";
    private static final String CONF_KEY_LOG_ACTIVITY = "log-activity";
    private static final String CONF_KEY_ADD_TAGS = "add-tags";

    private static final int GELF_MAX_CHUNKS = 128;
    private static final int GELF_MAX_CHUNK_SIZE = 1420;
    private static final int GELF_MAX_MESSAGE_SIZE = (GELF_MAX_CHUNKS * GELF_MAX_CHUNK_SIZE);

    // Configuration defaults
    private static final int DEF_PORT = 12201;
    private static final String DEF_ADDRESS = "127.0.0.1";
    private static final boolean DEF_LOG_ACTIVITY = false;

    private static final String SYS_PROP_GRAYLOG_MAP_MAX_SIZE = "io.spikex.greylog2.expiring.map.max.size";
    private static final String DEF_GRAYLOG_MAP_MAX_SIZE = "100";

    // Chunked messages (each expires within 5 seconds)
    // http://docs.graylog.org/en/2.2/pages/gelf.html
    private final Map<String, okio.Buffer[]> m_messages = ExpiringMap.builder()
            .maxSize(Integer.parseInt(System.getProperty(
                    SYS_PROP_GRAYLOG_MAP_MAX_SIZE,
                    DEF_GRAYLOG_MAP_MAX_SIZE)))
            .expiration(5, TimeUnit.SECONDS)
            .build();

    private JsonArray m_tags;

    @Override
    protected void onStart() {

        final int port = config().getInteger(CONF_KEY_PORT, DEF_PORT);
        final String host = config().getString(CONF_KEY_ADDRESS, DEF_ADDRESS);

        boolean logActivity = config().getBoolean(CONF_KEY_LOG_ACTIVITY, DEF_LOG_ACTIVITY);

        // Tags to add
        m_tags = config().getJsonArray(CONF_KEY_ADD_TAGS, new JsonArray());

        //
        // Start listening to packets
        //
        DatagramSocketOptions opts = new DatagramSocketOptions();
        opts.setLogActivity(logActivity);
        final DatagramSocket socket = vertx.createDatagramSocket(opts); // This can take seconds...
        socket.listen(port, host, ar -> {
            if (ar.succeeded()) {

                logger().info("Listening on {}:{}", host, port);

                socket.handler(packet -> {
                    try {
                        handlePacket(packet);
                    } catch (IOException e) {
                        logger().error("Failed to decode GELF packet", e);
                    }
                });

            } else {
                logger().error("Failed to bind to {}:{}", host, port,
                        ar.cause());
            }
        });
    }

    private void handlePacket(final DatagramPacket packet) throws IOException {

        okio.Buffer source = new okio.Buffer();
        source.write(packet.data().getBytes());

        source.require(2);
        byte n = source.getByte(0L);

        switch (n) {

            case 0x78: // zlib (deflate) message
                decompressAndEmit(source);
                break;

            case 0x1f: // gzipped message
                decompressAndEmit(source);
                break;

            case 0x1e: // chunked message
                logger().trace("Received chunked messsage: 0x1E");
                source.require(12); // magic bytes(2) + message ID(8) + seq(1) + chunks(1)
                source.readByteArray(2); // Jump over magic bytes
                String msgId = source.readByteString(8).hex();

                int seqNum = source.readByte();
                int seqCount = source.readByte();

                logger().trace("Received messsage: {} seq: {} chunks: {}",
                        msgId, seqNum, seqCount);

                // Sanity check
                if (seqNum > (GELF_MAX_CHUNKS - 1)
                        || seqCount > GELF_MAX_CHUNKS) {
                    throw new IOException("GELF client is trying to send too many chunks: "
                            + seqCount + " (max is " + GELF_MAX_CHUNKS + ")");
                }

                okio.Buffer chunk = new okio.Buffer();
                source.readAll(chunk);

                if (seqCount > 1) {

                    okio.Buffer[] chunks = m_messages.remove(msgId);
                    if (chunks == null) {
                        chunks = new okio.Buffer[seqCount];
                    }
                    chunks[seqNum] = chunk;

                    if (seqNum >= (seqCount - 1)) {
                        // All received
                        decompressAndEmit(chunks);
                    } else {
                        // Just store the chunk (until all have been received within 5 secs)
                        m_messages.put(msgId, chunks);
                    }

                } else {
                    // Only one chunk
                    decompressAndEmit(chunk);
                }
                break;
            default:
                logger().warn("Ignoring unsupported message type: {}",
                        String.format("%02X ", n));

        }
    }

    private void decompressAndEmit(final okio.Buffer[] chunks) throws IOException {
        okio.Buffer chunk = new okio.Buffer();
        for (okio.Buffer buffer : chunks) {
            chunk.writeAll(buffer);
        }
        decompressAndEmit(chunk);
    }

    private void decompressAndEmit(final okio.Buffer chunk) throws IOException {

        // Uncompress and emit chunk to next in chain
        String body = null;
        byte n = chunk.getByte(0L);
        switch (n) {
            case 0x78: // zlib (deflate) message
                logger().trace("Uncompressing zlib (deflate) messsage: 0x78");
                body = deflate(chunk).readUtf8();
                break;
            case 0x1f: // gzipped message
                logger().trace("Uncompressing gzipped messsage: 0x1F");
                body = gunzip(chunk).readUtf8();
                break;
            default:
                logger().warn("Ignoring unsupported chunk type: {}",
                        String.format("%02X ", n));
        }

        if (body != null) {

            JsonObject event = Events.createEvent(
                    getClass().getSimpleName(),
                    HostOs.hostName(),
                    EVENT_TYPE_LOG);

            // Add tags
            event.put(EVENT_FIELD_TAGS, m_tags);

            // Add GELF body to event
            JsonObject bodyJson = new JsonObject(body);
            event.mergeIn(bodyJson);
            publishEvent(event);
        } else {
            logger().warn("Ignoring event with empty body");
        }
    }

    private okio.Buffer deflate(final okio.Buffer buffer) throws IOException {
        okio.Buffer result = new okio.Buffer();
        InflaterSource source = new InflaterSource(buffer, new Inflater());
        while (source.read(result, GELF_MAX_MESSAGE_SIZE) != -1) ;
        return result;
    }

    private okio.Buffer gunzip(final okio.Buffer buffer) throws IOException {
        okio.Buffer result = new okio.Buffer();
        GzipSource source = new GzipSource(buffer);
        while (source.read(result, GELF_MAX_MESSAGE_SIZE) != -1) ;
        return result;
    }
}

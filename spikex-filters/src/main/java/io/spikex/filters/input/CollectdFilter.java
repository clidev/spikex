/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.input;

import com.google.common.base.Strings;
import io.spikex.filters.AbstractFilter;
import io.spikex.filters.internal.CollectdMeasurement;
import io.spikex.filters.internal.FilterEvents;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.datagram.DatagramPacket;
import io.vertx.core.datagram.DatagramSocket;
import io.vertx.core.datagram.DatagramSocketOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static io.spikex.base.helper.Events.*;
import static io.spikex.filters.internal.CollectdMeasurement.*;
import static io.spikex.filters.internal.CollectdTypes.*;
import static io.spikex.filters.internal.FilterEvents.EVENT_EMPTY_VALUE;

/**
 * namespace.instrumented section.target (noun).action (past tense verb)
 * accounts.authentication.password.attempted
 * accounts.authentication.password.succeeded
 * accounts.authentication.password.failed
 * <p>
 * http://matt.aimonetti.net/posts/2013/06/26/practical-guide-to-graphite-monitoring
 * http://obfuscurity.com/2012/05/Organizing-Your-Graphite-Metrics
 * http://metrics20.org
 *
 * @author cli
 */
public final class CollectdFilter extends AbstractFilter {

    private int m_secLevel;
    private JsonArray m_tags;
    private final CollectdMeasurement m_measurement;

    private static final String CONF_KEY_ADDRESS = "address";
    private static final String CONF_KEY_PORT = "port";
    private static final String CONF_KEY_MCAST_ENABLED = "multicast-enabled";
    private static final String CONF_KEY_MCAST_GROUP = "multicast-group";
    private static final String CONF_KEY_SECURITY_LEVEL = "security-level";
    private static final String CONF_KEY_ADD_TAGS = "add-tags";

    // Security levels
    private static final String SECURITY_LEVEL_NONE = "None";
    private static final String SECURITY_LEVEL_SIGN = "Sign";
    private static final String SECUIRYT_LEVEL_ENCRYPT = "Encrypt";

    private static final int SEC_NONE = 0;
    private static final int SEC_SIGN = 1;
    private static final int SEC_ENCRYPT = 2;

    // Configuration defaults
    private static final int DEF_PORT = 25826;
    private static final boolean DEF_MCAST_ENABLED = false;
    private static final String DEF_HOST = "localhost";
    private static final String DEF_MCAST_GROUP = "239.192.74.66";
    private static final String DEF_SECURITY_LEVEL = SECURITY_LEVEL_NONE;

    public CollectdFilter() {
        m_measurement = new CollectdMeasurement();
    }

    @Override
    protected void onStart() {

        final int port = config().getInteger(CONF_KEY_PORT, DEF_PORT);
        final String host = config().getString(CONF_KEY_ADDRESS, DEF_HOST);
        final boolean mcastEnabled = config().getBoolean(CONF_KEY_MCAST_ENABLED, DEF_MCAST_ENABLED);
        final String mcastGroup = config().getString(CONF_KEY_MCAST_GROUP, DEF_MCAST_GROUP);
        final String securityLevel = config().getString(CONF_KEY_SECURITY_LEVEL, DEF_SECURITY_LEVEL);

        m_secLevel = SEC_NONE;
        switch (securityLevel) {
            case SECURITY_LEVEL_SIGN:
                m_secLevel = SEC_SIGN;
                break;
            case SECUIRYT_LEVEL_ENCRYPT:
                m_secLevel = SEC_ENCRYPT;
                break;
        }

        // Tags to add
        m_tags = config().getJsonArray(CONF_KEY_ADD_TAGS, new JsonArray());

        //
        // Start listening to packets
        //
        DatagramSocketOptions opts = new DatagramSocketOptions();
        final DatagramSocket socket = vertx.createDatagramSocket(opts);
        socket.listen(port, host, ar -> {
            if (ar.succeeded()) {

                logger().info("Listening on {}:{}", host, port);
                socket.handler((packet) -> handlePacket(packet));

                if(mcastEnabled) {
                    // Also join multicast group
                    socket.listenMulticastGroup(mcastGroup, ar2 -> {
                        if (ar2.failed()) {
                            logger().error("Failed to join multicast group: {}",
                                    mcastGroup, ar2.cause());
                        }
                    });
                }

            } else {
                logger().error("Failed to bind to {}:{}", host, port,
                        ar.cause());
            }
        });
    }

    private void handlePacket(final DatagramPacket packet) {

        try {

            final Buffer buffer = packet.data();
            logger().trace("Packet size: {}", buffer.length());

            CollectdMeasurement measurement = m_measurement;
            measurement.clear();
            String precision = DSTIME_PRECISION_SEC; // Default

            int i = 0;
            while (i < buffer.length()) {

                // Next part
                int partType = buffer.getShort(i);
                int partLen = buffer.getShort(i + 2);
                int pos = i + 4;
                int strLen = i + partLen - 1;

                switch (partType) {
                    case TYPE_HOST: {
                        measurement.setHostname(buffer, pos, strLen);
                        break;
                    }
                    case TYPE_TIME_EPOCH: {
                        long tm = buffer.getLong(pos) * 1000L; // ms
                        measurement.setTimestamp(tm);
                        precision = DSTIME_PRECISION_SEC; // Seconds
                        break;
                    }
                    case TYPE_TIME_HIGHRES: {
                        long tm = (buffer.getLong(pos) / 1073741824L * 1000L); // ms
                        // Add millis - we want ms precision
                        long millis = System.currentTimeMillis();
                        long epoch = (millis / 1000L) * 1000L;
                        measurement.setTimestamp(tm + (millis - epoch));
                        precision = DSTIME_PRECISION_MILLIS; // Higher precision
                        break;
                    }
                    case TYPE_PLUGIN: {
                        measurement.setPlugin(buffer, pos, strLen);
                        break;
                    }
                    case TYPE_PLUGIN_INSTANCE: {
                        measurement.setPluginInstance(buffer, pos, strLen);
                        break;
                    }
                    case TYPE_TYPE: {
                        measurement.setType(buffer, pos, strLen);
                        break;
                    }
                    case TYPE_TYPE_INSTANCE: {
                        measurement.setTypeInstance(buffer, pos, strLen);
                        break;
                    }
                    case TYPE_VALUES: {
                        int numValues = buffer.getShort(pos++);
                        pos++;
                        // Read types
                        byte[] types = new byte[numValues];
                        for (int n = 0; n < numValues; n++) {
                            types[n] = buffer.getByte(pos++);
                        }
                        // Read actual values
                        for (int n = 0; n < numValues; n++) {
                            switch (types[n]) {
                                // COUNTER
                                case 0: {
                                    long value = buffer.getLong(pos);
                                    measurement.pushValue(DSTYPE_COUNTER, value);
                                    break;
                                }
                                // GAUGE
                                case 1: {
                                    ByteBuffer buf = ByteBuffer.wrap(buffer.getBytes(pos, pos + 8));
                                    double value = buf.order(ByteOrder.LITTLE_ENDIAN).getDouble();
                                    measurement.pushValue(DSTYPE_GAUGE, value);
                                    break;
                                }
                                // DERIVE
                                case 2: {
                                    long value = buffer.getLong(pos);
                                    measurement.pushValue(DSTYPE_DERIVE, value);
                                    break;
                                }
                                // ABSOLUTE
                                case 3: {
                                    long value = buffer.getLong(pos);
                                    measurement.pushValue(DSTYPE_ABSOLUTE, value);
                                    break;
                                }
                            }
                            pos += 8; // 64 bit field
                        }
                        break;
                    }
                    case TYPE_INTERVAL_RRD: {
                        long interval = buffer.getLong(i + 4);
                        measurement.setInterval(interval);
                        break;
                    }
                    case TYPE_INTERVAL_HIGHRES: {
                        long interval = (buffer.getLong(i + 4) / 1073741824L);
                        measurement.setInterval(interval);
                        break;
                    }
                    case TYPE_MESSAGE:
                        logger().trace("Message - not supported");
                        break;
                    case TYPE_SEVERITY:
                        logger().trace("Severity - not supported");
                        break;
                    case TYPE_SIGNATURE:
                        logger().trace("Signature - not supported");
                        break;
                    case TYPE_ENCRYPTION:
                        logger().trace("Encryption - not supported");
                        break;
                }

                String outAddr = getOutputAddress();
                if (measurement.hasValues()
                        && !Strings.isNullOrEmpty(outAddr)) {

                    logger().trace("{} - precision {}", measurement, precision);

                    String plugin = measurement.getPlugin();
                    String pluginInstance = measurement.getPluginInstance();

                    String type = measurement.getType();
                    String typeInstance = measurement.getTypeInstance();

                    String[] subgroups = TYPES_DB.get(type);
                    StringBuilder dsname = new StringBuilder(plugin);

                    // Do not append type if it has the same value as plugin
                    if (!plugin.equals(type)) {
                        dsname.append("-");
                        dsname.append(type);
                    }

                    for (int n = 0; n < measurement.getValueCount(); n++) {

                        Object value = measurement.getValue(n);
                        String dstype = measurement.getDstype(n);

                        String subgroup = EVENT_EMPTY_VALUE;
                        String instance = EVENT_EMPTY_VALUE;

                        if (subgroups != null && n < subgroups.length) {
                            subgroup = subgroups[n];
                        } else {
                            if (!Strings.isNullOrEmpty(typeInstance)) {
                                subgroup = typeInstance;
                            }
                        }

                        if (!Strings.isNullOrEmpty(pluginInstance)) {
                            instance = pluginInstance;
                        }

                        //
                        // Create new event per value
                        //
                        JsonObject event = FilterEvents.createMetricEvent(
                                this,
                                measurement.getTimestamp(),
                                TIMEZONE_UTC,
                                measurement.getHostname(),
                                dsname.toString(),
                                dstype,
                                precision,
                                subgroup,
                                instance,
                                EVENT_EMPTY_VALUE, // unit
                                measurement.getInterval(),
                                value);

                        // Add tags
                        event.put(EVENT_FIELD_TAGS, m_tags);

                        // Collectd specific fields
                        event.put(FIELD_PLUGIN, plugin);
                        event.put(FIELD_PLUGIN_INSTANCE, pluginInstance);
                        event.put(FIELD_TYPE, type);
                        event.put(FIELD_TYPE_INSTANCE, typeInstance);

                        publishEvent(outAddr, event);
                    }

                    measurement.clear(); // Ready for next round
                }

                i += partLen;
            }
        } catch (Exception e) {
            logger().error("Failed to handle packet", e);
        }
    }
}

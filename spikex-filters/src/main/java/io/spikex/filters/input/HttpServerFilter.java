/**
 * Copyright (c) 2017 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.filters.input;


import io.spikex.filters.AbstractFilter;
import io.spikex.filters.internal.ElasticsearchHandler;
import io.spikex.filters.internal.HttpServerEvent;
import io.spikex.filters.internal.JsonHandler;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;

/**
 * @author cli
 */
public final class HttpServerFilter extends AbstractFilter {

    private Handler<HttpServerEvent> m_handler;
    private io.vertx.core.http.HttpServer m_server;

    private static final String CONF_KEY_ADDRESS = "address";
    private static final String CONF_KEY_PORT = "port";
    private static final String CONF_KEY_HANDLER = "handler";
    private static final String CONF_KEY_SSL_ENABLED = "ssl-enabled";
    private static final String CONF_KEY_KEYSTORE_PATH = "keystore-path";
    private static final String CONF_KEY_KEYSTORE_PASSWORD = "keystore-password";
    private static final String CONF_KEY_KEYSTORE_TYPE = "keystore-type";
    private static final String CONF_KEY_TRUSTSTORE_PATH = "truststore-path";
    private static final String CONF_KEY_TRUSTSTORE_PASSWORD = "truststore-password";
    private static final String CONF_KEY_TRUSTSTORE_TYPE = "truststore-type";
    private static final String CONF_KEY_CLIENT_AUTH_REQUIRED = "client-auth-required";
    private static final String CONF_KEY_ADD_TAGS = "add-tags";

    // Handlers
    private static final String HANDLER_JSON = "json";
    private static final String HANDLER_ES = "elasticsearch";

    // Configuration defaults
    private static final int DEF_PORT = 44120;
    private static final String DEF_ADDRESS = "localhost";
    private static final String DEF_HANDLER = HANDLER_JSON;

    @Override
    protected void onStart() {

        final int port = config().getInteger(CONF_KEY_PORT, DEF_PORT);
        final String host = config().getString(CONF_KEY_ADDRESS, DEF_ADDRESS);
        String handler = config().getString(CONF_KEY_HANDLER, DEF_HANDLER);

        // Tags to add
        JsonArray tags = config().getJsonArray(CONF_KEY_ADD_TAGS, new JsonArray());

        switch (handler) {
            case HANDLER_JSON: {
                m_handler = new JsonHandler(this, eventBus(), tags);
            }
            break;
            case HANDLER_ES: {
                m_handler = new ElasticsearchHandler(this, eventBus(), tags);
            }
            break;
            default: {
                m_handler = new JsonHandler(this, eventBus(), tags);
            }
            break;
        }

        m_server = vertx.createHttpServer();
        m_server.requestHandler(request -> {

            final Buffer body = Buffer.buffer(0);

            request.handler(buffer -> {
                body.appendBuffer(buffer);
            });

            request.endHandler(v -> {

                // The entire body has now been received (keep it small)
                String data = body.toString();
                HttpServerResponse response = request.response();

                try {
                    logger().trace("Received: {}", data);
                    m_handler.handle(HttpServerEvent.create(data, request));
                } catch (Exception e) {
                    logger().error("Failed to parse: {}", data, e);
                    response.setStatusCode(500).end();
                } finally {
                    if (!response.ended()) {
                        response.end();
                    }
                }
            });
        });

        m_server.listen(port, host, ar -> {
            if (ar.succeeded()) {
                logger().info("Listening on {}:{}", host, port);
            } else {
                logger().error("Failed to bind to {}:{}", host, port);
            }
        });
    }

    @Override
    protected void onStop() {
        m_server.close();
    }
}

/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.input;

import com.amazonaws.services.ec2.model.Instance;
import com.google.common.base.Strings;
import io.spikex.base.helper.Events;
import io.spikex.base.helper.HostMetaData;
import io.spikex.filters.AbstractFilter;
import io.spikex.filters.internal.Ec2MetaDataClient;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;

import java.util.List;

import static io.spikex.base.helper.Events.*;
import static io.spikex.base.helper.HostMetaData.DSNAME_HOST_META_DATA;
import static io.spikex.base.helper.HostMetaData.SHARED_DATA_MAP;

/**
 *
 */
public final class Ec2MetaDataFilter extends AbstractFilter {

    private static final String CONF_KEY_REGION = "region";
    private static final String CONF_KEY_FILTERS = "filters";
    private static final String CONF_KEY_ENV_TAG = "tag-env";
    private static final String CONF_KEY_PLATFORM_TAG = "tag-platform";
    private static final String DEF_ENV_TAG = "Environment";
    private static final String DEF_PLATFORM_TAG = "Platform";
    private static final String CONF_KEY_ADD_TAGS = "add-tags";

    private static final long MIN_INTERVAL = 60L * 1000L; // 1 minute

    private String m_region;
    private SharedData m_sharedData;
    private Ec2MetaDataClient m_metaDataClient;
    private JsonArray m_tags;

    public Ec2MetaDataFilter() {
        super(MIN_INTERVAL);
    }

    @Override
    protected void onStart() {
        m_region = config().getString(CONF_KEY_REGION);
        m_sharedData = vertx.sharedData();

        // Tags to add
        m_tags = config().getJsonArray(CONF_KEY_ADD_TAGS, new JsonArray());

        JsonArray filters = config().getJsonArray(CONF_KEY_FILTERS, new JsonArray());
        m_metaDataClient = new Ec2MetaDataClient();
        m_metaDataClient.connect(m_region, filters);

        // Perform initial host lookup and building of meta data
        buildHostMetaData();
    }

    @Override
    protected void onStop() {
        m_metaDataClient.close();
    }

    @Override
    protected void onTimerEvent() {
        buildHostMetaData();
    }

    private void buildHostMetaData() {
        LocalMap<String, HostMetaData> sd = m_sharedData.getLocalMap(SHARED_DATA_MAP);
        List<Instance> instances = m_metaDataClient.fetchInstances();

        String envKey = config().getString(CONF_KEY_ENV_TAG, DEF_ENV_TAG);
        String platformKey = config().getString(CONF_KEY_PLATFORM_TAG, DEF_PLATFORM_TAG);

        for (Instance instance : instances) {
            HostMetaData metaData = Ec2MetaDataClient.createHostMetaData(
                    instance,
                    m_region,
                    envKey,
                    platformKey);

            sd.put(metaData.getHostName(), metaData);
            logger().trace("{}: {}", metaData.getHostName(), metaData.toJson());

            //
            // Also emit the data (inventory event)
            //
            JsonObject event = Events.createEvent(
                    getClass().getSimpleName(),
                    metaData.getHostName(),
                    EVENT_TYPE_LOG);

            // Add some useful metric fields
            event.put(EVENT_FIELD_DSNAME, DSNAME_HOST_META_DATA);
            event.put(EVENT_FIELD_DSTYPE, DSTYPE_STRING);
            event.put(EVENT_FIELD_INTERVAL, getUpdateInterval());
            event.put(EVENT_FIELD_PRECISION, DSTIME_PRECISION_MIN);
            event.put(EVENT_FIELD_VALUE, 0.0d);

            // HostMetaData specific fields
            event.mergeIn(metaData.toJson());
            publishMetric(event);
        }
    }

    private void publishMetric(final JsonObject event) {

        String outAddr = getOutputAddress();
        if (!Strings.isNullOrEmpty(outAddr)) {

            // Add tags
            event.put(EVENT_FIELD_TAGS, m_tags);

            publishEvent(outAddr, event);
        }
    }
}

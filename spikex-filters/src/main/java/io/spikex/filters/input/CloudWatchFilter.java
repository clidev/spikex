/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters.input;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.amazonaws.services.ec2.model.Instance;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import io.spikex.base.helper.Events;
import io.spikex.base.helper.HostMetaData;
import io.spikex.filters.AbstractFilter;
import io.spikex.filters.internal.Ec2MetaDataClient;
import io.spikex.filters.internal.FilterEvents;
import io.spikex.utils.HostOs;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static io.spikex.base.helper.Events.*;
import static io.spikex.filters.internal.FilterEvents.EVENT_EMPTY_VALUE;

/**
 *
 */
public final class CloudWatchFilter extends AbstractFilter {

    private static final String CONF_KEY_ENDPOINT = "endpoint";
    private static final String CONF_KEY_REGION = "region";
    private static final String CONF_KEY_FILTERS = "filters";
    private static final String CONF_KEY_INSTANCE_FILTERS = "instance-filters";
    private static final String CONF_KEY_PERIOD = "period";
    private static final String CONF_KEY_DIMENSIONS = "dimensions";
    private static final String CONF_KEY_STATISTICS = "statistics";
    private static final String CONF_KEY_METRICS = "metrics";
    private static final String CONF_KEY_ENV_TAG = "tag-env";
    private static final String CONF_KEY_PLATFORM_TAG = "tag-platform";
    private static final String CONF_KEY_INSTANCE = "instance";
    private static final String CONF_KEY_SUBGROUP = "subgroup";
    private static final String CONF_KEY_ADD_TAGS = "add-tags";

    private static final String NS_AWS_EC2 = "AWS/EC2";
    private static final String KEY_DIMENSION_INSTANCEID = "InstanceId";

    private static final int DEF_PERIOD = 60;
    private static final String DEF_ENV_TAG = "Environment";
    private static final String DEF_PLATFORM_TAG = "Platform";

    private static final long MIN_INTERVAL = 60L * 1000L; // 1 minute
    private static final String STATISTIC_AVERAGE = "avg";
    private static final String STATISTIC_SUM = "sum";
    private static final String STATISTIC_MAXIMUM = "max";
    private static final String STATISTIC_MINIMUM = "min";

    private AmazonCloudWatch m_cloudWatchClient;
    private List<MetricFilter> m_filters;
    private String m_region;
    private String m_envTag;
    private String m_platformTag;
    private JsonArray m_tags;

    public CloudWatchFilter() {
        super(MIN_INTERVAL);
    }

    @Override
    protected void onStart() {

        m_region = config().getString(CONF_KEY_REGION);
        m_envTag = config().getString(CONF_KEY_ENV_TAG, DEF_ENV_TAG);
        m_platformTag = config().getString(CONF_KEY_PLATFORM_TAG, DEF_PLATFORM_TAG);
        String endpoint = config().getString(CONF_KEY_ENDPOINT);
        JsonObject filters = config().getJsonObject(CONF_KEY_FILTERS, new JsonObject());

        // Tags to add
        m_tags = config().getJsonArray(CONF_KEY_ADD_TAGS, new JsonArray());

        // Sanity checks
        Preconditions.checkArgument(
                !Strings.isNullOrEmpty(endpoint), CONF_KEY_ENDPOINT + " must be defined");

        Preconditions.checkArgument(
                !Strings.isNullOrEmpty(m_region), CONF_KEY_REGION + " must be defined");

        // Parse metric filters
        m_filters = new ArrayList();
        Set<String> namespaces = filters.fieldNames();
        for (String namespace : namespaces) {

            JsonObject filter = filters.getJsonObject(namespace);
            MetricFilter metricFilter = new MetricFilter(
                    filter.getInteger(CONF_KEY_PERIOD, DEF_PERIOD),
                    namespace,
                    filter.getString(CONF_KEY_INSTANCE, ""),
                    filter.getString(CONF_KEY_SUBGROUP, ""));

            JsonObject dimensions = filter.getJsonObject(CONF_KEY_DIMENSIONS, new JsonObject());
            Set<String> names = dimensions.fieldNames();
            for (String name : names) {
                metricFilter.addDimension(name, dimensions.getString(name));
            }

            JsonArray statistics = filter.getJsonArray(CONF_KEY_STATISTICS, new JsonArray());
            for (int i = 0; i < statistics.size(); i++) {
                metricFilter.addStatistic(statistics.getString(i));
            }

            JsonArray metrics = filter.getJsonArray(CONF_KEY_METRICS, new JsonArray());
            for (int i = 0; i < metrics.size(); i++) {
                metricFilter.addMetric(metrics.getString(i));
            }

            // Instance filters
            JsonArray instanceFilters = filter.getJsonArray(CONF_KEY_INSTANCE_FILTERS, new JsonArray());
            metricFilter.setInstanceFilters(instanceFilters);

            m_filters.add(metricFilter);
        }

        m_cloudWatchClient = AmazonCloudWatchClientBuilder
                .standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, m_region))
                .build();
//                .withClientConfiguration()
    }

    @Override
    protected void onStop() {
        m_cloudWatchClient.shutdown();
    }

    @Override
    protected void onTimerEvent() {

        long interval = getUpdateInterval();

        // Minimal is 1 minute
        if (interval < MIN_INTERVAL) {
            interval = MIN_INTERVAL;
        }

        Instant now = Instant.now(); // UTC
        Date startTime = Date.from(now.minus(interval, ChronoUnit.MILLIS)); // now - interval
        Date endTime = Date.from(now); // now

        for (MetricFilter filter : m_filters) {

            // AWS/EC2 namespace
            if (NS_AWS_EC2.equals(filter.getNamespace())
                    && !filter.hasDimensions()) {

                // Fetch metrics per instance (if no dimensions specified)
                List<Instance> instances = fetchInstances(filter.getInstanceFilters());
                for (Instance instance : instances) {

                    List<Dimension> dimensions = new ArrayList();
                    dimensions.add(new Dimension()
                            .withName(KEY_DIMENSION_INSTANCEID)
                            .withValue(instance.getInstanceId()));

                    fetchAndPublishMetric(
                            filter,
                            dimensions,
                            startTime,
                            endTime,
                            instance);
                }
            } else {
                fetchAndPublishMetric(
                        filter,
                        filter.getDimensions(),
                        startTime,
                        endTime,
                        null);
            }
        }
    }

    private List<Instance> fetchInstances(final JsonArray instanceFilters) {

        List<Instance> instances = new ArrayList();

        if (instanceFilters.size() > 0) {
            Ec2MetaDataClient client = new Ec2MetaDataClient();

            try {
                client.connect(m_region, instanceFilters);
                instances = client.fetchInstances();
            } finally {
                client.close();
            }
        }

        return instances;
    }

    private void fetchAndPublishMetric(
            final MetricFilter filter,
            final List<Dimension> dimensions,
            final Date startTime,
            final Date endTime,
            final Instance instance) {

        for (String metric : filter.getMetrics()) {

            StringBuilder dsname = new StringBuilder();
            dsname.append(filter.getNamespace().toLowerCase().replace('/', '_'));
            dsname.append("_");
            dsname.append(metric.toLowerCase());

            logger().debug("Fetching metric: {} (dimensions: {})",
                    dsname.toString(),
                    filter.getDimensions());

            GetMetricStatisticsRequest request = new GetMetricStatisticsRequest();
            request.setMetricName(metric);
            request.setNamespace(filter.getNamespace());
            request.setDimensions(dimensions);
            request.setStatistics(filter.getStatistics());
            request.setPeriod(filter.getPeriod());
            request.setStartTime(startTime);
            request.setEndTime(endTime);

            GetMetricStatisticsResult result = m_cloudWatchClient.getMetricStatistics(request);
            for (Datapoint datapoint : result.getDatapoints()) {
                //
                // Create new event per datapoint and statistic
                //
                Double avg = datapoint.getAverage();
                Double min = datapoint.getMinimum();
                Double max = datapoint.getMaximum();
                Double sum = datapoint.getSum();

                if (avg != null) {
                    publishMetric(
                            filter,
                            instance,
                            datapoint,
                            dsname,
                            STATISTIC_AVERAGE,
                            avg);
                }
                if (min != null) {
                    publishMetric(
                            filter,
                            instance,
                            datapoint,
                            dsname,
                            STATISTIC_MINIMUM,
                            avg);
                }
                if (max != null) {
                    publishMetric(
                            filter,
                            instance,
                            datapoint,
                            dsname,
                            STATISTIC_MAXIMUM,
                            avg);
                }
                if (sum != null) {
                    publishMetric(
                            filter,
                            instance,
                            datapoint,
                            dsname,
                            STATISTIC_SUM,
                            avg);
                }
            }
        }
    }

    private void publishMetric(
            final MetricFilter filter,
            final Instance instance,
            final Datapoint datapoint,
            final StringBuilder dsname,
            final String statistic,
            final Double value) {

        JsonObject event;
        long timestamp = datapoint.getTimestamp().getTime();

        // Special case (InstanceId)
        if (NS_AWS_EC2.equals(filter.getNamespace())
                && instance != null) {

            HostMetaData metaData = Ec2MetaDataClient.createHostMetaData(
                    instance,
                    m_region,
                    m_envTag,
                    m_platformTag);

            event = FilterEvents.createMetricEvent(
                    this,
                    timestamp,
                    TIMEZONE_UTC,
                    metaData.getHostName(),
                    dsname.toString(),
                    Events.DSTYPE_GAUGE,
                    DSTIME_PRECISION_MIN,
                    statistic,
                    EVENT_EMPTY_VALUE,
                    datapoint.getUnit(),
                    getUpdateInterval(),
                    value);

            // HostMetaData specific fields
            event.mergeIn(metaData.toJson());

        } else {
            event = FilterEvents.createMetricEvent(
                    this,
                    timestamp,
                    TIMEZONE_UTC,
                    HostOs.hostName(),
                    dsname.toString(),
                    Events.DSTYPE_GAUGE,
                    DSTIME_PRECISION_MIN,
                    statistic,
                    EVENT_EMPTY_VALUE,
                    datapoint.getUnit(),
                    getUpdateInterval(),
                    value);
        }

        String outAddr = getOutputAddress();
        if (!Strings.isNullOrEmpty(outAddr)) {

            // Add tags
            event.put(EVENT_FIELD_TAGS, m_tags);

            publishEvent(outAddr, event);
        }
    }

    private static class MetricFilter {

        private final int m_period;
        private final String m_namespace;
        private final String m_instance;
        private final String m_subgroup;
        private final List<String> m_metrics;
        private final List<String> m_statistics;
        private final List<Dimension> m_dimensions;
        private JsonArray m_instanceFilters;

        private MetricFilter(
                final int period,
                final String namespace,
                final String instance,
                final String subgroup) {

            m_period = period;
            m_namespace = namespace;
            m_instance = instance;
            m_subgroup = subgroup;
            m_metrics = new ArrayList();
            m_statistics = new ArrayList();
            m_dimensions = new ArrayList();
        }

        private boolean hasDimensions() {
            return !m_dimensions.isEmpty();
        }

        private int getPeriod() {
            return m_period;
        }

        private String getNamespace() {
            return m_namespace;
        }

        private String getInstance() {
            return m_instance;
        }

        private String getSubgroup() {
            return m_subgroup;
        }

        private List<String> getMetrics() {
            return m_metrics;
        }

        private List<String> getStatistics() {
            return m_statistics;
        }

        private List<Dimension> getDimensions() {
            return m_dimensions;
        }

        private JsonArray getInstanceFilters() {
            return m_instanceFilters;
        }

        private void addDimension(
                final String name,
                final String value) {

            m_dimensions.add(new Dimension()
                    .withName(name)
                    .withValue(value));
        }

        private void addStatistic(final String statistic) {
            m_statistics.add(statistic);
        }

        private void addMetric(final String metric) {
            m_metrics.add(metric);
        }

        private void setInstanceFilters(final JsonArray instanceFilters) {
            m_instanceFilters = instanceFilters;
        }
    }
}

/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters;

import io.spikex.filters.internal.Mapping;
import io.spikex.filters.internal.Rule;
import io.spikex.utils.Environment;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.spikex.base.helper.Events.EVENT_FIELD_BATCH_EVENTS;

/**
 * @author cli
 */
public final class MappingFilter extends AbstractFilter {

    private final Map<String, Mapping> m_mappings; // action-id => action
    private final List<Rule> m_rules;

    //
    // Configuration defaults
    //
    private static final String[] DEF_MAPPINGS = {
            "classpath:/mapping/common.json"
    };

    private static final String CONF_KEY_MAPPINGS = "mappings";

    public MappingFilter() {
        m_mappings = new HashMap();
        m_rules = new ArrayList();
    }

    @Override
    protected void onStart() {

        // Reset state
        m_mappings.clear();
        m_rules.clear();

        // Create mapping directory if it doesn't exist
        File mappingDir = new File(Environment.configHome(), "mapping");
        boolean exists = mappingDir.exists();
        if (!exists) {
            if (!mappingDir.mkdirs()) {
                throw new IllegalStateException(
                        "Failed to create mapping directory in: "
                                + Environment.configHome());
            }
        }

        JsonArray defUrls = new JsonArray(Arrays.asList(DEF_MAPPINGS));
        JsonArray urls = config().getJsonArray(CONF_KEY_MAPPINGS, defUrls);

        for (int i = 0; i < urls.size(); i++) {

            String url = urls.getString(i);
            URI uri = variables().resolveUrl(getClass(), url);

            try {
                Path uriPath = Paths.get(uri); // Mapping file URI
                String filename = uriPath.getFileName().toString();
                Path mappingPath = Paths.get(mappingDir.getAbsolutePath(), filename);

                // Copy mapping file to local directory
                io.spikex.utils.Files.downloadOrCopy(uri, mappingPath, false);
                m_mappings.putAll(addMappingsFromFile(mappingPath));

            } catch (IOException e) {
                throw new IllegalStateException("Failed to add mapping: "
                        + uri.toString(), e);
            }

        }
    }

    @Override
    protected void onEvent(final JsonObject batchEvent) {
        //
        // Operate on arrays only (batches)
        //
        JsonArray batch = batchEvent.getJsonArray(EVENT_FIELD_BATCH_EVENTS, new JsonArray());
        if (!batchEvent.containsKey(EVENT_FIELD_BATCH_EVENTS)) {
            batch.add(batchEvent);
        }

        for (int i = 0; i < batch.size(); i++) {

            JsonObject event = batch.getJsonObject(i);

            //
            // Try to match rules
            //
            Map<String, Mapping> mappings = m_mappings;
            List<Rule> rules = m_rules;

            for (Rule rule : rules) {
                logger().trace("Evaluating rule: {}", rule);
                if (rule.match(event)) {
                    Mapping mapping = mappings.get(rule.getAction());
                    logger().trace("Applying mapping: {}", mapping);
                    mapping.apply(variables(), event);
                }
            }
        }

        publishEvent(batchEvent);
    }

    private Map<String, Mapping> addMappingsFromFile(final Path mappingPath) {

        Map<String, Mapping> mappings = new HashMap();
        String filename = mappingPath.getFileName().toString();
        logger().debug("Parsing mappings: {}", mappingPath);

        JsonArray values = io.spikex.utils.Files.readJsonArray(mappingPath);
        for (int i = 0; i < values.size(); i++) {
            JsonObject json = values.getJsonObject(i);
            String key = filename + i;
            Rule rule = Rule.create(key, json);
            Mapping mapping = Mapping.create(vertx, json);
            m_rules.add(rule);
            m_mappings.put(key, mapping);
        }

        return mappings;
    }
}

/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters;

import ch.hsr.geohash.GeoHash;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.maxmind.db.CHMCache;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.WebServiceClient;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.City;
import com.maxmind.geoip2.record.Country;
import com.maxmind.geoip2.record.Location;
import com.maxmind.geoip2.record.Subdivision;
import io.spikex.base.helper.GeoMetaData;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

import static io.spikex.base.helper.Events.EVENT_FIELD_BATCH_EVENTS;
import static io.spikex.base.helper.GeoMetaData.SHARED_DATA_MAP;
import static io.spikex.base.helper.HostMetaData.EVENT_FIELD_HOST_PUBLICIP;
import static io.spikex.filters.internal.FilterEvents.EVENT_EMPTY_VALUE;

public final class GeoIp2Filter extends AbstractFilter {

    private static final String CONF_KEY_DATABASE = "database";
    private static final String CONF_KEY_USER_ID = "user-id";
    private static final String CONF_KEY_LICENSE_KEY = "license-key";
    private static final String CONF_KEY_CONNECT_TIMEOUT = "connect-timeout";
    private static final String CONF_KEY_READ_TIMEOUT = "read-timeout";
    private static final String CONF_KEY_ADDRESS_FIELD = "address-field";
    private static final String CONF_KEY_REFRESH_INTERVAL = "refresh-interval";

    private static final String DEF_ADDRESS_FIELD = EVENT_FIELD_HOST_PUBLICIP;
    private static final int DEF_CONNECT_TIMEOUT = 1500;
    private static final int DEF_READ_TIMEOUT = 3000;
    private static final long DEF_REFRESH_INTERVAL = 15L * 60L * 1000L; // 15 min

    private static final String TYPE_GEOIP2_DB = "GeoIP2-DB";
    private static final String TYPE_GEOIP2_WS = "GeoIP2-WS";

    private SharedData m_sharedData;
    private String m_addressField;
    private long m_refreshInterval;
    private WebServiceClient m_webServiceclient;
    private DatabaseReader m_databaseReader;

    @Override
    protected void onStart() {

        String database = config().getString(CONF_KEY_DATABASE, "");
        int userId = config().getInteger(CONF_KEY_USER_ID, 0);
        String licenceKey = config().getString(CONF_KEY_LICENSE_KEY);

        // Sanity check
        Preconditions.checkArgument(!Strings.isNullOrEmpty(database)
                        || (userId > 0 && !Strings.isNullOrEmpty(licenceKey)),
                "Either database or user-id and license-key must be defined");

        m_sharedData = vertx.sharedData();
        m_addressField = config().getString(CONF_KEY_ADDRESS_FIELD, DEF_ADDRESS_FIELD);
        m_refreshInterval = config().getLong(CONF_KEY_REFRESH_INTERVAL, DEF_REFRESH_INTERVAL);

        try {
            if (!Strings.isNullOrEmpty(database)) {
                m_databaseReader = new DatabaseReader.Builder(new File((String) variables().translate(database)))
                        .withCache(new CHMCache())
                        .build();
            } else {
                m_webServiceclient = new WebServiceClient.Builder(userId, licenceKey)
                        .connectTimeout(config().getInteger(CONF_KEY_CONNECT_TIMEOUT, DEF_CONNECT_TIMEOUT))
                        .readTimeout(config().getInteger(CONF_KEY_READ_TIMEOUT, DEF_READ_TIMEOUT))
                        .build();
            }
        } catch (IOException e) {
            logger().error("Failed to initialize GeoIP client", e);
        }
    }

    @Override
    protected void onStop() {

        try {
            if (m_databaseReader != null) {
                m_databaseReader.close();
            }
        } catch (IOException e) {
            logger().error("Failed to close database", e);
        }

        try {
            if (m_webServiceclient != null) {
                m_webServiceclient.close();
            }
        } catch (IOException e) {
            logger().error("Failed to close connection", e);
        }
    }

    @Override
    protected void onTimerEvent() {
        // Update GeoIP database
    }

    @Override
    protected void onEvent(final JsonObject batchEvent) {
        //
        // Operate on arrays only (batches)
        //
        JsonArray batch = batchEvent.getJsonArray(EVENT_FIELD_BATCH_EVENTS, new JsonArray());
        if (!batchEvent.containsKey(EVENT_FIELD_BATCH_EVENTS)) {
            batch.add(batchEvent);
        }

        if (m_databaseReader != null || m_webServiceclient != null) {

            LocalMap<String, GeoMetaData> sd = m_sharedData.getLocalMap(SHARED_DATA_MAP);
            long tm = System.currentTimeMillis();

            for (int i = 0; i < batch.size(); i++) {

                JsonObject event = batch.getJsonObject(i);

                String address = event.getString(m_addressField);
                GeoMetaData metaData = sd.get(address);

                if (!Strings.isNullOrEmpty(address)
                        && (metaData == null || ((tm - metaData.getCreationTimestamp()) > m_refreshInterval))) {

                    try {
                        InetAddress ipAddress = InetAddress.getByName(address);

                        if (m_databaseReader != null) {
                            logger().trace("Doing database lookup for: {}", address);
                            CityResponse response = m_databaseReader.city(ipAddress);
                            metaData = createMetaData(TYPE_GEOIP2_DB, address, response);
                        }

                        if (m_webServiceclient != null) {
                            logger().trace("Doing web service lookup for: {}", address);
                            CityResponse response = m_webServiceclient.city(ipAddress);
                            metaData = createMetaData(TYPE_GEOIP2_WS, address, response);
                        }

                        sd.put(address, metaData);
                        logger().trace("{}: {}", address, metaData.toJson());

                    } catch (Exception e) {
                        logger().error("Failed to perform GeoIP2 lookup for: " + address, e);
                    }
                }
            }
        }

        publishEvent(batchEvent);
    }

    private GeoMetaData createMetaData(
            final String type,
            final String address,
            final CityResponse response) {

        String countryIsoCode = EVENT_EMPTY_VALUE;
        Country country = response.getCountry();
        if (country != null) {
            countryIsoCode = country.getIsoCode();
        }

        String cityName = EVENT_EMPTY_VALUE;
        City city = response.getCity();
        if (city != null) {
            cityName = city.getName();
        }

        String area = EVENT_EMPTY_VALUE;
        Subdivision subdivision = response.getMostSpecificSubdivision();
        if (subdivision != null) {
            area = subdivision.getIsoCode(); // State, municipality, etc..
        }

        Double latitude = 0.0d;
        Double longitude = 0.0d;
        String geoHash = EVENT_EMPTY_VALUE;

        Location location = response.getLocation();
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            // Calculate geohash
            geoHash = GeoHash.withCharacterPrecision(latitude, longitude, 12).toBase32();
        }

        return GeoMetaData.builder()
                .withType(type)
                .withHash(geoHash)
                .withAddress(address)
                .withCountry(countryIsoCode)
                .withCity(cityName)
                .withArea(area)
                .withLatitude(latitude)
                .withLongitude(longitude)
                .build();
    }
}

/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.io.CharStreams;
import io.spikex.base.BaseVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import static io.spikex.base.helper.VerticleHelper.KEY_CONFIGS;
import static io.spikex.filters.AbstractFilter.*;

/**
 * @author cli
 */
public final class MainVerticle extends BaseVerticle {

    private JsonObject m_filters;

    public static final String KEY_CHAIN = "chain";
    public static final String KEY_INPUT = "input";
    public static final String KEY_OUTPUTS = "outputs";
    public static final String KEY_FILTER = "filter";
    public static final String KEY_CONFIG = "config";

    private static final String FILENAME_FILTERS = "/filters.json";
    private static final String SUFFIX_JSON = ".json";

    private static final String KEY_MAX_WORKER_EXEC_TIME = "spikex.filters.max.worker.exec.time";
    private static final String DEF_MAX_WORKER_EXEC_TIME = "10000000000"; // 10 sec

    private final Logger LOG = LoggerFactory.getLogger(MainVerticle.class);

    public void onStart() {

        try {
            // Load filter -> class name mappings
            try (InputStream in = getClass().getResourceAsStream(FILENAME_FILTERS)) {
                m_filters = new JsonObject(CharStreams.toString(new InputStreamReader(in)));
                LOG.trace("Parsed {}", FILENAME_FILTERS);
            }
        } catch (Exception e) {
            throw new IllegalStateException(FILENAME_FILTERS + " not found", e);
        }

        //
        // Initialize filter chains
        //
        JsonArray configs = config().getJsonArray(KEY_CONFIGS);
        LOG.trace("configs: {}", configs);

        configs.forEach(config -> {

            try {
                JsonObject chainConfig = loadConfig(config.toString());
                final String filename = resolveFilename(config.toString());

                //
                // Input filter (worker)
                //
                JsonObject input = chainConfig.getJsonObject(KEY_INPUT);
                final String[] address = {
                        startFilter(input,
                                filename,
                                KEY_INPUT,
                                "",
                                true)};

                //
                // Chain (non-workers)
                //
                JsonArray chain = chainConfig.getJsonArray(KEY_CHAIN, new JsonArray());
                chain.forEach(filter -> {
                    address[0] = startFilter((JsonObject) filter,
                            filename,
                            KEY_CHAIN,
                            address[0],
                            false);
                });

                //
                // Output filters (workers)
                //
                JsonArray outputs = chainConfig.getJsonArray(KEY_OUTPUTS, new JsonArray());
                outputs.forEach(output -> {
                    startFilter((JsonObject) output,
                            filename,
                            KEY_CHAIN,
                            address[0],
                            true);
                });
            } catch (IOException e) {
                throw new IllegalStateException("Failed to create filter chain: " + config, e);
            }
        });
    }

    private String startFilter(
            final JsonObject section,
            final String chainName,
            final String sectionName,
            final String addressInput,
            final boolean worker) {

        // Sanity checks
        Preconditions.checkNotNull(section, chainName + ": "
                + sectionName + " must be defined: "
                + "\"" + sectionName + "\": { ... }");

        Preconditions.checkArgument(!section.isEmpty(), chainName + ": "
                + sectionName + " must not be empty");

        String filterName = section.getString(KEY_FILTER);

        // Sanity checks
        Preconditions.checkNotNull(filterName, chainName + ": "
                + "filter must be defined: "
                + "\"" + sectionName + "\": { \"filter\": \"...\" }");

        Preconditions.checkArgument(!filterName.isEmpty(), chainName + ": "
                + "filter must not be empty");

        String className = m_filters.getString(filterName);
        Preconditions.checkNotNull(className,
                "filter name to class mapping is missing for "
                        + filterName
                        + " from " + FILENAME_FILTERS);

        JsonObject filterConfig = section.getJsonObject(KEY_CONFIG,
                new JsonObject());

        filterConfig.put(KEY_CHAIN_NAME, chainName);

        // Input and output address
        String addressOutput = chainName + "-" + filterName;
        filterConfig.put(KEY_ADDRESS_INPUT, addressInput);
        filterConfig.put(KEY_ADDRESS_OUTPUT, addressOutput);

        // Deploy filter
        DeploymentOptions opts = new DeploymentOptions();
        opts.setConfig(filterConfig);
        opts.setWorker(worker);
        long maxWorkerExecTime = Long.parseLong(
                System.getProperty(
                        KEY_MAX_WORKER_EXEC_TIME,
                        DEF_MAX_WORKER_EXEC_TIME));
        opts.setMaxWorkerExecuteTime(maxWorkerExecTime);

        LOG.info("Starting filter: {} (in: {} out: {})",
                className,
                addressInput,
                addressOutput);

        getVertx().deployVerticle(className, opts);

        return addressOutput;
    }

    private String resolveFilename(final String config) {
        //
        // Resolve filename
        // Support %{#spikex.xyz} variables
        //
        URI configUri = URI.create(variables().substitute(config));
        String filename = new File(configUri.toString()).getName();

        Preconditions.checkArgument(!Strings.isNullOrEmpty(filename),
                "No filename found in: " + config);

        // Strip off .json suffix
        int pos = filename.lastIndexOf(SUFFIX_JSON);
        if (pos != -1) {
            filename = filename.substring(0, pos);
        }

        return filename;
    }
}

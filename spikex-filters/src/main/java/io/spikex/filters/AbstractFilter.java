/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters;

import io.spikex.base.BaseVerticle;
import io.vertx.core.json.JsonObject;

/**
 * @author cli
 */
public abstract class AbstractFilter extends BaseVerticle {

    public static final String KEY_CHAIN_NAME = "chain";
    public static final String KEY_ADDRESS_INPUT = "address-input";
    public static final String KEY_ADDRESS_OUTPUT = "address-output";

    public String getChainName() {
        return config().getString(KEY_CHAIN_NAME, "");
    }

    public String getInputAddress() {
        return config().getString(KEY_ADDRESS_INPUT, "");
    }

    public String getOutputAddress() {
        return config().getString(KEY_ADDRESS_OUTPUT, "");
    }

    public AbstractFilter() {
    }

    public AbstractFilter(final long minInterval) {
        super(minInterval);
    }

    public void start() {

        super.start();

        //
        // Register event consumer
        //
        eventBus().consumer(getInputAddress(), message -> {
            onEvent((JsonObject) message.body());
        });
    }

    protected void onEvent(final JsonObject event) {
        // Do nothing by default...
    }

    protected void publishEvent(final JsonObject event) {

        if (logger().isTraceEnabled()) {
            logger().trace("Publishing event: {} dest-address: {}",
                    event.encode(),
                    getOutputAddress());
        }

        vertx.eventBus().publish(getOutputAddress(), event);
    }

    protected void publishEvent(
            final String outputAddress,
            final JsonObject event) {

        if (logger().isTraceEnabled()) {
            logger().trace("Publishing event: {} dest-address: {}",
                    event.encode(),
                    outputAddress);
        }

        vertx.eventBus().publish(outputAddress, event);
    }

    protected void sendEvent(final JsonObject event) {

        if (logger().isTraceEnabled()) {
            logger().trace("Sending event: {} address: {}",
                    event.encode(),
                    getOutputAddress());
        }

        vertx.eventBus().send(getOutputAddress(), event);
    }
}

/**
 * Copyright (c) 2015 NG Modular Oy.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.spikex.filters;

import com.google.common.base.Preconditions;
import io.spikex.filters.internal.FilterEvents;
import io.spikex.utils.Environment;
import io.thekraken.grok.api.Grok;
import io.thekraken.grok.api.Match;
import io.thekraken.grok.api.exception.GrokException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static io.spikex.base.helper.Events.EVENT_FIELD_BATCH_EVENTS;
import static io.spikex.base.helper.Events.EVENT_FIELD_TAGS;

/**
 * Example:
 * <pre>
 *  "chain": [
 *          { "Grok": {
 *                   "patterns": [
 *                       "file:%{#spikex.config}/grok/base.grok",
 *                       "file:%{#spikex.config}/grok/log.grok"
 *                   ],
 *                   "input-field": "@message",
 *                   "output-field": "@message",
 *                   "group": {
 *                       "fields": ["class", "method", "message", "thread", "level",
 *                                  "year", "month", "day", "hour", "minute", "second"],
 *                       "output-field": "@fields"
 *                   },
 *                   "match-lines": [
 *                      {
 *                       "pattern": "%{JAVAJBOSS4LOG:line}",
 *                       "tags": ["log", "java"],
 *                       "ignore": ["JAVALVLCLS"]
 *                      }
 *                   ],
 *                   "multi-line": {
 *                       "pattern": "%{JAVAEXCEPTION:line}",
 *                       "tags": ["error", "exception"],
 *                       "segment-field": "class"
 *                   }
 *               }
 *          }
 *  ]
 * </pre>
 *
 * @author cli
 */
public final class GrokFilter extends AbstractFilter {

    private final Grok m_grokMulti;
    private final String[] m_multiTags;
    private final List<String> m_groupFields;
    private final List<MatchLine> m_matchLines;
    private final MultiLine m_multiLine;
    private String m_inputField;
    private String m_outputField;
    private String m_groupField;

    //
    // Configuration defaults
    //
    private static final String[] DEF_PATTERNS = {
            "classpath:/grok/patterns",
            "classpath:/grok/firewalls",
            "classpath:/grok/linux-syslog",
            "classpath:/grok/haproxy",
            "classpath:/grok/nagios",
            "classpath:/grok/java",
            "classpath:/grok/ruby"
    };

    private static final int MAX_TAG_COUNT = 32;
    private static final int MAX_LINE_COUNT = 1000;

    private static final String CONF_KEY_TAGS = "tags";
    private static final String CONF_KEY_GROUP = "group";
    private static final String CONF_KEY_FIELDS = "fields";
    private static final String CONF_KEY_IGNORE = "ignore";
    private static final String CONF_KEY_PATTERN = "pattern";
    private static final String CONF_KEY_PATTERNS = "patterns";
    private static final String CONF_KEY_MATCH_LINES = "match-lines";
    private static final String CONF_KEY_MULTI_LINE = "multi-line";
    private static final String CONF_KEY_INPUT_FIELD = "input-field";
    private static final String CONF_KEY_OUTPUT_FIELD = "output-field";
    private static final String CONF_KEY_SEGMENT_FIELD = "segment-field";

    private static final String DEF_INPUT_FIELD = "@message";
    private static final String DEF_OUTPUT_FIELD = "@message";
    private static final String DEF_GROUP_FIELD = "@fields";

    public GrokFilter() {
        m_grokMulti = new Grok();
        m_multiTags = new String[MAX_TAG_COUNT];
        m_matchLines = new ArrayList();
        m_multiLine = new MultiLine(MAX_LINE_COUNT, DEF_GROUP_FIELD);
        m_groupFields = new ArrayList();
        m_groupField = "";
    }

    @Override
    protected void onStart() {

        m_matchLines.clear();
        m_groupFields.clear();
        m_multiLine.clear();
        Arrays.fill(m_multiTags, null);

        // Create grok directory if it doesn't exist
        File grokDir = new File(Environment.configHome(), "grok");
        boolean exists = grokDir.exists();
        if (!exists) {
            if (!grokDir.mkdirs()) {
                throw new IllegalStateException(
                        "Failed to create grok directory in: "
                                + Environment.configHome());
            }
        }

        // input and output fields
        m_inputField = config().getString(CONF_KEY_INPUT_FIELD, DEF_INPUT_FIELD);
        m_outputField = config().getString(CONF_KEY_OUTPUT_FIELD, DEF_OUTPUT_FIELD);
        m_multiLine.setOutputField(m_outputField);

        // Group fields
        JsonObject group = config().getJsonObject(CONF_KEY_GROUP, new JsonObject());
        m_groupField = group.getString(CONF_KEY_GROUP, DEF_GROUP_FIELD);
        JsonArray groupFields = group.getJsonArray(CONF_KEY_FIELDS, new JsonArray());
        for (int i = 0; i < groupFields.size(); i++) {
            m_groupFields.add(groupFields.getString(i));
        }
        m_multiLine.setGroupField(m_groupField);

        // Pre build match lines
        // match-lines
        if (config().containsKey(CONF_KEY_MATCH_LINES)) {
            JsonArray matchLines = config().getJsonArray(CONF_KEY_MATCH_LINES, new JsonArray());
            for (int i = 0; i < matchLines.size(); i++) {
                MatchLine matcher = new MatchLine();
                m_matchLines.add(matcher);
            }
        }

        // grok-urls
        JsonArray defUrls = new JsonArray(Arrays.asList(DEF_PATTERNS));
        JsonArray urls = config().getJsonArray(CONF_KEY_PATTERNS, defUrls);

        for (int i = 0; i < urls.size(); i++) {

            String url = urls.getString(i);
            URI uri = variables().resolveUrl(getClass(), url);

            try {
                Path uriPath = Paths.get(uri); // Grok file URI
                String filename = uriPath.getFileName().toString();
                Path grokPath = Paths.get(grokDir.getAbsolutePath(), filename);

                // Copy grok file to local directory
                io.spikex.utils.Files.downloadOrCopy(uri, grokPath, false);

                for (MatchLine matcher : m_matchLines) {
                    matcher.addPatternFromFile(grokPath.toString());
                }
                m_grokMulti.addPatternFromFile(grokPath.toString());

            } catch (GrokException | IOException e) {
                throw new IllegalStateException("Failed to add grok pattern: "
                        + uri.toString(), e);
            }
        }

        String pattern = null;
        try {
            // match-lines
            if (config().containsKey(CONF_KEY_MATCH_LINES)) {

                JsonArray matchLines = config().getJsonArray(CONF_KEY_MATCH_LINES, new JsonArray());
                for (int i = 0; i < matchLines.size(); i++) {

                    JsonObject matchLine = matchLines.getJsonObject(i);
                    MatchLine matcher = m_matchLines.get(i);

                    pattern = matchLine.getString(CONF_KEY_PATTERN);
                    matcher.compile(pattern);

                    // tags
                    JsonArray tags = matchLine.getJsonArray(CONF_KEY_TAGS);
                    Preconditions.checkArgument(tags.size() <= MAX_TAG_COUNT,
                            "You can define only " + MAX_TAG_COUNT + " tags for "
                                    + CONF_KEY_MATCH_LINES);

                    for (int j = 0; j < tags.size() && j < MAX_TAG_COUNT; j++) {
                        matcher.addTag(tags.getString(j), j);
                    }

                    // ignore
                    JsonArray ignore = matchLine.getJsonArray(CONF_KEY_IGNORE, new JsonArray());
                    for (int j = 0; j < ignore.size(); j++) {
                        matcher.addIgnore(ignore.getString(j));
                    }
                }
            }
        } catch (GrokException e) {
            throw new IllegalStateException("Failed to compile pattern: "
                    + pattern, e);
        }

        pattern = null;
        try {
            // multi-line
            if (config().containsKey(CONF_KEY_MULTI_LINE)) {
                JsonObject multiLine = config().getJsonObject(CONF_KEY_MULTI_LINE);
                pattern = multiLine.getString(CONF_KEY_PATTERN);
                m_grokMulti.compile(pattern);
                // segment-field
                String field = multiLine.getString(CONF_KEY_SEGMENT_FIELD, "");
                m_multiLine.setSegmentField(field);
                // tags
                JsonArray tags = multiLine.getJsonArray(CONF_KEY_TAGS);
                Preconditions.checkArgument(tags.size() <= MAX_TAG_COUNT,
                        "You can define only " + MAX_TAG_COUNT + " tags for "
                                + CONF_KEY_MULTI_LINE);
                for (int i = 0; i < tags.size() && i < MAX_TAG_COUNT; i++) {
                    m_multiTags[i] = tags.getString(i);
                }
            }
        } catch (GrokException e) {
            throw new IllegalStateException("Failed to compile pattern: "
                    + pattern, e);
        }
    }

    @Override
    protected void onEvent(final JsonObject batchEvent) {

        //
        // Operate on arrays only (batches)
        //
        JsonArray batch = batchEvent.getJsonArray(EVENT_FIELD_BATCH_EVENTS, new JsonArray());
        if (!batchEvent.containsKey(EVENT_FIELD_BATCH_EVENTS)) {
            batch.add(batchEvent);
        }

        List<JsonObject> matchingEvents = new ArrayList();
        for (int i = 0; i < batch.size(); i++) {

            JsonObject event = batch.getJsonObject(i);
            final String line = event.getString(m_inputField, "");

            // Does line match grok single-line expression?
            if (matchLine(event, line)) {

                emitMultiLine(matchingEvents); // Emit multi-line (if any)
                matchingEvents.add(event);

            } else {

                // Does line match grok multi-line expression?
                if (!matchMulti(event, line)) {
                    emitMultiLine(matchingEvents); // Emit multi-line (if any)
                }
            }
        }

        //
        // Forward events
        //
        if (!matchingEvents.isEmpty()) {
            JsonObject destBatch = FilterEvents.createBatchEvent(this, matchingEvents);
            publishEvent(destBatch);
        }
    }

    private boolean matchLine(
            final JsonObject event,
            final String line) {

        boolean match = false;

        for (MatchLine matcher : m_matchLines) {
            Match m = matcher.match(line);
            m.captures();

            List<String> ignore = matcher.getIgnore();
            Map<String, Object> captures = m.toMap();
            match = !captures.isEmpty();
            if (match) {

                JsonObject group = null;
                if (m_groupField.length() > 0) {
                    group = new JsonObject();
                }

                List<String> groupFields = m_groupFields;
                String outputField = m_outputField;
                Iterator<String> keys = captures.keySet().iterator();
                while (keys.hasNext()) {
                    String key = keys.next();
                    if (!ignore.contains(key)) {
                        Object value = captures.get(key);
                        if (value != null) {
                            if ("line".equals(key)) {
                                event.put(outputField, value);
                            } else {
                                if (group != null && groupFields.contains(key)) {
                                    group.put(key, value);
                                } else {
                                    event.put(key, value);
                                }
                            }
                        }
                    }
                }
                // Contain specified fields within a specific group
                if (group != null) {
                    event.put(m_groupField, group);
                }
                // Update tags
                JsonArray tags = event.getJsonArray(EVENT_FIELD_TAGS);

                for (String tag : matcher.getLineTags()) {
                    if (tag != null) {
                        tags.add(tag);
                    } else {
                        break;
                    }
                }
                event.put(EVENT_FIELD_TAGS, tags);
                break; // Match found - first match wins
            }
        }

        return match;
    }

    private boolean matchMulti(
            final JsonObject event,
            final String line) {

        boolean match = false;
        Match m = m_grokMulti.match(line);
        m.captures();

        Map<String, Object> captures = m.toMap();
        if (!captures.isEmpty()) {
            MultiLine multiLine = m_multiLine;
            multiLine.setEvent(event);
            match = multiLine.addFields(captures);
        }

        return match;
    }

    private void emitMultiLine(final List<JsonObject> matchingEvents) {
        // Emit multi-line (if any)
        MultiLine multiLine = m_multiLine;
        if (multiLine.hasLines()) {

            JsonObject multiEvent = multiLine.getEvent();
            JsonArray tags = multiEvent.getJsonArray(EVENT_FIELD_TAGS);
            for (String tag : m_multiTags) {
                if (tag != null) {
                    tags.add(tag);
                } else {
                    break;
                }
            }
            matchingEvents.add(multiEvent);
            multiLine.clear();
        }
    }

    private static final class MatchLine {

        private final Grok m_grokLine;
        private final String[] m_lineTags;
        private final List<String> m_ignore;

        private MatchLine() {
            m_grokLine = new Grok();
            m_lineTags = new String[MAX_TAG_COUNT];
            m_ignore = new ArrayList();
        }

        private String[] getLineTags() {
            return m_lineTags;
        }

        private List<String> getIgnore() {
            return m_ignore;
        }

        private void addPatternFromFile(final String path) throws GrokException {
            m_grokLine.addPatternFromFile(path);
        }

        private void compile(final String pattern) throws GrokException {
            m_grokLine.compile(pattern);
        }

        private void addTag(
                final String tag,
                final int index) {

            m_lineTags[index] = tag;
        }

        private void addIgnore(final String ignore) {
            m_ignore.add(ignore);
        }

        private Match match(final String line) {
            return m_grokLine.match(line);
        }
    }

    private static final class MultiLine {

        private final int m_maxLineCount;
        private JsonObject m_event;
        private List<MultiLineSegment> m_segments;
        private String m_segmentField;
        private String m_outputField;
        private String m_groupField;
        private int m_count;
        private int m_segIndex;

        private MultiLine(
                final int maxLineCount,
                final String groupField) {

            m_maxLineCount = maxLineCount;
            m_groupField = groupField;
            clear();
        }

        private boolean hasLines() {
            return (m_count > 0);
        }

        private JsonObject getEvent() {

            JsonObject event = m_event; // The original/first event
            JsonArray segArray = new JsonArray();
            StringBuilder lines = new StringBuilder();

            for (MultiLineSegment segment : m_segments) {
                lines.append(segment.getLines());
                segArray.add(segment.getFields());
            }

            event.put(m_outputField, lines.toString());
            event.put(m_groupField, segArray);

            return event;
        }

        private MultiLineSegment getCurrentSegment() {
            return m_segments.get(m_segIndex);
        }

        private boolean addFields(final Map<String, Object> fields) {

            boolean added = false;

            if (m_count < m_maxLineCount && fields.containsKey("line")) {

                // Create new segment?
                Object segFieldValue = fields.get(m_segmentField);
                if (m_count > 0 && segFieldValue != null) {
                    MultiLineSegment segment = new MultiLineSegment();
                    m_segments.add(segment);
                    m_segIndex++;
                }

                // Add fields to current segment
                MultiLineSegment segment = getCurrentSegment();
                segment.addFields(fields);
                m_count++;
                added = true;
            }

            return added;
        }

        private void setSegmentField(final String segmentField) {
            m_segmentField = segmentField;
        }

        private void setOutputField(final String outputField) {
            m_outputField = outputField;
        }

        private void setGroupField(final String groupField) {
            m_groupField = groupField;
        }

        private void setEvent(final JsonObject event) {
            if (m_count == 0) {
                m_event.mergeIn(event);
            }
        }

        private void clear() {
            m_count = 0;
            m_event = new JsonObject();
            m_segments = new ArrayList();
            m_segments.add(new MultiLineSegment());
            m_segIndex = 0;
        }
    }

    private static final class MultiLineSegment {

        private final StringBuilder m_lines;
        private final Map<String, Object> m_fields;

        private MultiLineSegment() {
            m_lines = new StringBuilder();
            m_fields = new HashMap();
        }

        private String getLines() {
            return m_lines.toString();
        }

        private JsonObject getFields() {
            return new JsonObject(m_fields);
        }

        private void addFields(final Map<String, Object> fields) {

            // Append line to this segment
            String line = (String) fields.get("line");
            m_lines.append(line);
            m_lines.append("\n");

            // Store fields for later use (append to list if same field exists)
            Map<String, Object> segFields = m_fields;

            Iterator<String> keys = fields.keySet().iterator();
            while (keys.hasNext()) {
                String key = keys.next();
                Object value = fields.get(key);
                if (!"line".equals(key) && value != null) {

                    // Create list if field already exists
                    if (segFields.containsKey(key)) {
                        Object entries = segFields.get(key);
                        if (!(entries instanceof List)) {
                            List tmp = new ArrayList();
                            tmp.add(entries);
                            entries = tmp;
                        }
                        ((List) entries).add(value);
                        segFields.put(key, entries);

                    } else {
                        segFields.put(key, value);
                    }
                }
            }
        }
    }
}

/*
 * Copyright 2017 NG Modular Oy.
 *
 * NG Modular licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.spikex.filters;

import com.google.common.base.Preconditions;
import io.spikex.filters.internal.FilterEvents;
import io.spikex.filters.internal.Modifier;
import io.spikex.filters.internal.Rule;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static io.spikex.base.helper.Events.EVENT_FIELD_BATCH_EVENTS;

/**
 * @author cli
 */
public final class MutateFilter extends AbstractFilter {

    private final Map<String, Modifier> m_actions; // action-id => action
    private final List<Rule> m_rules;

    private static final String CONF_KEY_ID = "id";
    private static final String CONF_KEY_RULES = "rules";
    private static final String CONF_KEY_MODIFIERS = "modifiers";

    private static final String DEFAULT_MODIFIER = "*"; // Always performed (not rule based)

    public MutateFilter() {
        m_actions = new HashMap();
        m_rules = new ArrayList();
    }

    @Override
    protected void onStart() {
        //
        // Non-rules based modifiers
        //
        m_actions.clear();
        Modifier defModifier = Modifier.create(DEFAULT_MODIFIER, variables(), config());
        if (!defModifier.isEmpty()) {
            m_actions.put(DEFAULT_MODIFIER, defModifier);
        }
        //
        // Rule based modifiers
        //
        JsonObject modifiers = config().getJsonObject(CONF_KEY_MODIFIERS, new JsonObject());
        Iterator<String> fields = modifiers.fieldNames().iterator();
        while (fields.hasNext()) {

            String name = fields.next();
            JsonObject def = modifiers.getJsonObject(name);
            Modifier modifier = Modifier.create(name, variables(), def);
            if (!modifier.isEmpty()) {
                m_actions.put(name, modifier);
            }
        }
        // Some modifiers must be defined
        Preconditions.checkState(m_actions.size() > 0, "No modifiers have been defined");
        //
        // Rules (optional)
        //
        m_rules.clear();
        JsonArray rules = config().getJsonArray(CONF_KEY_RULES, new JsonArray());
        for (int i = 0; i < rules.size(); i++) {
            JsonObject ruleDef = rules.getJsonObject(i);
            String id = ruleDef.getString(CONF_KEY_ID, "rule-" + (i + 1));
            Rule rule = Rule.create(id, ruleDef);
            m_rules.add(rule);
            Preconditions.checkState(m_actions.containsKey(rule.getAction()),
                    "rule \"" + rule.getId() + "\" is referencing a missing modifier: "
                            + rule.getAction());
        }
    }

    @Override
    protected void onEvent(final JsonObject batchEvent) {
        //
        // Operate on arrays only (batches)
        //
        JsonArray batch = batchEvent.getJsonArray(EVENT_FIELD_BATCH_EVENTS, new JsonArray());
        if (!batchEvent.containsKey(EVENT_FIELD_BATCH_EVENTS)) {
            batch.add(batchEvent);
        }

        //
        // Destination map
        //
        Map<String, List<JsonObject>> destinations = new HashMap();

        for (int i = 0; i < batch.size(); i++) {
            JsonObject event = batch.getJsonObject(i);
            //
            // Default modifier
            //
            Map<String, Modifier> actions = m_actions;
            Modifier defModifier = actions.get(DEFAULT_MODIFIER);
            if (defModifier != null) {
                logger().trace("Applying default modifier: {}", defModifier);
                defModifier.apply(event);
            }

            //
            // Try to match rule based actions
            //
            String outputAddress = getOutputAddress();
            List<Rule> rules = m_rules;
            for (Rule rule : rules) {
                logger().trace("Evaluating rule: {}", rule);
                if (rule.match(event)) {
                    Modifier modifier = actions.get(rule.getAction());
                    if (modifier != null) {
                        logger().trace("Applying modifier: {}", modifier);
                        outputAddress = modifier.apply(event);
                    }
                    break; // First match breaks loop
                }
            }

            //
            // Build destinations
            //
            List<JsonObject> destBatch = destinations.get(outputAddress);

            if (destBatch == null) {
                destBatch = new ArrayList();
                destinations.put(outputAddress, destBatch);
            }

            destBatch.add(event);
        }

        //
        // Forward events
        //
        for (Map.Entry<String, List<JsonObject>> entry : destinations.entrySet()) {
            JsonObject destBatch = FilterEvents.createBatchEvent(this, entry.getValue());
            publishEvent(entry.getKey(), destBatch);
        }
    }
}
